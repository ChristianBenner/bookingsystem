package com.christianbenner.itc;

import com.christianbenner.itc.data.DirectoryObject;
import com.christianbenner.itc.Directory;
import com.christianbenner.itc.json.DirectoryObjectJsonParser;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DirectoryTest
{
    class DataTest extends DirectoryObject
    {
        public final String name;
        public final int integerValue;

        public DataTest(final String name, final int integerValue)
        {
            this.name = name;
            this.integerValue = integerValue;
        }

        @Override
        public Directory.MatchType compare(String searchPhrase)
        {
            String searchVal = Directory.convertToSearchPhrase(name);
            if(searchPhrase.contains(searchVal))
            {
                return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
            }
            else if(searchVal.contains(searchPhrase))
            {
                return Directory.MatchType.SEARCH_RESULT_OK_MATCH;
            }

            return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
        }
    }

    class DataTestJsonParser extends DirectoryObjectJsonParser<DataTest>
    {
        private static final String DATA_TEST_ARRAY_TAG = "dataTestArray";
        private static final String ID_TAG = "id";
        private static final String INTEGER_VALUE_TAG = "integerValue";
        private static final String NAME_TAG = "name";

        public DataTestJsonParser()
        {
            super(DATA_TEST_ARRAY_TAG);
        }

        protected DataTest readObject(JSONObject jsonObject)
        {
            final DataTest dataTest = new DataTest(jsonObject.getString(NAME_TAG),
                    jsonObject.getInt(INTEGER_VALUE_TAG));
            dataTest.setUniqueId(jsonObject.getInt(ID_TAG));
            return dataTest;
        }

        protected JSONObject writeObject(DataTest dataTest)
        {
            JSONObject obj = new JSONObject();
            obj.put(ID_TAG, dataTest.getUniqueId());
            obj.put(NAME_TAG, dataTest.name);
            obj.put(INTEGER_VALUE_TAG, dataTest.integerValue);
            return obj;
        }
    }

    @Test
    void testAddAndGet()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);

        // Check they exist in the array
        assertEquals(testDirectory.get(jake.getUniqueId()).name, jake.name);
        assertEquals(testDirectory.get(jake.getUniqueId()).integerValue, jake.integerValue);
        assertEquals(testDirectory.get(georgie.getUniqueId()).name, georgie.name);
        assertEquals(testDirectory.get(georgie.getUniqueId()).integerValue, georgie.integerValue);
        assertEquals(testDirectory.get(shiloh.getUniqueId()).name, shiloh.name);
        assertEquals(testDirectory.get(shiloh.getUniqueId()).integerValue, shiloh.integerValue);
        assertEquals(testDirectory.get(jaya.getUniqueId()).name, jaya.name);
        assertEquals(testDirectory.get(jaya.getUniqueId()).integerValue, jaya.integerValue);
        assertEquals(testDirectory.get(heidi.getUniqueId()).name, heidi.name);
        assertEquals(testDirectory.get(heidi.getUniqueId()).integerValue, heidi.integerValue);

        // Remove them so they do not persist in the json file
        testDirectory.clear();
    }

    @Test
    void testRemoveByRef()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);
        testDirectory.remove(jake);
        testDirectory.remove(georgie);
        testDirectory.remove(shiloh);
        testDirectory.remove(heidi);
        testDirectory.remove(jaya);

        // Check they no longer exist in the array
        assertNull(testDirectory.get(jake.getUniqueId()));
        assertNull(testDirectory.get(georgie.getUniqueId()));
        assertNull(testDirectory.get(shiloh.getUniqueId()));
        assertNull(testDirectory.get(heidi.getUniqueId()));
        assertNull(testDirectory.get(jaya.getUniqueId()));
    }

    @Test
    void testRemoveById()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);
        testDirectory.remove(jake.getUniqueId());
        testDirectory.remove(georgie.getUniqueId());
        testDirectory.remove(shiloh.getUniqueId());
        testDirectory.remove(heidi.getUniqueId());
        testDirectory.remove(jaya.getUniqueId());

        // Check they no longer exist in the array
        assertNull(testDirectory.get(jake.getUniqueId()));
        assertNull(testDirectory.get(georgie.getUniqueId()));
        assertNull(testDirectory.get(shiloh.getUniqueId()));
        assertNull(testDirectory.get(heidi.getUniqueId()));
        assertNull(testDirectory.get(jaya.getUniqueId()));
    }

    @Test
    void testUniqueIdAssigned()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);



        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);

        List<DataTest> directoryContents = testDirectory.getAll();

        // Test that each DataTest has a unique ID
        for(int i = 0; i < directoryContents.size(); i++)
        {
            for(int n = 0; n < directoryContents.size(); n++)
            {
                if(n == i)
                {
                    continue;
                }

                assertNotEquals(directoryContents.get(i).getUniqueId(), directoryContents.get(n).getUniqueId());
            }
        }

        testDirectory.clear();
    }

    @Test
    void testSearch()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);

        List<DataTest> search = testDirectory.search("Shil");
        assertNotNull(search);
        assertNotEquals(search.size(), 0);
        assertEquals(search.get(0).integerValue, 124);

        testDirectory.clear();
    }

    @Test
    void testGoodMatchSearch()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);

        List<DataTest> search = testDirectory.search("Georgie");
        assertNotNull(search);
        assertNotEquals(search.size(), 0);
        assertEquals(search.get(0).integerValue, 643);

        testDirectory.clear();
    }

    @Test
    void testClear()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);

        testDirectory.clear();

        assertEquals(testDirectory.getSize(), 0);
        assertNull(testDirectory.get(jake.getUniqueId()));
        assertNull(testDirectory.get(georgie.getUniqueId()));
        assertNull(testDirectory.get(shiloh.getUniqueId()));
        assertNull(testDirectory.get(jaya.getUniqueId()));
        assertNull(testDirectory.get(heidi.getUniqueId()));
    }

    @Test
    void testGetSize()
    {
        Directory<DataTest> testDirectory = new Directory<>(new File("test.json"), new DataTestJsonParser());

        DataTest jake = new DataTest("Jake", 523);
        DataTest georgie = new DataTest("Georgie", 643);
        DataTest shiloh = new DataTest("Shiloh", 124);
        DataTest jaya = new DataTest("Jaya", 856);
        DataTest heidi = new DataTest("Heidi", 897);

        testDirectory.add(jake);
        testDirectory.add(georgie);
        testDirectory.add(shiloh);
        testDirectory.add(heidi);
        testDirectory.add(jaya);

        assertEquals(testDirectory.getSize(), 5);

        testDirectory.clear();
    }
}