package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Lesson;
import com.christianbenner.itc.data.Session;
import com.christianbenner.itc.data.SessionTime;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LessonJsonParserTest
{
    @Test
    void readWriteObject()
    {
        LessonJsonParser jsonParser = new LessonJsonParser();

        Lesson session = new Lesson(1, 1, 1, new SessionTime(SessionTime.MONDAY,
                900), 5);
        Session readSession = jsonParser.readObject(jsonParser.writeObject(session));
        assertEquals(session, readSession);
    }
}