package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Room;
import com.christianbenner.itc.data.Subject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubjectJsonParserTest
{
    @Test
    void readWriteObject()
    {
        SubjectJsonParser jsonParser = new SubjectJsonParser();

        Subject subject = new Subject("Mathematics");
        Subject readSubject = jsonParser.readObject(jsonParser.writeObject(subject));
        assertEquals(subject, readSubject);
    }
}