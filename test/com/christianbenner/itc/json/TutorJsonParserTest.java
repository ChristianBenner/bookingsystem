package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.data.Tutor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TutorJsonParserTest
{
    @Test
    void readWriteObject()
    {
        TutorJsonParser jsonParser = new TutorJsonParser();

        Tutor tutor = new Tutor("Mr", "Chris", "Benner", new Address("AB1 2CD",
                "1 Test Road", "Test Town", "Test City"),
                "555-55-5");
        Tutor readTutor = jsonParser.readObject(jsonParser.writeObject(tutor));
        assertEquals(tutor, readTutor);
    }
}