package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Room;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoomJsonParserTest
{
    @Test
    void readWriteObject()
    {
        RoomJsonParser jsonParser = new RoomJsonParser();

        Room room = new Room("Hall A", 2);
        Room readRoom = jsonParser.readObject(jsonParser.writeObject(room));
        assertEquals(room, readRoom);
    }
}