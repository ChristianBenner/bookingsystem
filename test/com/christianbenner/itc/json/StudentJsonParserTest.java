package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.data.Room;
import com.christianbenner.itc.data.Student;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentJsonParserTest
{
    @Test
    void readWriteObject()
    {
        StudentJsonParser jsonParser = new StudentJsonParser();

        Student student = new Student("Student", "Person", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1");
        Student readStudent = jsonParser.readObject(jsonParser.writeObject(student));
        assertEquals(student, readStudent);
    }
}