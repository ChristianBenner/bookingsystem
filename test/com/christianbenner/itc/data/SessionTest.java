package com.christianbenner.itc.data;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SessionTest
{
    @Test
    void getTutorId()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        assertEquals(session.getTutorId(), 2);
    }

    @Test
    void setTutorId()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        session.setTutorId(643);
        assertEquals(session.getTutorId(), 643);
    }

    @Test
    void getRoomId()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        assertEquals(session.getRoomId(), 3);
    }

    @Test
    void setRoomId()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        session.setRoomId(62);
        assertEquals(session.getRoomId(), 62);
    }

    @Test
    void getSubjectId()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        assertEquals(session.getSubjectId(), 4);
    }

    @Test
    void setSubjectId()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        session.setSubjectId(123);
        assertEquals(session.getSubjectId(), 123);
    }

    @Test
    void getLessonTime()
    {
        SessionTime sessionTime = new SessionTime(SessionTime.MONDAY, 900);
        Lesson session = new Lesson(2, 3, 4, sessionTime, 5);
        assertEquals(session.getSessionTime(), sessionTime);
    }

    @Test
    void setLessonTime()
    {
        SessionTime sessionTime = new SessionTime(SessionTime.MONDAY, 900);
        Lesson session = new Lesson(2, 3, 4, sessionTime, 5);

        SessionTime sessionTime2 = new SessionTime(SessionTime.TUESDAY, 1020);
        session.setSessionTime(sessionTime2);
        assertEquals(session.getSessionTime(), sessionTime2);
    }

    @Test
    void getMaxSpaces()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        assertEquals(session.getMaxSpaces(), 5);
    }

    @Test
    void testSetGetStudents()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);

        List<Integer> studentsToAdd = new ArrayList<>();
        studentsToAdd.add(5);
        studentsToAdd.add(6);
        studentsToAdd.add(7);
        studentsToAdd.add(8);

        session.setStudents(studentsToAdd);

        List<Integer> students = session.getStudents();

        for(int i = 0; i < studentsToAdd.size(); i++)
        {
            boolean found = false;

            // Look for student in array
            for(int n = 0; n < students.size(); n++)
            {
                if(studentsToAdd.get(i) == students.get(n))
                {
                    found = true;
                    break;
                }
            }

            assertTrue(found);
        }
    }

    @Test
    void hasSpace()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);

        assertTrue(session.hasSpace());
        session.addStudent(5);
        assertTrue(session.hasSpace());
        session.addStudent(6);
        assertTrue(session.hasSpace());
        session.addStudent(7);
        assertTrue(session.hasSpace());
        session.addStudent(8);
        assertTrue(session.hasSpace());
        session.addStudent(9);
        assertFalse(session.hasSpace());
    }

    @Test
    void getSpacesLeft()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        assertEquals(session.getSpacesLeft(), 5);
        session.addStudent(5);
        assertEquals(session.getSpacesLeft(), 4);
        session.addStudent(6);
        assertEquals(session.getSpacesLeft(), 3);
        session.addStudent(7);
        assertEquals(session.getSpacesLeft(), 2);
        session.addStudent(8);
        assertEquals(session.getSpacesLeft(), 1);
        session.addStudent(9);
        assertEquals(session.getSpacesLeft(), 0);
    }

    @Test
    void addStudent()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        session.addStudent(5);
        assertTrue(session.containsStudent(5));
    }

    @Test
    void testEquals()
    {
        Lesson session = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);
        Lesson session2 = new Lesson(2, 3, 4, new SessionTime(SessionTime.MONDAY,
                900), 5);

        assertEquals(session, session);
        assertEquals(session, session2);
    }
}