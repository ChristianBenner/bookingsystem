package com.christianbenner.itc.data;

import com.christianbenner.itc.ChangeListener;
import com.christianbenner.itc.Directory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DirectoryObjectTest
{
    class TestDirectoryObject extends DirectoryObject
    {
        int val = 12;

        public void setIntVal(int value)
        {
            this.val = value;
            super.notifyChangeListeners();
        }

        public int getIntVal()
        {
            return val;
        }

        @Override
        public Directory.MatchType compare(String searchPhrase)
        {
            return null;
        }
    }

    @Test
    void addChangeListener()
    {
        TestDirectoryObject testDirectoryObject = new TestDirectoryObject();

        final boolean[] changed = {false};
        testDirectoryObject.addChangeListener(new ChangeListener()
        {
            @Override
            public void hasChanged()
            {
                changed[0] = true;
            }
        });

        assertFalse(changed[0]);
        testDirectoryObject.setUniqueId(5);
        assertTrue(changed[0]);


        TestDirectoryObject testDirectoryObject2 = new TestDirectoryObject();

        final boolean[] changed2 = {false};
        testDirectoryObject2.addChangeListener(new ChangeListener()
        {
            @Override
            public void hasChanged()
            {
                changed2[0] = true;
            }
        });

        assertFalse(changed2[0]);
        testDirectoryObject2.setIntVal(63);
        assertTrue(changed2[0]);
        assertEquals(testDirectoryObject2.getIntVal(), 63);
    }

    @Test
    void removeChangeListener()
    {
        TestDirectoryObject testDirectoryObject = new TestDirectoryObject();

        final boolean[] changed = {false};

        ChangeListener listener = new ChangeListener()
        {
            @Override
            public void hasChanged()
            {
                changed[0] = true;
            }
        };

        testDirectoryObject.addChangeListener(listener);
        testDirectoryObject.removeChangeListener(listener);

        assertFalse(changed[0]);
        testDirectoryObject.setUniqueId(5);
        assertFalse(changed[0]);
    }

    @Test
    void notifyChangeListeners()
    {
        TestDirectoryObject testDirectoryObject = new TestDirectoryObject();

        final boolean[] changed1 = {false};
        final boolean[] changed2 = {false};

        ChangeListener listener1 = new ChangeListener()
        {
            @Override
            public void hasChanged()
            {
                changed1[0] = true;
            }
        };

        ChangeListener listener2 = new ChangeListener()
        {
            @Override
            public void hasChanged()
            {
                changed2[0] = true;
            }
        };

        testDirectoryObject.addChangeListener(listener1);
        testDirectoryObject.addChangeListener(listener2);

        assertFalse(changed1[0]);
        assertFalse(changed2[0]);
        testDirectoryObject.setUniqueId(5);
        assertTrue(changed1[0]);
        assertTrue(changed2[0]);
    }

    @Test
    void testSetGetUniqueId()
    {
        TestDirectoryObject directoryObject = new TestDirectoryObject();
        assertTrue(directoryObject.setUniqueId(5));
        assertEquals(directoryObject.getUniqueId(), 5);
        assertFalse(directoryObject.setUniqueId(2));
        assertNotEquals(directoryObject.getUniqueId(), 2);
        assertEquals(directoryObject.getUniqueId(), 5);
    }
}