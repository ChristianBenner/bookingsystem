package com.christianbenner.itc.data;

import com.christianbenner.itc.data.Subject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubjectTest
{
    @Test
    void getName()
    {
        Subject subject = new Subject("Geography");
        assertEquals(subject.getName(), "Geography");
    }
}