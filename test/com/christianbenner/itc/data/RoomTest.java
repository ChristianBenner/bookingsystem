package com.christianbenner.itc.data;

import com.christianbenner.itc.data.Room;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoomTest
{
    @Test
    void getBuildingName()
    {
        Room room = new Room("Art Building", 10);
        assertEquals(room.getBuildingName(), "Art Building");
    }

    @Test
    void getRoomNumber()
    {
        Room room = new Room("Art Building", 10);
        assertEquals(room.getRoomNumber(), 10);
    }
}