package com.christianbenner.itc.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SessionTimeTest
{
    @Test
    void getHoursFromDayStart()
    {
        SessionTime sessionTime = new SessionTime(SessionTime.FRIDAY, 600);
        assertEquals(sessionTime.getHoursFromDayStart(), 600);
    }

    @Test
    void setTime()
    {
        SessionTime sessionTime = new SessionTime(SessionTime.WEDNESDAY, 532);
        sessionTime.setTime(900);
        assertEquals(sessionTime.getHoursFromDayStart(), 900);
    }

    @Test
    void getDay()
    {
        SessionTime sessionTime = new SessionTime(SessionTime.THURSDAY, 150);
        assertEquals(sessionTime.getDay(), SessionTime.THURSDAY);
    }

    @Test
    void setDay()
    {
        SessionTime sessionTime = new SessionTime(SessionTime.MONDAY, 720);
        sessionTime.setDay(SessionTime.TUESDAY);
        assertEquals(sessionTime.getDay(), SessionTime.TUESDAY);

    }
}