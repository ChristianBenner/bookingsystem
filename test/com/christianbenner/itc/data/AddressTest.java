package com.christianbenner.itc.data;

import com.christianbenner.itc.data.Address;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest
{
    @Test
    void getPostCode()
    {
        Address address = new Address("AB1 2CD", null, null,
                null);
        assertEquals(address.getPostCode(), "AB1 2CD");
    }

    @Test
    void setPostCode()
    {
        Address address = new Address(null, null, null, null);
        address.setPostCode("AB1 2CD");
        assertEquals(address.getPostCode(), "AB1 2CD");
    }

    @Test
    void getAddressLineOne()
    {
        Address address = new Address(null, "515 Test Avenue", null,
                null);
        assertEquals(address.getAddressLineOne(), "515 Test Avenue");
    }

    @Test
    void setAddressLineOne()
    {
        Address address = new Address(null, null, null, null);
        address.setAddressLineOne("515 Test Avenue");
        assertEquals(address.getAddressLineOne(), "515 Test Avenue");
    }

    @Test
    void getAddressLineTwo()
    {
        Address address = new Address(null, null, "Test Village",
                null);
        assertEquals(address.getAddressLineTwo(), "Test Village");
    }

    @Test
    void setAddressLineTwo()
    {
        Address address = new Address(null, null, null, null);
        address.setAddressLineTwo("Test Village");
        assertEquals(address.getAddressLineTwo(), "Test Village");
    }

    @Test
    void getAddressLineThree()
    {
        Address address = new Address(null, null, null,
                "Test Town");
        assertEquals(address.getAddressLineThree(), "Test Town");
    }

    @Test
    void setAddressLineThree()
    {
        Address address = new Address(null, null, null, null);
        address.setAddressLineThree("Test Town");
        assertEquals(address.getAddressLineThree(), "Test Town");
    }

    @Test
    void testEquals()
    {
        // Test if an item referring to itself is equal
        final Address address = new Address("AB1 2CD", "515 Test Avenue",
                "Test Village", "Test Town");
        assertEquals(address, address);

        // Test if an two different addresses with the same data returns true on equals
        final Address address2 = new Address("AB1 2CD", "515 Test Avenue",
                "Test Village", "Test Town");
        assertEquals(address, address2);

        // Check that two different addresses do not return true on equals
        final Address address3 = new Address("EF3 4GH", "72 Test Close",
                "Test Town", "Test County");
        assertNotEquals(address2, address3);
    }
}