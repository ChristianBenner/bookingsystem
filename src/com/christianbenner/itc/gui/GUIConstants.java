package com.christianbenner.itc.gui;

import java.awt.*;
public class GUIConstants
{
    public static final Color BACKGROUND_COLOUR = new Color(199, 219, 243);
    public static final Color PANEL_COLOUR = new Color(177, 195, 216);
}
