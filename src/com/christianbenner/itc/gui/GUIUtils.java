package com.christianbenner.itc.gui;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.data.ParentMeeting;
import com.christianbenner.itc.data.SessionTime;
import com.christianbenner.itc.data.Tutor;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static com.christianbenner.itc.data.SessionTime.HOURS_IN_DAY;

public class GUIUtils
{
    private static final int DAYS_IN_EDUCATION_WEEK = 5;
    private static final int HOURS_IN_DAY = 24;
    private static final int NUM_AVAILABLE_WEEKS_TO_SHOW = 6;

    private static final String TRIAL_KEY = "iddqd";
    private static final String ACTUAL_KEY = "ak_k98vm1ogVChiPqwvElUC1jcUQUPuO";
    private static final String API_KEY = ACTUAL_KEY;
    private static final String API_URL = "https://api.ideal-postcodes.co.uk/v1/postcodes/";

    public static boolean isWeekAvailableForParentMeeting(final GregorianCalendar gregorianCalendar, final Tutor tutor)
    {
        // Get parent meetings for the tutor
        List<ParentMeeting> parentMeetings = tutor.getParentMeetings();

        // See if the week contains any of the session times of the parent meetings
        for(ParentMeeting parentMeeting : parentMeetings)
        {
            if(weekContainsSessionTime(gregorianCalendar, parentMeeting.getSessionTime()))
            {
                return false;
            }
        }

        return true;
    }

    public static List<GregorianCalendar> getNextMeetingSpaces(final Tutor tutor)
    {
        // List of available weeks for booking parent meetings
        List<GregorianCalendar> availableWeeks = new ArrayList<>();

        // Get parent meetings for the tutor
        List<ParentMeeting> parentMeetings = tutor.getParentMeetings();


        GregorianCalendar gregorianCalendar = getEducationWeek();

        // Must fill the list of available weeks with the specified number of weeks
        while(availableWeeks.size() < NUM_AVAILABLE_WEEKS_TO_SHOW)
        {
            boolean weekEmpty = true;

            // See if the week contains any of the session times of the parent meetings
            for(ParentMeeting parentMeeting : parentMeetings)
            {
                if(weekContainsSessionTime(gregorianCalendar, parentMeeting.getSessionTime()))
                {
                    weekEmpty = false;
                }
            }

            if(weekEmpty)
            {
                // Construct a copy of the week
                final GregorianCalendar currentWeek = new GregorianCalendar(
                        gregorianCalendar.get(GregorianCalendar.YEAR),
                        gregorianCalendar.get(GregorianCalendar.MONTH),
                        gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH));

                // Add this week to the list of available weeks
                availableWeeks.add(currentWeek);
            }

            // Add a week to the calendar for next attempt
            addWeek(gregorianCalendar);
        }

        return availableWeeks;
    }

    public static boolean matchesDate(GregorianCalendar calendar, SessionTime sessionTime)
    {
        // Compare the dates to see if they are the same day
        if(calendar.get(GregorianCalendar.YEAR) == sessionTime.getYear() &&
                calendar.get(GregorianCalendar.MONTH) + 1 == sessionTime.getMonth() &&
                calendar.get(GregorianCalendar.DAY_OF_MONTH) == sessionTime.getDay())
        {
            return true;
        }

        return false;
    }

    public static boolean weekContainsSessionTime(GregorianCalendar gregorianCalendar, SessionTime sessionTime)
    {
        // Get days that the lesson time must be on to be inside the timetable
        GregorianCalendar weekDays = new GregorianCalendar();
        weekDays.setTime(gregorianCalendar.getTime());

        for(int i = 0; i < DAYS_IN_EDUCATION_WEEK; i++)
        {
            if(matchesDate(weekDays, sessionTime))
            {
                return true;
            }

            weekDays.add(GregorianCalendar.HOUR, HOURS_IN_DAY);
        }

        return false;
    }

    public static int getDayOfWeek(SessionTime sessionTime)
    {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(sessionTime.getYear(),
                sessionTime.getMonth() - 1, sessionTime.getDay());

        switch (gregorianCalendar.get(Calendar.DAY_OF_WEEK))
        {
            case GregorianCalendar.MONDAY:
                return SessionTime.MONDAY;
            case GregorianCalendar.TUESDAY:
                return SessionTime.TUESDAY;
            case GregorianCalendar.WEDNESDAY:
                return SessionTime.WEDNESDAY;
            case GregorianCalendar.THURSDAY:
                return SessionTime.THURSDAY;
            case GregorianCalendar.FRIDAY:
                return SessionTime.FRIDAY;
        }

        return 0;
    }

    public static void addWeek(GregorianCalendar gregorianCalendar)
    {
        gregorianCalendar.add(Calendar.HOUR, 7 * 24);
    }

    public static void takeWeek(GregorianCalendar gregorianCalendar)
    {
        gregorianCalendar.add(Calendar.HOUR, -7 * 24);
    }

    public static GregorianCalendar getEducationWeek()
    {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();

        // Start on Monday. If the date is Monday-Friday, get date for current week Monday. If the date is Saturday or
        // Sunday, get the date for the next Monday.
        while(gregorianCalendar.get(GregorianCalendar.DAY_OF_WEEK) != GregorianCalendar.MONDAY)
        {
            if(gregorianCalendar.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SATURDAY ||
                    gregorianCalendar.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY)
            {
                gregorianCalendar.add(GregorianCalendar.HOUR, 24);
            }
            else
            {
                gregorianCalendar.add(GregorianCalendar.HOUR, -24);
            }
        }

        return gregorianCalendar;
    }

    public static Image loadImage(final String filepath)
    {
        Image image = null;
        try
        {
            image = ImageIO.read(GUIConstants.class.getResource(filepath));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return image;
    }

    public static Image loadImage(final String filepath, final int width, final int height)
    {
        final Image image = loadImage(filepath);
        if(image != null)
        {
            return getScaledImage(image, width, height);
        }

        return null;
    }

    public static Image getScaledImage(final Image imageSource, final int width, final int height)
    {
        final BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        Graphics2D graphics2d = resizedImage.createGraphics();
        graphics2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.drawImage(imageSource, 0, 0, width, height, null);
        graphics2d.dispose();

        return resizedImage;
    }

    public static Font loadFont(final String filepath)
    {
        final InputStream inputStream = GUIConstants.class.getResourceAsStream(filepath);
        Font font = null;
        try
        {
            font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
        }
        catch (FontFormatException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return font;
    }

    public static java.util.List<Address> getAddressListFromPostCode(String postCode) throws IOException
    {
        // Create UTF-8 format post code without illegal characters to be sent to API webservice
        final String apiPostCode = URLEncoder.encode(postCode, "UTF-8").replace("+", "%20");

        // Construct URL to target the request at (which contains the API key)
        final URL url = new URL(API_URL + apiPostCode + "?api_key=" + API_KEY);

        // Create secure connection and request response
        final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        // Retrieve the response code
        final int responseCode = connection.getResponseCode();

        // Create a BufferedReader so we can iterate through the reply and create a string
        final BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        // String to store each line from the response reader
        String inputLine;

        // String buffer to store and construct the response string
        StringBuffer response = new StringBuffer();

        // Iterate through the response until there is no more lines to read, adding each line to the response string
        // buffer.
        while((inputLine = responseReader.readLine()) != null)
        {
            response.append(inputLine);
        }

        // Finished reading, close the buffered reader
        responseReader.close();

        System.out.println("RESPONSE: " + response);

        // Array list to store all the addresses
        List<Address> addressArrayList = new ArrayList<>();

        // Parse the JSON
        JSONObject responseJson = new JSONObject(response.toString());

        // Get the JSON array for the 'result' array (array of address information)
        JSONArray resultArray = responseJson.getJSONArray("result");

        // Iterate through the result array creating an address object from information in each
        for(int i = 0; i < resultArray.length(); i++)
        {
            // This JSON object represents a single result from the result array
            JSONObject resultObject = resultArray.getJSONObject(i);

            // Create address and populate it with information from the result JSON object
            final Address newAddress = new Address(resultObject.getString("postcode"),
                    resultObject.getString("line_1"),
                    resultObject.getString("line_2"),
                    resultObject.getString("post_town"));

            // Add address to address list
            addressArrayList.add(newAddress);
        }

        return addressArrayList;
    }
}
