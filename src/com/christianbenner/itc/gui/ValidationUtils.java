package com.christianbenner.itc.gui;

import javax.swing.*;
import java.awt.*;

public class ValidationUtils
{
    public static void showSpinnerRangeFailure(final Component component, final String fieldName, final int minValue,
                                               final int maxValue)
    {
        JOptionPane.showMessageDialog(component, fieldName + " must have a value in the range " + minValue +
                        "-" + maxValue, "Unsuccessful", JOptionPane.WARNING_MESSAGE);
    }

    public static void showNoSelectionFailure(final Component component, final String fieldName)
    {
        JOptionPane.showMessageDialog(component, "You must select a " + fieldName + " value",
                "Unsuccessful", JOptionPane.WARNING_MESSAGE);
    }

    public static void showValidationFailure(final Component component, final String fieldName, final int minLength,
                                             final int maxLength)
    {
        JOptionPane.showMessageDialog(component, fieldName + " must be at least " + minLength +
                "-" + maxLength + " characters long", "Unsuccessful", JOptionPane.WARNING_MESSAGE);
    }

    public static boolean validateSpinner(final int value, final int minValue, final int maxValue)
    {
        return value >= minValue && value <= maxValue;
    }

    public static boolean validateText(final String string, final int minLength, final int maxLength)
    {
        if(minLength == 0 && string == null)
        {
            return true;
        }

        if(string.length() < minLength)
        {
            return false;
        }

        if(string.length() > maxLength)
        {
            return false;
        }

        return true;
    }
}
