package com.christianbenner.itc.gui.search;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public class SearchTextField extends JTextField
{
    private List<TextChangedListener> textChangedListeners;

    interface TextChangedListener extends EventListener
    {
        void textChanged(final String text);
    }

    public SearchTextField()
    {
        this.textChangedListeners = new ArrayList<>();

        super.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void insertUpdate(DocumentEvent documentEvent)
            {
                textChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent)
            {
                textChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent)
            {
                textChange();
            }

            public void textChange()
            {
                notifyTextChangedListeners();
            }
        });
    }

    public boolean addTextChangedListener(final TextChangedListener textChangedListener)
    {
        return this.textChangedListeners.add(textChangedListener);
    }

    public boolean removeTextChangedListener(final TextChangedListener textChangedListener)
    {
        return this.textChangedListeners.remove(textChangedListener);
    }

    private void notifyTextChangedListeners()
    {
        for(final TextChangedListener textChangedListener : textChangedListeners)
        {
            textChangedListener.textChanged(getText());
        }
    }
}
