package com.christianbenner.itc.gui.search;

import com.christianbenner.itc.gui.GUIConstants;

import javax.swing.*;

public class SearchPopupTabbed extends JDialog
{
    private JTabbedPane tabbedPane;

    public SearchPopupTabbed(JFrame frameOwner, String objectName)
    {
        super(frameOwner, "Search " + objectName + "s", ModalityType.APPLICATION_MODAL);
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);
        super.getContentPane().setBackground(GUIConstants.BACKGROUND_COLOUR);

        tabbedPane = new JTabbedPane();
        tabbedPane.setBackground(GUIConstants.BACKGROUND_COLOUR);

        super.add(tabbedPane);
        super.setAlwaysOnTop(true);
        super.pack();
        super.setSize(500, 500);
        super.setLocationRelativeTo(null);
    }

    public void addTab(String tabName, SearchPanel panel)
    {
        tabbedPane.addTab(tabName, panel);
    }
}
