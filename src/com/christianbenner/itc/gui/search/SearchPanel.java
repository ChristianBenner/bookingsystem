package com.christianbenner.itc.gui.search;

import com.christianbenner.itc.Directory;
import com.christianbenner.itc.data.DirectoryObject;
import com.christianbenner.itc.gui.GUIConstants;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;
import java.util.List;

import static com.christianbenner.itc.gui.GUIConstants.PANEL_COLOUR;

public class SearchPanel<E extends DirectoryObject> extends JPanel
{
    private SearchTextField searchField;
    private JList<E> searchResults;
    private Directory<E> directory;
    private String existingSearchString;

    public SearchPanel(final Font font, final Directory<E> directoryRef, boolean addSearchField)
    {
        super.setLayout(new BorderLayout());

        this.directory = directoryRef;
        this.existingSearchString = "";

        JPanel borderLayout = new JPanel(new BorderLayout());
        borderLayout.setBackground(PANEL_COLOUR);

        Font textFont = font.deriveFont(22f);

        if(addSearchField)
        {
            JPanel searchHeader = new JPanel();
            searchHeader.setLayout(new BorderLayout());
            searchHeader.setBackground(GUIConstants.BACKGROUND_COLOUR);
            searchHeader.setBorder(new EmptyBorder(5, 5, 5, 5));

            JLabel searchText = new JLabel("Search:");
            searchText.setFont(textFont);
            searchText.setBorder(new EmptyBorder(0, 3, 0, 10));
            searchHeader.add(searchText, BorderLayout.WEST);

            searchField = new SearchTextField();
            searchField.setFont(textFont);
            searchField.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, true));
            searchField.setBackground(GUIConstants.BACKGROUND_COLOUR);
            searchField.addTextChangedListener(new SearchTextField.TextChangedListener()
            {
                @Override
                public void textChanged(String text)
                {
                    updateSearchResults(text);
                }
            });

            searchHeader.add(searchField, BorderLayout.CENTER);
            borderLayout.add(searchHeader, BorderLayout.NORTH);
        }

        DefaultListModel<E> listModel = new DefaultListModel<>();
        searchResults = new JList<>(listModel);
        showAllInList();

        searchResults.setSelectionBackground(Color.DARK_GRAY);
        searchResults.setSelectionForeground(Color.WHITE);
        searchResults.setBackground(PANEL_COLOUR);
        searchResults.setFont(textFont);

        JScrollPane scrollPane = new JScrollPane(searchResults);
        scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, false));
        scrollPane.getVerticalScrollBar().setBackground(PANEL_COLOUR);
        scrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI()
        {
            @Override
            protected void configureScrollBarColors()
            {
                this.thumbColor = PANEL_COLOUR.darker();
            }
        });
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        borderLayout.add(scrollPane, BorderLayout.CENTER);
        super.add(borderLayout, BorderLayout.CENTER);
    }

    public SearchPanel(final Font font, final Directory<E> directoryRef)
    {
        this(font, directoryRef, true);
    }

    public void showAllInList()
    {
        DefaultListModel<E> listModel = new DefaultListModel<>();
        List<E> uniqueObjectList = directory.getAll();
        for(E directoryObject : uniqueObjectList)
        {
            listModel.addElement(directoryObject);
        }
        searchResults.setModel(listModel);
    }

    public void setExistingSearchString(final String searchString)
    {
        existingSearchString = Directory.convertToSearchPhrase(searchString);
        updateSearchResults(existingSearchString);
    }

    public E getSelection()
    {
        return searchResults.getSelectedValue();
    }

    private void updateSearchResults(final String text)
    {
        final String searchString = existingSearchString + text;
        if(searchString.isEmpty())
        {
            showAllInList();
        }
        else
        {
            // Update list with new search
            DefaultListModel listModel = new DefaultListModel();
            List<E> uniqueObjectList = directory.search(searchString);
            for(DirectoryObject directoryObject : uniqueObjectList)
            {
                listModel.addElement(directoryObject);
            }
            searchResults.setModel(listModel);
        }
    }
}
