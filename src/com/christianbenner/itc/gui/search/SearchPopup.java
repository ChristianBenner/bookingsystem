package com.christianbenner.itc.gui.search;

import javax.swing.*;
import java.awt.*;

public class SearchPopup extends JDialog
{
    private SearchPanel panel;

    public SearchPopup(JFrame frameOwner, SearchPanel panel, String objectName)
    {
        super(frameOwner, "Search " + objectName + "s", ModalityType.APPLICATION_MODAL);
        this.panel = panel;
        this.panel.setPreferredSize(new Dimension(500, 500));

        super.setAlwaysOnTop(true);
        super.add(panel);
        super.pack();
        super.setLocationRelativeTo(null);
        super.setResizable(false);
    }

    public SearchPanel getPanel()
    {
        return panel;
    }
}
