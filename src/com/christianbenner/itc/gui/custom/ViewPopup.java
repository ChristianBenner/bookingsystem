package com.christianbenner.itc.gui.custom;

import javax.swing.*;

public class ViewPopup extends JDialog
{
    private ViewPanel panel;

    public ViewPopup(JFrame frameOwner, ViewPanel panel, String objectName)
    {
        super(frameOwner, objectName, ModalityType.APPLICATION_MODAL);
        this.panel = panel;

        super.setAlwaysOnTop(true);
        super.add(panel);
        super.pack();
        super.setLocationRelativeTo(null);
    }

    public ViewPanel getPanel()
    {
        return panel;
    }
}
