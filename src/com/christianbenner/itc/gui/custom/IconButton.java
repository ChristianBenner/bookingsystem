package com.christianbenner.itc.gui.custom;

import com.christianbenner.itc.gui.GUIUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class IconButton extends JButton
{
    private static final Color COLOUR = new Color(177, 195, 216);

    private JLabel textLabel;

    public IconButton(final Font font, final String text, final Image image)
    {
        super.setBorderPainted(false);
        super.setFocusPainted(false);
        super.setFont(font);

        ImageIcon imageIcon = new ImageIcon(GUIUtils.getScaledImage(image, 64, 64));
        JLabel iconLabel = new JLabel(imageIcon);
        textLabel = new JLabel(text, SwingConstants.CENTER);
        textLabel.setFont(font);
        super.setLayout(new BorderLayout());
        super.add(iconLabel, BorderLayout.WEST);
        super.add(textLabel, BorderLayout.CENTER);
        super.setBackground(COLOUR);
        super.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseEntered(MouseEvent e)
            {
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                setBackground(Color.DARK_GRAY);
                textLabel.setForeground(COLOUR);
            }

            @Override
            public void mouseExited(MouseEvent e)
            {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                setBackground(COLOUR);
                textLabel.setForeground(Color.DARK_GRAY);
            }
        });
    }

    @Override
    public void setText(String text)
    {
        textLabel.setText(text);
    }
}
