package com.christianbenner.itc.gui.custom;

import com.christianbenner.itc.gui.GUIConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ViewPanel extends JPanel
{
    public ViewPanel(Font font, String title)
    {
        super(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Font textFont = font.deriveFont(28f);

        JLabel titleLabel = new JLabel(title, JLabel.HORIZONTAL);
        titleLabel.setFont(textFont);
        super.add(titleLabel, BorderLayout.NORTH);
    }
}
