package com.christianbenner.itc.gui.custom;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.christianbenner.itc.gui.GUIConstants.PANEL_COLOUR;

public class Button extends JButton
{
    public Button(final String text, final Color backgroundColor)
    {
        super(text);
        super.setFocusPainted(false);
        super.setBackground(PANEL_COLOUR);
        super.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2, true));

        super.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseEntered(MouseEvent e)
            {
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                setBackground(Color.DARK_GRAY);
                setForeground(backgroundColor);
            }

            @Override
            public void mouseExited(MouseEvent e)
            {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                setBackground(backgroundColor);
                setForeground(Color.DARK_GRAY);
            }
        });
    }

    public Button(final String text)
    {
        this(text, PANEL_COLOUR);
    }

    public Button(final Font font, final String text)
    {
        this(text);
        setFont(font);
    }
}
