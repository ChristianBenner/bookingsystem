package com.christianbenner.itc.gui.custom;

import javax.swing.*;

public class PanelPopup extends JDialog
{
    private JPanel panel;

    public PanelPopup(JFrame frameOwner, JPanel panel, String objectName)
    {
        super(frameOwner, objectName, ModalityType.APPLICATION_MODAL);
        this.panel = panel;

        super.setAlwaysOnTop(true);
        super.add(panel);
        super.pack();
        super.setLocationRelativeTo(null);
        super.setResizable(false);
    }

    public JPanel getPanel()
    {
        return panel;
    }
}
