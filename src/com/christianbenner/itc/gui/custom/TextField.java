package com.christianbenner.itc.gui.custom;

import javax.swing.*;
import java.awt.*;

public class TextField extends JTextField
{
    public TextField(final Font font)
    {
        super.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2, true));
        super.setFont(font);
    }
}
