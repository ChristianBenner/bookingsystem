package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;

import javax.swing.*;
import java.awt.*;

public class SelectNewOrExistingTutor extends SelectNewOrExistingPanel
{
    public static final String CARD_NAME = "SelectNewOrExistingTutor";

    public SelectNewOrExistingTutor(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(font, "Tutor");
        super.backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(RoleSelectIntroPanel.CARD_NAME));

        final SelectNewOrExistingTutor referenceToSelf = this;
        super.existingButton.addActionListener(actionEvent ->
        {
            if(Directories.getTutorDirectory().isEmpty())
            {
                JOptionPane.showMessageDialog(referenceToSelf,
                        "No tutors currently exist in the system.",
                        "No Tutors Exist", JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                roleSelectPopup.showCard(RoleSelectExistingTutor.CARD_NAME);
            }
        });
        super.newButton.addActionListener(actionEvent -> roleSelectPopup.showCard(RoleSelectAddTutor.CARD_NAME));
    }
}
