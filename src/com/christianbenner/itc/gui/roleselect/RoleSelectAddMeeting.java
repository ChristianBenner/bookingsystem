package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.data.ParentMeeting;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.window.BookMeetingPanel;
import com.christianbenner.itc.gui.custom.IconButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RoleSelectAddMeeting extends JPanel
{
    public static final String CARD_NAME = "AddMeeting";
    private IconButton backButton;

    public RoleSelectAddMeeting(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(new BorderLayout());

        Font panelFont = font.deriveFont(18f);
        Font buttonFont = font.deriveFont(36f);

        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        BookMeetingPanel bookMeetingPanel = new BookMeetingPanel(panelFont, "Book Meeting");

        bookMeetingPanel.addSelectButtonListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                ParentMeeting parentMeeting = bookMeetingPanel.getSelected();

                // Add student to meeting and close
                if(parentMeeting.isAvailable())
                {
                    parentMeeting.book(bookMeetingPanel.getSelectedStudentId());
                    bookMeetingPanel.refreshMeetingList();

                    JOptionPane.showMessageDialog(bookMeetingPanel, "Successfully booked meeting",
                            "Success", JOptionPane.INFORMATION_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(bookMeetingPanel, "That meeting is full, try another",
                            "Failed", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        backButton = new IconButton(buttonFont, "Back", backIcon);
        backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(ParentMeetingOrTimetable.CARD_NAME));

        super.add(bookMeetingPanel, BorderLayout.CENTER);
        super.add(backButton, BorderLayout.SOUTH);
    }
}
