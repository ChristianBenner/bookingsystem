package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Student;
import com.christianbenner.itc.data.Tutor;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.custom.IconButton;
import com.christianbenner.itc.gui.custom.TextField;
import com.christianbenner.itc.gui.window.StudentWindow;
import com.christianbenner.itc.gui.window.TutorWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

public class RoleSelectExistingTutor extends JPanel
{
    public static final String CARD_NAME = "ExistingTutor";

    private IconButton selectButton;
    private IconButton backButton;

    public RoleSelectExistingTutor(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(new BorderLayout());

        Font sizedFont = font.deriveFont(36f);
        Font buttonFont = font.deriveFont(36f);
        Font panelFont = font.deriveFont(18f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JPanel logOnAndTitlePanel = new JPanel(new BorderLayout());
        logOnAndTitlePanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("Existing Tutor", SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        // Panel for search bar and stuff
        JPanel gridPanel = new JPanel();
        gridPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        JLabel tutorLogonLabel = new JLabel("Tutor Logon: ");
        tutorLogonLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridPanel.add(tutorLogonLabel, gridBagConstraints);

        TextField tutorLogonTextField = new TextField(panelFont);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridPanel.add(tutorLogonTextField, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        logOnAndTitlePanel.add(label, BorderLayout.NORTH);
        logOnAndTitlePanel.add(gridPanel, BorderLayout.CENTER);

        Image selectIcon = GUIUtils.loadImage("/resources/icons/select_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        backButton = new IconButton(buttonFont, "Back", backIcon);
        backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(SelectNewOrExistingTutor.CARD_NAME));

        selectButton = new IconButton(buttonFont, "Login", selectIcon);
        selectButton.addActionListener(actionEvent ->
        {
            // Check if the text field contains data
            if(tutorLogonTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(roleSelectPopup,
                        "Please enter tutor log-in details. Format 'LASTNAMEID' e.g. SMITH255",
                        "Enter Log-in Details",
                        JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                final String input = tutorLogonTextField.getText();

                // Check if any tutors match the information
                List<Tutor> tutorList = Directories.getTutorDirectory().getAll();

                boolean found = false;
                for(Tutor tutor : tutorList)
                {
                    final String nameId = tutor.getLastName().toUpperCase() + Integer.toString(tutor.getUniqueId());
                    if(input.compareTo(nameId) == 0)
                    {
                        // Log-in
                        roleSelectPopup.dispose();
                        showTutorWindow(font, tutor);
                        found = true;
                    }
                }

                if(!found)
                {
                    JOptionPane.showMessageDialog(roleSelectPopup,
                            "No tutor with that username was found, please try again.\nFormat 'LASTNAMEID' " +
                                    "e.g. SMITH255", "Enter Log-in Details", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        panel.add(selectButton);
        panel.add(backButton);

        super.add(logOnAndTitlePanel, BorderLayout.CENTER);
        super.add(panel, BorderLayout.SOUTH);
    }

    private void showTutorWindow(final Font font, final Tutor tutor)
    {
        final TutorWindow tutorWindow = new TutorWindow(font, tutor);
        tutorWindow.setVisible(true);
    }
}
