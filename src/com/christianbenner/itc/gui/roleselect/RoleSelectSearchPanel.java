package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directory;
import com.christianbenner.itc.data.DirectoryObject;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.custom.IconButton;
import com.christianbenner.itc.gui.search.SearchPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class RoleSelectSearchPanel<E extends DirectoryObject> extends JPanel
{
    private SearchPanel<E> searchPanel;
    private IconButton selectButton;
    private IconButton backButton;

    public RoleSelectSearchPanel(final Font font, final Directory directory)
    {
        super(new BorderLayout());

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Image selectIcon = GUIUtils.loadImage("/resources/icons/select_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        Font buttonFont = font.deriveFont(36f);

        selectButton = new IconButton(buttonFont, "Select", selectIcon);
        backButton = new IconButton(buttonFont, "Back", backIcon);

        panel.add(selectButton);
        panel.add(backButton);

        searchPanel = new SearchPanel<>(font, directory);

        super.add(searchPanel, BorderLayout.NORTH);
        super.add(panel, BorderLayout.CENTER);
    }

    public E getSelection()
    {
        return searchPanel.getSelection();
    }

    public void addSelectButtonActionListener(final ActionListener actionListener)
    {
        selectButton.addActionListener(actionListener);
    }

    public void addBackButtonActionListener(final ActionListener actionListener)
    {
        backButton.addActionListener(actionListener);
    }
}
