package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.custom.IconButton;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RoleSelectIntroPanel extends JPanel
{
    public static final String CARD_NAME = "SelectRoleIntroPanel";

    private IconButton tutorButton;
    private IconButton studentButton;
    private IconButton parentButton;
    private IconButton exitButton;

    public RoleSelectIntroPanel(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super.setLayout(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Font buttonFont = font.deriveFont(36f);
        Font sizedFont = font.deriveFont(52f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("Select Role", SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        Image tutorIcon = GUIUtils.loadImage("/resources/icons/tutor_icon.png");
        Image studentIcon = GUIUtils.loadImage("/resources/icons/student_icon.png");
        Image parentIcon = GUIUtils.loadImage("/resources/icons/parent_icon.png");
        Image exitIcon = GUIUtils.loadImage("/resources/icons/exit_icon.png");

        tutorButton = new IconButton(buttonFont, "Tutor", tutorIcon);
        studentButton = new IconButton(buttonFont, "Student", studentIcon);
        parentButton = new IconButton(buttonFont, "Parent", parentIcon);
        exitButton = new IconButton(buttonFont, "Exit", exitIcon);

        tutorButton.addActionListener(actionEvent -> roleSelectPopup.showCard(SelectNewOrExistingTutor.CARD_NAME));
        studentButton.addActionListener(actionEvent -> roleSelectPopup.showCard(SelectNewOrExistingStudent.CARD_NAME));
        parentButton.addActionListener(actionEvent -> roleSelectPopup.showCard(ParentMeetingOrTimetable.CARD_NAME));
        exitButton.addActionListener(actionEvent -> System.exit(0));

        panel.add(tutorButton);
        panel.add(studentButton);
        panel.add(parentButton);
        panel.add(exitButton);

        super.add(panel, BorderLayout.CENTER);
    }

    public void setEnabledAll(boolean state)
    {
        tutorButton.setEnabled(state);
        studentButton.setEnabled(state);
        parentButton.setEnabled(state);
        exitButton.setEnabled(state);
    }

    public void enableUI()
    {
        setEnabledAll(true);
    }

    public void disableUI()
    {
        setEnabledAll(false);
    }
}
