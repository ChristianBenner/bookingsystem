package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.gui.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RoleSelectPopup extends JFrame
{
    private CardLayout cardLayout;
    private JPanel contentPanel;
    private JPanel basePanel;

    public RoleSelectPopup()
    {
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setTitle("Select Role");
        super.getContentPane().setBackground(GUIConstants.BACKGROUND_COLOUR);

        basePanel = new JPanel(new BorderLayout());
        basePanel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 6));
        basePanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        final Font font = GUIUtils.loadFont("/resources/fonts/oswald_regular.ttf");
        final Font companyFont = font.deriveFont(Font.ITALIC).deriveFont(12f);

        JLabel companyText = new JLabel("Inspire Tuition Center", SwingConstants.CENTER);
        companyText.setFont(companyFont);
        companyText.setAlignmentX(Component.CENTER_ALIGNMENT);
        companyText.setBorder(new EmptyBorder(10, 0, 0, 0));

        RoleSelectIntroPanel roleSelectIntroPanel = new RoleSelectIntroPanel(this, font);
        SelectNewOrExistingTutor selectNewOrExistingTutor = new SelectNewOrExistingTutor(this, font);
        RoleSelectExistingTutor roleSelectExistingTutor = new RoleSelectExistingTutor(this, font);
        SelectNewOrExistingStudent selectNewOrExistingStudent = new SelectNewOrExistingStudent(this,
                font);
        RoleSelectExistingStudent roleSelectExistingStudent = new RoleSelectExistingStudent(this, font);
        RoleSelectAddTutor roleSelectAddTutor = new RoleSelectAddTutor(this, font);
        RoleSelectAddStudent roleSelectAddStudent = new RoleSelectAddStudent(this, font);
        RoleSelectExistingStudentAsParent roleSelectExistingStudentAsParent = new RoleSelectExistingStudentAsParent(
                this, font);
        ParentMeetingOrTimetable parentMeetingOrTimetable = new ParentMeetingOrTimetable(this, font);
        RoleSelectAddMeeting roleSelectAddMeeting = new RoleSelectAddMeeting(this, font);

        cardLayout = new CardLayout();
        contentPanel = new JPanel(cardLayout);
        contentPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);
        contentPanel.add(roleSelectIntroPanel, RoleSelectIntroPanel.CARD_NAME);
        contentPanel.add(selectNewOrExistingTutor, SelectNewOrExistingTutor.CARD_NAME);
        contentPanel.add(selectNewOrExistingStudent, SelectNewOrExistingStudent.CARD_NAME);
        contentPanel.add(roleSelectExistingTutor, RoleSelectExistingTutor.CARD_NAME);
        contentPanel.add(roleSelectExistingStudent, RoleSelectExistingStudent.CARD_NAME);
        contentPanel.add(roleSelectAddStudent, RoleSelectAddStudent.CARD_NAME);
        contentPanel.add(roleSelectAddTutor, RoleSelectAddTutor.CARD_NAME);
        contentPanel.add(roleSelectExistingStudentAsParent, RoleSelectExistingStudentAsParent.CARD_NAME);
        contentPanel.add(parentMeetingOrTimetable, ParentMeetingOrTimetable.CARD_NAME);
        contentPanel.add(roleSelectAddMeeting, RoleSelectAddMeeting.CARD_NAME);

        basePanel.add(companyText, BorderLayout.NORTH);
        basePanel.add(contentPanel, BorderLayout.CENTER);
        basePanel.updateUI();

        super.add(basePanel);
        super.setUndecorated(true);
        super.pack();
        super.setSize(400, 600);
        super.setLocationRelativeTo(null);
    }

    public void showCard(final String card)
    {
        cardLayout.show(contentPanel, card);
    }
}
