package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.data.Tutor;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.addpanels.AddTutorPanel;
import com.christianbenner.itc.gui.custom.IconButton;
import com.christianbenner.itc.gui.window.TutorWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RoleSelectAddTutor extends JPanel
{
    public static final String CARD_NAME = "AddTutor";

    private IconButton createButton;
    private IconButton backButton;
    private AddTutorPanel addTutorPanel;

    public RoleSelectAddTutor(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(new BorderLayout());

        Font sizedFont = font.deriveFont(36f);
        Font buttonFont = font.deriveFont(36f);
        Font panelFont = font.deriveFont(18f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JPanel addStudentAndTitlePanel = new JPanel(new BorderLayout());
        addStudentAndTitlePanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("New Tutor", SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        addTutorPanel = new AddTutorPanel(panelFont);

        addStudentAndTitlePanel.add(label, BorderLayout.NORTH);
        addStudentAndTitlePanel.add(addTutorPanel, BorderLayout.CENTER);

        Image selectIcon = GUIUtils.loadImage("/resources/icons/select_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        backButton = new IconButton(buttonFont, "Back", backIcon);
        backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(SelectNewOrExistingTutor.CARD_NAME));

        createButton = new IconButton(buttonFont, "Create", selectIcon);
        createButton.addActionListener(actionEvent ->
        {
            // Check if the fields are valid
            if(addTutorPanel.runValidationChecks())
            {
                Tutor tutor = new Tutor(addTutorPanel.getPrefix(), addTutorPanel.getFirstName(),
                        addTutorPanel.getLastName(),
                        new Address(addTutorPanel.getPostCode(), addTutorPanel.getAddressLineOne(),
                                addTutorPanel.getAddressLineTwo(), addTutorPanel.getAddressLineThree()),
                        addTutorPanel.getPhoneNumber());
                Directories.getTutorDirectory().add(tutor);
                roleSelectPopup.dispose();
                showTutorWindow(font, tutor);
            }
        });

        panel.add(createButton);
        panel.add(backButton);

        super.add(addStudentAndTitlePanel, BorderLayout.CENTER);
        super.add(panel, BorderLayout.SOUTH);
    }

    private void showTutorWindow(final Font font, final Tutor tutor)
    {
        final TutorWindow tutorWindow = new TutorWindow(font, tutor);
        tutorWindow.setVisible(true);
    }
}
