package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Student;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.custom.IconButton;
import com.christianbenner.itc.gui.custom.TextField;
import com.christianbenner.itc.gui.window.ParentWindow;
import com.christianbenner.itc.gui.window.StudentWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

public class RoleSelectExistingStudent extends JPanel
{
    public static final String CARD_NAME = "ExistingStudent";

    private IconButton selectButton;
    private IconButton backButton;

    public RoleSelectExistingStudent(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(new BorderLayout());

        Font sizedFont = font.deriveFont(36f);
        Font buttonFont = font.deriveFont(36f);
        Font panelFont = font.deriveFont(18f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JPanel addStudentAndTitlePanel = new JPanel(new BorderLayout());
        addStudentAndTitlePanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("Existing Student", SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        // Panel for search bar and stuff
        JPanel gridPanel = new JPanel();
        gridPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        JLabel studentLogonLabel = new JLabel("Student Logon: ");
        studentLogonLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridPanel.add(studentLogonLabel, gridBagConstraints);

        TextField studentLogonTextField = new TextField(panelFont);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridPanel.add(studentLogonTextField, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        addStudentAndTitlePanel.add(label, BorderLayout.NORTH);
        addStudentAndTitlePanel.add(gridPanel, BorderLayout.CENTER);

        Image selectIcon = GUIUtils.loadImage("/resources/icons/select_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        backButton = new IconButton(buttonFont, "Back", backIcon);
        backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(SelectNewOrExistingStudent.CARD_NAME));

        selectButton = new IconButton(buttonFont, "Login", selectIcon);
        selectButton.addActionListener(actionEvent ->
        {
            // Check if the text field contains data
            if(studentLogonTextField.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(roleSelectPopup,
                        "Please enter student log-in details. Format 'LASTNAMEID' e.g. SMITH255",
                        "Enter Log-in Details",
                        JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                final String input = studentLogonTextField.getText();

                // Check if any students match the information
                List<Student> studentList = Directories.getStudentDirectory().getAll();

                boolean found = false;
                for(Student student : studentList)
                {
                    final String nameId = student.getLastName().toUpperCase() + Integer.toString(student.getUniqueId());
                    if(input.compareTo(nameId) == 0)
                    {
                        // Log-in
                        roleSelectPopup.dispose();

                        showStudentWindow(font, student);

                        found = true;
                    }
                }

                if(!found)
                {
                    JOptionPane.showMessageDialog(roleSelectPopup,
                            "No student with that username was found, please try again.\nFormat 'LASTNAMEID' " +
                                    "e.g. SMITH255", "Enter Log-in Details", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        panel.add(selectButton);
        panel.add(backButton);

        super.add(addStudentAndTitlePanel, BorderLayout.CENTER);
        super.add(panel, BorderLayout.SOUTH);
    }

    private void showStudentWindow(final Font font, final Student student)
    {
        final StudentWindow studentWindow = new StudentWindow(font, student);
        studentWindow.setVisible(true);
    }
}
