package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.custom.IconButton;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ParentMeetingOrTimetable extends JPanel
{
    public static final String CARD_NAME = "ParentMeetingOrTimetable";

    private IconButton backButton;
    private IconButton tutorMeetingButton;
    private IconButton studentTimetableButton;

    public ParentMeetingOrTimetable(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super.setLayout(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Font buttonFont = font.deriveFont(36f);
        Font optionButtonFont = font.deriveFont(24f);
        Font sizedFont = font.deriveFont(52f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("Select Option", SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        Image meetingIcon = GUIUtils.loadImage("/resources/icons/meeting_icon.png");
        Image timetableIcon = GUIUtils.loadImage("/resources/icons/timetable_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        tutorMeetingButton = new IconButton(optionButtonFont, "Book Tutor Meeting", meetingIcon);
        studentTimetableButton = new IconButton(optionButtonFont, "View Student Timetable", timetableIcon);
        backButton = new IconButton(buttonFont, "Back", backIcon);

        tutorMeetingButton.addActionListener(actionEvent -> roleSelectPopup.showCard(RoleSelectAddMeeting.CARD_NAME));

        final ParentMeetingOrTimetable referenceToSelf = this;
        studentTimetableButton.addActionListener(actionEvent ->
        {
            if(Directories.getStudentDirectory().isEmpty())
            {
                JOptionPane.showMessageDialog(referenceToSelf,
                        "No students currently exist in the system.",
                        "No Students Exist", JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                roleSelectPopup.showCard(RoleSelectExistingStudentAsParent.CARD_NAME);
            }
        });
        backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(RoleSelectIntroPanel.CARD_NAME));

        panel.add(tutorMeetingButton);
        panel.add(studentTimetableButton);
        panel.add(backButton);

        super.add(panel, BorderLayout.CENTER);
    }
}
