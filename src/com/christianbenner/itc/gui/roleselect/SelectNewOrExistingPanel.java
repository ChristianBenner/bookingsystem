package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.custom.IconButton;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SelectNewOrExistingPanel extends JPanel
{
    protected IconButton backButton;
    protected IconButton existingButton;
    protected IconButton newButton;

    public SelectNewOrExistingPanel(final Font font, final String objectName)
    {
        super.setLayout(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Font buttonFont = font.deriveFont(36f);
        Font sizedFont = font.deriveFont(52f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("Select " + objectName, SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        Image searchIcon = GUIUtils.loadImage("/resources/icons/search_icon.png");
        Image newIcon = GUIUtils.loadImage("/resources/icons/add_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        existingButton = new IconButton(buttonFont, "Existing " + objectName, searchIcon);
        newButton = new IconButton(buttonFont, "New " + objectName, newIcon);
        backButton = new IconButton(buttonFont, "Back", backIcon);

        panel.add(existingButton);
        panel.add(newButton);
        panel.add(backButton);

        super.add(panel, BorderLayout.CENTER);
    }

    public void setEnabledAll(boolean state)
    {
        backButton.setEnabled(state);
        existingButton.setEnabled(state);
        newButton.setEnabled(state);
    }

    public void enableUI()
    {
        setEnabledAll(true);
    }

    public void disableUI()
    {
        setEnabledAll(false);
    }

    public IconButton getBackButton()
    {
        return backButton;
    }
}
