package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.data.Student;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.addpanels.AddStudentPanel;
import com.christianbenner.itc.gui.custom.IconButton;
import com.christianbenner.itc.gui.window.StudentWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RoleSelectAddStudent extends JPanel
{
    public static final String CARD_NAME = "AddStudent";

    private IconButton createButton;
    private IconButton backButton;
    private AddStudentPanel addStudentPanel;

    public RoleSelectAddStudent(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(new BorderLayout());

        Font sizedFont = font.deriveFont(52f);
        Font buttonFont = font.deriveFont(36f);
        Font panelFont = font.deriveFont(18f);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JPanel addStudentAndTitlePanel = new JPanel(new BorderLayout());
        addStudentAndTitlePanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel label = new JLabel("New Student", SwingConstants.CENTER);
        label.setFont(sizedFont);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(20, 0, 20, 0));
        panel.add(label);

        addStudentPanel = new AddStudentPanel(panelFont);

        addStudentAndTitlePanel.add(label, BorderLayout.NORTH);
        addStudentAndTitlePanel.add(addStudentPanel, BorderLayout.CENTER);

        Image selectIcon = GUIUtils.loadImage("/resources/icons/select_icon.png");
        Image backIcon = GUIUtils.loadImage("/resources/icons/back_icon.png");

        createButton = new IconButton(buttonFont, "Create", selectIcon);
        backButton = new IconButton(buttonFont, "Back", backIcon);

        panel.add(createButton);
        panel.add(backButton);

        super.add(addStudentAndTitlePanel, BorderLayout.CENTER);
        super.add(panel, BorderLayout.SOUTH);

        backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(SelectNewOrExistingStudent.CARD_NAME));
        createButton.addActionListener(actionEvent ->
        {
            // Check if the fields are valid
            if(addStudentPanel.runValidationChecks())
            {
                // Add the student to the directory
                Student student = new Student(addStudentPanel.getFirstName(), addStudentPanel.getLastName(),
                        new Address(addStudentPanel.getPostCode(), addStudentPanel.getAddressLineOne(),
                                addStudentPanel.getAddressLineTwo(), addStudentPanel.getAddressLineThree()),
                        addStudentPanel.getPhoneNumber());
                Directories.getStudentDirectory().add(student);
                roleSelectPopup.dispose();

                JOptionPane.showMessageDialog(roleSelectPopup,
                        "You have created a new student account with the log-in details '" +
                                student.getLastName().toUpperCase() + Integer.toString(student.getUniqueId()) + "'",
                        "Welcome!", JOptionPane.INFORMATION_MESSAGE);

                showStudentWindow(font, student);
            }
        });
    }

    private void showStudentWindow(final Font font, final Student student)
    {
        final StudentWindow studentWindow = new StudentWindow(font, student);
        studentWindow.setVisible(true);
    }
}
