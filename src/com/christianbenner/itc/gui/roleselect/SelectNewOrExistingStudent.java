package com.christianbenner.itc.gui.roleselect;

import com.christianbenner.itc.Directories;

import javax.swing.*;
import java.awt.*;

public class SelectNewOrExistingStudent extends SelectNewOrExistingPanel
{
    public static final String CARD_NAME = "SelectNewOrExistingStudent";

    public SelectNewOrExistingStudent(final RoleSelectPopup roleSelectPopup, final Font font)
    {
        super(font, "Student");
        super.backButton.addActionListener(actionEvent -> roleSelectPopup.showCard(RoleSelectIntroPanel.CARD_NAME));

        final SelectNewOrExistingStudent referenceToSelf = this;
        super.existingButton.addActionListener(actionEvent ->
        {
            if(Directories.getStudentDirectory().isEmpty())
            {
                JOptionPane.showMessageDialog(referenceToSelf,
                        "No students currently exist in the system.",
                        "No Students Exist", JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                roleSelectPopup.showCard(RoleSelectExistingStudent.CARD_NAME);
            }
        });
        super.newButton.addActionListener(actionEvent -> roleSelectPopup.showCard(RoleSelectAddStudent.CARD_NAME));
    }
}
