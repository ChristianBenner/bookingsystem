package com.christianbenner.itc.gui.window;

import javax.swing.*;
import java.awt.*;

import static com.christianbenner.itc.gui.GUIConstants.BACKGROUND_COLOUR;

public class Window extends JFrame
{
    private static final String WINDOW_TITLE = "Inspire Tuition Centre";

    private JTabbedPane tabbedPane;

    public Window(Font font)
    {
        super(WINDOW_TITLE);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowSize = new Dimension((int)(screenSize.width * 0.8f), (int)(screenSize.height * 0.8f));

        Font tabFont = font.deriveFont(16f);
        tabbedPane = new JTabbedPane();
        tabbedPane.setFont(tabFont);

        tabbedPane.setBackground(BACKGROUND_COLOUR);
        super.setSize(windowSize);
        super.setLocationRelativeTo(null);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.getContentPane().setBackground(BACKGROUND_COLOUR);
        super.setLayout(new BorderLayout());
        super.add(tabbedPane, BorderLayout.CENTER);
    }

    public void addTab(final String name, final JPanel panel)
    {
        tabbedPane.addTab(name, panel);
    }
}
