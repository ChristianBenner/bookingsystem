package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Lesson;
import com.christianbenner.itc.data.Student;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.Button;
import com.christianbenner.itc.gui.custom.ViewPanel;
import com.christianbenner.itc.gui.custom.ViewPopup;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;
import java.util.List;

import static com.christianbenner.itc.gui.GUIConstants.PANEL_COLOUR;

public class ViewStudentList extends ViewPanel
{
    private static final int PREFERRED_WIDTH = 300;
    private static final int PREFERRED_HEIGHT = 300;

    public ViewStudentList(final Font font, final String title, final Lesson lesson, final JFrame frameAncestor,
                           final boolean personalInfoOnStudentView)
    {
        super(font, title);

        JPanel basePanel = new JPanel(new BorderLayout());
        basePanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel lessonLabel = new JLabel(lesson.toString(), JLabel.HORIZONTAL);
        lessonLabel.setBackground(GUIConstants.BACKGROUND_COLOUR);
        lessonLabel.setFont(font);

        DefaultListModel<Student> studentListModel = new DefaultListModel<>();

        // Retrieve list of students from list of IDs
        final List<Student> enrolledStudents = Directories.getStudentDirectory().getList(lesson.getStudents());

        // Populate the list model using the list of students
        for(final Student student : enrolledStudents)
        {
            studentListModel.addElement(student);
        }

        JList<Student> studentList = new JList<>(studentListModel);
        studentList.setFont(font);
        studentList.setSelectionBackground(Color.DARK_GRAY);
        studentList.setSelectionForeground(Color.WHITE);
        studentList.setBackground(PANEL_COLOUR);

        JScrollPane scrollPane = new JScrollPane(studentList);
        scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, false));
        scrollPane.getVerticalScrollBar().setBackground(PANEL_COLOUR);
        scrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI()
        {
            @Override
            protected void configureScrollBarColors()
            {
                this.thumbColor = PANEL_COLOUR.darker();
            }
        });
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        ViewStudentList referenceToSelf = this;

        basePanel.add(lessonLabel, BorderLayout.NORTH);
        basePanel.add(scrollPane, BorderLayout.CENTER);

        if(personalInfoOnStudentView)
        {
            Button viewStudent = new Button("View Student");
            viewStudent.setFont(font);
            viewStudent.setBackground(PANEL_COLOUR);
            viewStudent.setFocusPainted(false);
            viewStudent.setBorderPainted(false);
            viewStudent.addActionListener(actionEvent ->
            {
                if(studentList.getSelectedValue() == null)
                {
                    JOptionPane.showMessageDialog(referenceToSelf,
                            "Please select a student from the list to view details on them.",
                            "Select Student",
                            JOptionPane.WARNING_MESSAGE);
                }
                else
                {
                    ViewMemberDetails viewMemberDetails = new ViewMemberDetails(font, "Student Details",
                            studentList.getSelectedValue());
                    ViewPopup viewPopup = new ViewPopup(frameAncestor, viewMemberDetails, "Student Details");
                    viewPopup.setVisible(true);
                }
            });
            basePanel.add(viewStudent, BorderLayout.SOUTH);
        }

        super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
        super.add(basePanel, BorderLayout.CENTER);
    }
}
