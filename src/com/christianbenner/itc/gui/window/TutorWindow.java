package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.addpanels.*;
import com.christianbenner.itc.gui.custom.PanelPopup;
import com.christianbenner.itc.gui.custom.ViewPopup;
import com.christianbenner.itc.gui.timetable.TutorTimetable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class TutorWindow extends Window
{
    public TutorWindow(final Font font, final Tutor tutor)
    {
        super(font);

        Window referenceToSelf = this;
        TutorTimetable timetable = new TutorTimetable(font, tutor);

        Font panelFont = font.deriveFont(22f);

        // Menu-bar font
        Font menuBarFont = font.deriveFont(18f);

        // Add file menu to the window
        JMenuBar menuBar = new JMenuBar();
        menuBar.setBackground(GUIConstants.PANEL_COLOUR);
        menuBar.setBorderPainted(false);
        menuBar.setFont(menuBarFont);

        JMenu viewMenu = new JMenu("View");
        viewMenu.setBorderPainted(false);
        viewMenu.setFocusPainted(false);
        viewMenu.setFont(menuBarFont);

        JMenuItem viewMyDetails = new JMenuItem("My Details");
        viewMyDetails.setFont(menuBarFont);
        viewMenu.add(viewMyDetails);
        menuBar.add(viewMenu);

        viewMyDetails.addActionListener(actionEvent ->
        {
            ViewMemberDetails viewMemberDetails = new ViewMemberDetails(font, "My Details", tutor);
            ViewPopup viewPopup = new ViewPopup(referenceToSelf, viewMemberDetails, "My Details");
            viewPopup.setVisible(true);
        });

        JMenu createMenu= new JMenu("Create");
        createMenu.setBorderPainted(false);
        createMenu.setFocusPainted(false);
        createMenu.setFont(menuBarFont);
        menuBar.add(createMenu);

        JMenuItem createReport = new JMenuItem("Report");
        createReport.setFont(menuBarFont);
        createMenu.add(createReport);
        createReport.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                CreateReport createReportPanel = new CreateReport(panelFont);

                ViewPopup viewPopup = new ViewPopup(referenceToSelf, createReportPanel, "Create Reprot");
                viewPopup.setVisible(true);
            }
        });

        JMenu addMenu = new JMenu("Add");
        addMenu.setBorderPainted(false);
        addMenu.setFocusPainted(false);
        addMenu.setFont(menuBarFont);

        JMenuItem addRoom = new JMenuItem("Room");
        addRoom.setFont(menuBarFont);
        addRoom.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                AddRoomPanel addRoomPanel = new AddRoomPanel(panelFont);
                PanelPopup panelPopup = new PanelPopup(referenceToSelf, addRoomPanel, "Add Room");

                addRoomPanel.addCreateButtonActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        // Check that the fields are valid
                        if(addRoomPanel.runValidationChecks())
                        {
                            // Add room to directory
                            Room room = new Room(addRoomPanel.getBuildingName(), addRoomPanel.getRoomNumber());
                            Directories.getRoomDirectory().add(room);
                            JOptionPane.showMessageDialog(addRoomPanel, "Successfully added room",
                                    "Success", JOptionPane.INFORMATION_MESSAGE);
                            panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                        }
                    }
                });

                panelPopup.setVisible(true);
            }
        });

        JMenuItem addLesson = new JMenuItem("Lesson");
        addLesson.setFont(menuBarFont);
        addLesson.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                AddLessonPanel addLessonPanel = new AddLessonPanel(panelFont, "Add Lesson", false);
                PanelPopup panelPopup = new PanelPopup(referenceToSelf, addLessonPanel, "Add Lesson");

                addLessonPanel.setTutorComboBoxDisplay(tutor);
                addLessonPanel.setEnabledTutorComboBox(false);
                addLessonPanel.addCreateButtonActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        // Validate the entered data
                        if(addLessonPanel.runValidationChecks())
                        {
                            // Create and add lesson to directory
                            Lesson lesson = new Lesson(addLessonPanel.getSelectedTutorId(),
                                    addLessonPanel.getSelectedRoomId(), addLessonPanel.getSelectedSubjectId(),
                                    addLessonPanel.getSessionTime(),
                                    addLessonPanel.getMaxSpaces());

                            Directories.getLessonDirectory().add(lesson);

                            JOptionPane.showMessageDialog(addLessonPanel, "Successfully added lesson",
                                    "Success", JOptionPane.INFORMATION_MESSAGE);
                            panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                            timetable.refreshInfo();
                        }
                    }
                });

                panelPopup.setVisible(true);
            }
        });

        JMenuItem addSubject = new JMenuItem("Subject");
        addSubject.setFont(menuBarFont);
        addSubject.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                AddSubjectPanel addSubjectPanel = new AddSubjectPanel(panelFont);
                PanelPopup panelPopup = new PanelPopup(referenceToSelf, addSubjectPanel, "Add Subject");

                addSubjectPanel.addCreateButtonActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        // Validate the entered data
                        if(addSubjectPanel.runValidationChecks())
                        {
                            Subject subject = new Subject(addSubjectPanel.getSubjectName());
                            Directories.getSubjectDirectory().add(subject);
                            JOptionPane.showMessageDialog(addSubjectPanel, "Successfully added subject",
                                    "Success", JOptionPane.INFORMATION_MESSAGE);
                            panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                        }
                    }
                });

                panelPopup.setVisible(true);
            }
        });

        JMenuItem addStudent = new JMenuItem("Student");
        addStudent.setFont(menuBarFont);
        addStudent.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                AddStudentPanel addMemberPanel = new AddStudentPanel(panelFont, "Student", true);
                PanelPopup panelPopup = new PanelPopup(referenceToSelf, addMemberPanel, "Add Student");

                addMemberPanel.addCreateButtonActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        // Check if the fields are valid
                        if(addMemberPanel.runValidationChecks())
                        {
                            // Add the student to the directory
                            Student student = new Student(addMemberPanel.getFirstName(), addMemberPanel.getLastName(),
                                    new Address(addMemberPanel.getPostCode(), addMemberPanel.getAddressLineOne(),
                                            addMemberPanel.getAddressLineTwo(), addMemberPanel.getAddressLineThree()),
                                    addMemberPanel.getPhoneNumber());
                            Directories.getStudentDirectory().add(student);
                            JOptionPane.showMessageDialog(addMemberPanel, "Successfully added student",
                                    "Success", JOptionPane.INFORMATION_MESSAGE);
                            panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                        }
                    }
                });

                panelPopup.setVisible(true);
            }
        });

        JMenuItem bookMeeting = new JMenuItem("Office Hour");
        bookMeeting.setFont(menuBarFont);
        bookMeeting.setBorderPainted(false);
        bookMeeting.setFocusPainted(false);
        addMenu.add(bookMeeting);
        bookMeeting.addActionListener(actionEvent ->
        {
            // If there is a lesson there and you are viewing the timetable as a parent book a meeting
            AddOfficeHourPanel addMeetingPanel = new AddOfficeHourPanel(font, "Create Office Hour", tutor);

            PanelPopup panelPopup = new PanelPopup((JFrame) SwingUtilities.getWindowAncestor(referenceToSelf),
                    addMeetingPanel, "Create Office Hour");

            addMeetingPanel.addCreateButtonActionListener(actionEvent1 ->
            {
                // Validate the entered data
                if (addMeetingPanel.runValidationChecks())
                {
                    // Create three parent meeting slots
                    for(int i = 0; i < ParentMeeting.MAX_SLOTS; i++)
                    {
                        // Create and add parent meeting to directory
                        ParentMeeting parentMeeting = new ParentMeeting(tutor.getUniqueId(),
                                addMeetingPanel.getRoomId(),
                                addMeetingPanel.getSessionTime(), i);
                        Directories.getParentMeetingDirectory().add(parentMeeting);
                    }

                    JOptionPane.showMessageDialog(addMeetingPanel, "Successfully added meeting",
                            "Success", JOptionPane.INFORMATION_MESSAGE);
                    panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                    timetable.refreshInfo();
                }
            });

            panelPopup.setVisible(true);
        });



        menuBar.add(addMenu);
        addMenu.add(addRoom);
        addMenu.add(addLesson);
        addMenu.add(addSubject);
        addMenu.add(addStudent);
        add(menuBar, BorderLayout.NORTH);
        add(timetable);
    }
}
