package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.Button;
import com.christianbenner.itc.gui.custom.ViewPopup;
import com.christianbenner.itc.gui.search.SearchPanel;
import com.christianbenner.itc.gui.search.SearchPopup;
import com.christianbenner.itc.gui.search.SearchPopupTabbed;
import com.christianbenner.itc.gui.timetable.StudentTimetable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.List;

public class StudentWindow extends Window
{
    private StudentTimetable timetable;
    private SearchPopupTabbed searchPopupTabbed;
    private Student student;

    class EnrolButtonActionListener<E extends DirectoryObject> implements ActionListener
    {
        private SearchPanel<E> searchPanel;

        public EnrolButtonActionListener(SearchPanel<E> searchPanel)
        {
            this.searchPanel = searchPanel;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            // Check if the student is already on the lesson before adding them to it again
            final Lesson selectedLesson = Directories.getLessonDirectory().get(
                    searchPanel.getSelection().getUniqueId());

            if(selectedLesson.containsStudent(student.getUniqueId()))
            {
                JOptionPane.showMessageDialog(searchPanel,
                        "You are already enrolled on this lesson", "Already Enrolled",
                        JOptionPane.WARNING_MESSAGE);
            }
            else
            {
                if(selectedLesson.addStudent(student.getUniqueId()))
                {
                    timetable.refreshInfo();
                    JOptionPane.showMessageDialog(searchPanel,
                            "Successfully enrolled student from lesson", "Success",
                            JOptionPane.INFORMATION_MESSAGE);
                    searchPopupTabbed.dispatchEvent(new WindowEvent(searchPopupTabbed, WindowEvent.WINDOW_CLOSING));
                }
                else
                {
                    final String tutorPhoneNumber = Directories.getTutorDirectory().get(
                            selectedLesson.getTutorId()).getPhoneNumber();

                    JOptionPane.showMessageDialog(searchPopupTabbed,
                            "Unfortunately there is no remaining spaces for students on this " +
                                    "lesson (" + selectedLesson.getSpacesLeft() + "/" +
                                    selectedLesson.getMaxSpaces() + ").\nPlease contact the tutor for any " +
                                    "assistance: " + tutorPhoneNumber, "No Space!",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    public StudentWindow(final Font font, final Student student)
    {
        super(font);

        this.student = student;

        final Window referenceToSelf = this;
        timetable = new StudentTimetable(font, student);

        // Menu-bar font
        Font menuBarFont = font.deriveFont(18f);

        // Add file menu to the window
        JMenuBar menuBar = new JMenuBar();
        menuBar.setFont(menuBarFont);
        menuBar.setBackground(GUIConstants.PANEL_COLOUR);
        menuBar.setBorderPainted(false);

        JMenu viewMenu = new JMenu("View");
        viewMenu.setFont(menuBarFont);
        viewMenu.setBackground(GUIConstants.PANEL_COLOUR);
        viewMenu.setBorderPainted(false);
        viewMenu.setFocusPainted(false);

        JMenuItem viewMyDetails = new JMenuItem("My Details");
        viewMyDetails.setBackground(GUIConstants.PANEL_COLOUR);
        viewMyDetails.setFont(menuBarFont);
        viewMenu.add(viewMyDetails);
        menuBar.add(viewMenu);

        viewMyDetails.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                ViewMemberDetails viewMemberDetails = new ViewMemberDetails(font, "My Details", student);
                ViewPopup viewPopup = new ViewPopup(referenceToSelf, viewMemberDetails, "My Details");
                viewPopup.setVisible(true);
            }
        });

        JMenu findMenu = new JMenu("Find");
        findMenu.setBackground(GUIConstants.PANEL_COLOUR.darker());
        findMenu.setFont(menuBarFont);
        viewMenu.setBorderPainted(false);
        viewMenu.setFocusPainted(false);

        JMenuItem findLesson = new JMenuItem("Find Lesson");
        findLesson.setBackground(GUIConstants.PANEL_COLOUR);
        findLesson.setFont(menuBarFont);
        findLesson.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                searchPopupTabbed = new SearchPopupTabbed(referenceToSelf, "Lesson");

                SearchPanel<Lesson> lessonSearchPanel = new SearchPanel<>(font, Directories.getLessonDirectory());

                Button enrolButtonLessonSearchPanel = new Button("Enrol");
                enrolButtonLessonSearchPanel.setBackground(GUIConstants.PANEL_COLOUR);
                enrolButtonLessonSearchPanel.setFont(menuBarFont);
                enrolButtonLessonSearchPanel.addActionListener(new EnrolButtonActionListener<>(lessonSearchPanel));
                lessonSearchPanel.add(enrolButtonLessonSearchPanel, BorderLayout.SOUTH);

                searchPopupTabbed.addTab("Lesson by String", lessonSearchPanel);

                // Lesson by tutor
                SearchPanel<Lesson> lessonSearchPanelByTutor = new SearchPanel<>(font, Directories.getLessonDirectory(), false);

                Button enrolButtonLessonSearchPanelByTutor = new Button("Enrol");
                enrolButtonLessonSearchPanelByTutor.setBackground(GUIConstants.PANEL_COLOUR);
                enrolButtonLessonSearchPanelByTutor.setFont(menuBarFont);
                enrolButtonLessonSearchPanelByTutor.addActionListener(new EnrolButtonActionListener<>(lessonSearchPanelByTutor));
                lessonSearchPanelByTutor.add(enrolButtonLessonSearchPanelByTutor, BorderLayout.SOUTH);

                JComboBox<Tutor> tutorComboBox = new JComboBox<>();
                tutorComboBox.setBackground(GUIConstants.BACKGROUND_COLOUR);
                tutorComboBox.setFont(menuBarFont);
                java.util.List<Tutor> tutorList = Directories.getTutorDirectory().getAll();
                for(Tutor tutor : tutorList)
                {
                    tutorComboBox.addItem(tutor);
                }
                tutorComboBox.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        lessonSearchPanelByTutor.setExistingSearchString(((Tutor)tutorComboBox.getSelectedItem()).
                                getFullName());
                    }
                });
                lessonSearchPanelByTutor.setExistingSearchString(((Tutor)tutorComboBox.getSelectedItem()).
                        getFullName());
                lessonSearchPanelByTutor.add(tutorComboBox, BorderLayout.NORTH);
                searchPopupTabbed.addTab("Lesson by Tutor", lessonSearchPanelByTutor);

                // Lesson by subject
                SearchPanel<Lesson> lessonSearchPanelBySubject = new SearchPanel<>(font,
                        Directories.getLessonDirectory(), false);

                Button enrolButtonLessonSearchPanelBySubject = new Button("Enrol");
                enrolButtonLessonSearchPanelBySubject.setBackground(GUIConstants.PANEL_COLOUR);
                enrolButtonLessonSearchPanelBySubject.setFont(menuBarFont);
                enrolButtonLessonSearchPanelBySubject.addActionListener(new EnrolButtonActionListener<>(
                        lessonSearchPanelBySubject));
                lessonSearchPanelBySubject.add(enrolButtonLessonSearchPanelBySubject, BorderLayout.SOUTH);

                JComboBox<Subject> subjectComboBox = new JComboBox<>();
                subjectComboBox.setBackground(GUIConstants.BACKGROUND_COLOUR);
                subjectComboBox.setFont(menuBarFont);
                List<Subject> subjectList = Directories.getSubjectDirectory().getAll();
                for(Subject subject : subjectList)
                {
                    subjectComboBox.addItem(subject);
                }
                subjectComboBox.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        lessonSearchPanelBySubject.setExistingSearchString(((Subject)subjectComboBox.getSelectedItem()).
                                getName());
                    }
                });
                lessonSearchPanelBySubject.setExistingSearchString(((Subject)subjectComboBox.getSelectedItem()).
                        getName());
                lessonSearchPanelBySubject.add(subjectComboBox, BorderLayout.NORTH);
                searchPopupTabbed.addTab("Lesson by Subject", lessonSearchPanelBySubject);

                searchPopupTabbed.setVisible(true);
            }
        });

        add(timetable);
        menuBar.add(findMenu);
        findMenu.add(findLesson);
        add(menuBar, BorderLayout.NORTH);
    }
}
