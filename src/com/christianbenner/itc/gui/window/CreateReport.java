package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.Button;
import com.christianbenner.itc.gui.custom.ViewPanel;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class CreateReport extends ViewPanel
{
    private static final int PREFERRED_WIDTH = 330;
    private static final int PREFERRED_HEIGHT = 400;

    private boolean sessionTimeWithinFourWeeksAgo(SessionTime sessionTime)
    {
        long currentTimeMillis = new GregorianCalendar().getTimeInMillis();

        GregorianCalendar fourWeeksAgo = new GregorianCalendar();
        fourWeeksAgo.add(GregorianCalendar.HOUR, -24 * 7 * 4);
        long fourWeeksAgoMillis = fourWeeksAgo.getTimeInMillis();
        long sessionTimeMillis = new GregorianCalendar(sessionTime.getYear(),
                sessionTime.getMonth() - 1, sessionTime.getDay(),
                sessionTime.getHoursFromDayStart() + SessionTime.DAY_START_HOURS,
                0).getTimeInMillis();

        if(sessionTimeMillis > fourWeeksAgoMillis && sessionTimeMillis < currentTimeMillis)
        {
            return true;
        }

        return false;
    }

    private List<Lesson> getLastFourWeeksOfLessons()
    {
        final List<Lesson> lessons = Directories.getLessonDirectory().getAll();

        final List<Lesson> lastFourWeeks = new ArrayList<>();

        // Lessons repeat weekly so add lessons 4 times
        for(int i = 0; i < 4; i++)
        {
            for(Lesson lesson : lessons)
            {
                lastFourWeeks.add(lesson);
            }
        }

        return lastFourWeeks;
    }

    private List<ParentMeeting> getLastFourWeeksOfParentMeetings()
    {
        final List<ParentMeeting> parentMeetings = Directories.getParentMeetingDirectory().getAll();

        final List<ParentMeeting> lastFourWeeks = new ArrayList<>();

        for(ParentMeeting parentMeeting : parentMeetings)
        {
            if(sessionTimeWithinFourWeeksAgo(parentMeeting.getSessionTime()))
            {
                lastFourWeeks.add(parentMeeting);
            }
        }

        return lastFourWeeks;
    }

    private String createReport()
    {
        String reportString = "";

        int totalNumberOfStudentsEnrolled = 0;

        final List<Lesson> lessons = getLastFourWeeksOfLessons();
        final List<ParentMeeting> parentMeetings = getLastFourWeeksOfParentMeetings();

        reportString += "Lessons Created = " + lessons.size() + "\n";
        reportString += "Parent Meetings Created = " + parentMeetings.size() + "\n";

        for(Lesson lesson : lessons)
        {
            totalNumberOfStudentsEnrolled += lesson.getNumberOfStudentsEnrolled();

            final Tutor lessonTutor = Directories.getTutorDirectory().get(lesson.getTutorId());
            final Subject lessonSubject = Directories.getSubjectDirectory().get(lesson.getSubjectId());
            final Room lessonRoom = Directories.getRoomDirectory().get(lesson.getRoomId());
            final List<Student> lessonStudents = Directories.getStudentDirectory().getList(lesson.getStudents());
            final SessionTime lessonTime = lesson.getSessionTime();

            reportString += "Lesson:\n";
            reportString += "\tID: " + lesson.getUniqueId() + "\n";
            reportString += "\tTutor: " + lessonTutor.getFullName() + "\n";
            reportString += "\tSubject: " + lessonSubject.getName() + "\n";
            reportString += "\tRoom: " + lessonRoom.toString() + "\n";
            reportString += "\tTime: " + lessonTime.toString() + "\n";
            reportString += "\tMax Spaces: " + lesson.getMaxSpaces() + "\n";
            reportString += "\tNumber of enrolled students: " + lesson.getNumberOfStudentsEnrolled() + "\n";

            if(lessonStudents.isEmpty())
            {
                reportString += "\tStudents: NONE\n";
            }
            else
            {
                reportString += "\tStudents:\n";

                for(Student student : lessonStudents)
                {
                    reportString += "\t\tStudent: " + student.getFullName() + "\n";
                }
            }
        }

        for(ParentMeeting parentMeeting : parentMeetings)
        {
            final Tutor parentMeetingTutor = Directories.getTutorDirectory().get(parentMeeting.getTutorId());
            final Room parentMeetingRoom = Directories.getRoomDirectory().get(parentMeeting.getRoomId());

            reportString += "Parent Meeting:\n";
            reportString += "\tTutor: " + parentMeetingTutor.getFullName() + "\n";
            reportString += "\tRoom: " + parentMeetingRoom.toString() + "\n";
            reportString += "\tTime: " + parentMeeting.getAvailableInfo() + "\n";
            reportString += "\tSlot: " + parentMeeting.getSlot() + "\n";
            if(!parentMeeting.isAvailable())
            {
                final Student parentMeetingStudent = Directories.getStudentDirectory().get(parentMeeting.getStudentId());
                reportString += "\tStudent: " + parentMeetingStudent.getFullName() + "\n";
            }
        }

        reportString += "Total Number of Students Enrolled: " + totalNumberOfStudentsEnrolled + "\n";

        return reportString;
    }

    public CreateReport(Font font)
    {
        super(font, "Create Report");

        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JLabel createInfo = new JLabel("Create a report of the last 4 weeks", JLabel.HORIZONTAL);
        createInfo.setFont(font);
        panel.add(createInfo, BorderLayout.NORTH);

        JTextArea textArea = new JTextArea();
        textArea.setFont(font.deriveFont(12f));
        textArea.setBackground(GUIConstants.PANEL_COLOUR);

        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setBackground(GUIConstants.PANEL_COLOUR);
        panel.add(scrollPane, BorderLayout.CENTER);

        Button createReportButton = new Button("Create Report");
        createReportButton.setFont(font);
        createReportButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                SwingUtilities.invokeLater(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String report = createReport();
                        System.out.println(report);
                        textArea.append(report);
                    }
                });
                panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });
        panel.add(createReportButton, BorderLayout.SOUTH);

        super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
        super.add(panel, BorderLayout.CENTER);
    }
}
