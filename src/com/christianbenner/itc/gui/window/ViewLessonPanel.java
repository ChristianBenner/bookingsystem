package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.Button;
import com.christianbenner.itc.gui.custom.ViewPanel;
import com.christianbenner.itc.gui.custom.ViewPopup;

import javax.swing.*;
import java.awt.*;

public class ViewLessonPanel extends ViewPanel
{
    public ViewLessonPanel(final Font font, final Lesson lesson, final JFrame frameAncestor,
                           final boolean personalInfoStudentLessonView)
    {
        super(font, "View Lesson");

        JPanel gridPanel = new JPanel(new GridLayout(0, 2));
        gridPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Font panelFont = font.deriveFont(22f);
        Font infoFont = font.deriveFont(22f);

        JLabel tutorLabel = new JLabel("Tutor: ", JLabel.HORIZONTAL);
        tutorLabel.setFont(panelFont);
        gridPanel.add(tutorLabel);

        final Tutor tutor = Directories.getTutorDirectory().get(lesson.getTutorId());
        JLabel tutorInfoLabel = new JLabel(tutor.getPrefix() + " " + tutor.getLastName(),
                JLabel.HORIZONTAL);
        tutorInfoLabel.setFont(infoFont);
        tutorInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(tutorInfoLabel);

        final Room room = Directories.getRoomDirectory().get(lesson.getRoomId());
        JLabel roomBuildingNameLabel = new JLabel("Building Name: ", JLabel.HORIZONTAL);
        roomBuildingNameLabel.setFont(panelFont);
        gridPanel.add(roomBuildingNameLabel);

        JLabel roomBuildingNameInfoLabel = new JLabel(room.getBuildingName(), JLabel.HORIZONTAL);
        roomBuildingNameInfoLabel.setFont(infoFont);
        roomBuildingNameInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(roomBuildingNameInfoLabel);

        JLabel roomNumberLabel = new JLabel("Room Number: ", JLabel.HORIZONTAL);
        roomNumberLabel.setFont(panelFont);
        gridPanel.add(roomNumberLabel);

        JLabel roomNumberInfoLabel = new JLabel(Integer.toString(room.getRoomNumber()), JLabel.HORIZONTAL);
        roomNumberInfoLabel.setFont(infoFont);
        roomNumberInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(roomNumberInfoLabel);

        JLabel dayLabel = new JLabel("Day: ", JLabel.HORIZONTAL);
        dayLabel.setFont(panelFont);
        gridPanel.add(dayLabel);

        JLabel dayInfoLabel = new JLabel(SessionTime.getDayString(lesson.getSessionTime()), JLabel.HORIZONTAL);
        dayInfoLabel.setFont(infoFont);
        dayInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(dayInfoLabel);

        JLabel timeLabel = new JLabel("Time: ", JLabel.HORIZONTAL);
        timeLabel.setFont(panelFont);
        gridPanel.add(timeLabel);

        JLabel timeInfoLabel = new JLabel(SessionTime.getTimeString(lesson.getSessionTime().getHoursFromDayStart()),
                JLabel.HORIZONTAL);
        timeInfoLabel.setFont(infoFont);
        timeInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(timeInfoLabel);

        JLabel subjectLabel = new JLabel("Subject: ", JLabel.HORIZONTAL);
        subjectLabel.setFont(panelFont);
        gridPanel.add(subjectLabel);

        JLabel subjectInfoLabel = new JLabel(Directories.getSubjectDirectory().get(lesson.getSubjectId()).getName(),
                JLabel.HORIZONTAL);
        subjectInfoLabel.setFont(infoFont);
        subjectInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(subjectInfoLabel);

        JLabel enrolledLabel = new JLabel("Enrolled: ", JLabel.HORIZONTAL);
        enrolledLabel.setFont(panelFont);
        gridPanel.add(enrolledLabel);

        JLabel enrolledInfoLabel = new JLabel(lesson.getNumberOfStudentsEnrolled() + "/" + lesson.getMaxSpaces(),
                JLabel.HORIZONTAL);
        enrolledInfoLabel.setFont(infoFont);
        enrolledInfoLabel.setForeground(Color.GRAY);
        gridPanel.add(enrolledInfoLabel);

        JLabel studentsLabel = new JLabel("Students: ", JLabel.HORIZONTAL);
        studentsLabel.setFont(panelFont);
        gridPanel.add(studentsLabel);

        Button viewStudents = new Button("View");
        viewStudents.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, false));
        viewStudents.setFocusPainted(false);
        viewStudents.setForeground(Color.BLACK);
        viewStudents.setBackground(GUIConstants.PANEL_COLOUR);
        viewStudents.setFont(panelFont);

        ViewLessonPanel referenceToSelf = this;
        viewStudents.addActionListener(actionEvent ->
        {
            if(lesson.getNumberOfStudentsEnrolled() == 0)
            {
                JOptionPane.showMessageDialog(referenceToSelf,
                        "There are currently no students enrolled on this lesson",
                        "No Students Enrolled",
                        JOptionPane.INFORMATION_MESSAGE);
            }
            else
            {
                ViewStudentList viewStudentList = new ViewStudentList(panelFont, "Enrolled Students", lesson,
                        frameAncestor, personalInfoStudentLessonView);
                ViewPopup viewPopup = new ViewPopup(frameAncestor, viewStudentList, "Enrolled Students");
                viewPopup.setVisible(true);
            }
        });

        gridPanel.add(viewStudents);

        super.setPreferredSize(new Dimension(400, 360));
        super.add(gridPanel, BorderLayout.CENTER);
    }
}
