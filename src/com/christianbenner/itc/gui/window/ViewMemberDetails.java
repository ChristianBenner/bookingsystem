package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.data.Member;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.ViewPanel;

import javax.swing.*;
import java.awt.*;

public class ViewMemberDetails extends ViewPanel
{
    private static final int PREFERRED_WIDTH = 350;
    private static final int PREFERRED_HEIGHT = 350;

    private static final int ROWS = 0;
    private static final int COLUMNS = 2;

    public ViewMemberDetails(final Font font, final String title, final Member member)
    {
        super(font, title);

        final Font infoFont = font.deriveFont(22f);

        JPanel memberDetailsPanel = new JPanel(new GridLayout(ROWS, COLUMNS));
        memberDetailsPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        /// Unique ID
        JLabel idLabel = new JLabel("ID: ", JLabel.HORIZONTAL);
        idLabel.setFont(infoFont);
        memberDetailsPanel.add(idLabel);

        JLabel idInfoLabel = new JLabel(Integer.toString(member.getUniqueId()), JLabel.HORIZONTAL);
        idInfoLabel.setFont(infoFont);
        idInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(idInfoLabel);

        /// First name
        JLabel firstNameLabel = new JLabel("First Name: ", JLabel.HORIZONTAL);
        firstNameLabel.setFont(infoFont);
        memberDetailsPanel.add(firstNameLabel);

        JLabel firstNameInfoLabel = new JLabel(member.getFirstName(), JLabel.HORIZONTAL);
        firstNameInfoLabel.setFont(infoFont);
        firstNameInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(firstNameInfoLabel);

        // Last name
        JLabel lastNameLabel = new JLabel("Last Name: ", JLabel.HORIZONTAL);
        lastNameLabel.setFont(infoFont);
        memberDetailsPanel.add(lastNameLabel);

        JLabel lastNameInfoLabel = new JLabel(member.getLastName(), JLabel.HORIZONTAL);
        lastNameInfoLabel.setFont(infoFont);
        lastNameInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(lastNameInfoLabel);

        // Post code
        JLabel postCodeLabel = new JLabel("Post Code: ", JLabel.HORIZONTAL);
        postCodeLabel.setFont(infoFont);
        memberDetailsPanel.add(postCodeLabel);

        JLabel postCodeInfoLabel = new JLabel(member.getAddress().getPostCode(), JLabel.HORIZONTAL);
        postCodeInfoLabel.setFont(infoFont);
        postCodeInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(postCodeInfoLabel);

        // Address Line One
        JLabel addressLineOneLabel = new JLabel("Address Line 1: ", JLabel.HORIZONTAL);
        addressLineOneLabel.setFont(infoFont);
        memberDetailsPanel.add(addressLineOneLabel);

        JLabel addressLineOneInfoLabel = new JLabel(member.getAddress().getAddressLineOne(), JLabel.HORIZONTAL);
        addressLineOneInfoLabel.setFont(infoFont);
        addressLineOneInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(addressLineOneInfoLabel);

        // Address Line Two
        JLabel addressLineTwoLabel = new JLabel("Address Line 2: ", JLabel.HORIZONTAL);
        addressLineTwoLabel.setFont(infoFont);
        memberDetailsPanel.add(addressLineTwoLabel);

        JLabel addressLineTwoInfoLabel = new JLabel(member.getAddress().getAddressLineTwo(), JLabel.HORIZONTAL);
        addressLineTwoInfoLabel.setFont(infoFont);
        addressLineTwoInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(addressLineTwoInfoLabel);

        // Address Line Three (is commonly empty so don't show if its empty)
        if(!member.getAddress().getAddressLineThree().isEmpty())
        {
            JLabel addressLineThreeLabel = new JLabel("Address Line 3: ", JLabel.HORIZONTAL);
            addressLineThreeLabel.setFont(infoFont);
            memberDetailsPanel.add(addressLineThreeLabel);

            JLabel addressLineThreeInfoLabel = new JLabel(member.getAddress().getAddressLineThree(), JLabel.HORIZONTAL);
            addressLineThreeInfoLabel.setFont(infoFont);
            addressLineThreeInfoLabel.setForeground(Color.GRAY);
            memberDetailsPanel.add(addressLineThreeInfoLabel);
        }

        // Phone Number
        JLabel phoneNumberLabel = new JLabel("Phone Number: ", JLabel.HORIZONTAL);
        phoneNumberLabel.setFont(infoFont);
        memberDetailsPanel.add(phoneNumberLabel);

        JLabel phoneNumbeInfoLabel = new JLabel(member.getPhoneNumber(), JLabel.HORIZONTAL);
        phoneNumbeInfoLabel.setFont(infoFont);
        phoneNumbeInfoLabel.setForeground(Color.GRAY);
        memberDetailsPanel.add(phoneNumbeInfoLabel);

        // Set preferred size
        super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));

        // Add panel to parent
        super.add(memberDetailsPanel, BorderLayout.CENTER);
    }
}
