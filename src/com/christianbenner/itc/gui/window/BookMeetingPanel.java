package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.ValidationUtils;
import com.christianbenner.itc.gui.custom.Button;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static com.christianbenner.itc.gui.GUIConstants.PANEL_COLOUR;

public class BookMeetingPanel extends JPanel
{
    private static final int PREFERRED_WIDTH = 500;
    private static final int PREFERRED_HEIGHT = 500;

    private JComboBox<Tutor> tutorComboBox;
    private JComboBox<Student> studentJComboBox;

    private List<ParentMeeting> parentMeetings;
    private JList<String> parentMeetingJList;
    private JButton selectButton;

    public void refreshMeetingList()
    {
        if(tutorComboBox.getSelectedItem() != null && tutorComboBox.getSelectedItem() instanceof Tutor)
        {
            // Refresh the list of available dates
            final Tutor selectedTutor = (Tutor)tutorComboBox.getSelectedItem();

            // Get list of parent meetings for that tutor and populate the list
            List<ParentMeeting> allParentMeetings = Directories.getParentMeetingDirectory().getAll();

            DefaultListModel<String> defaultListModel = new DefaultListModel<>();

            parentMeetings = new ArrayList<>();
            for(ParentMeeting parentMeeting : allParentMeetings)
            {
                if(parentMeeting.getTutorId() == selectedTutor.getUniqueId() && parentMeeting.isAvailable())
                {
                    // Put it in the list
                    parentMeetings.add(parentMeeting);
                    defaultListModel.addElement(parentMeeting.getAvailableInfo());
                }
            }

            parentMeetingJList.setModel(defaultListModel);
        }
    }

    public BookMeetingPanel(Font font, String text)
    {
        super(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        Font textFont = font.deriveFont(28f);

        JLabel titleLabel = new JLabel(text, JLabel.HORIZONTAL);
        titleLabel.setFont(textFont);

        selectButton = new Button("Book");
        selectButton.setBackground(GUIConstants.PANEL_COLOUR);
        selectButton.setFont(textFont);


        JPanel gridBagPanel = new JPanel(new GridBagLayout());
        gridBagPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        Font panelFont = font.deriveFont(22f);

        JLabel tutorLabel = new JLabel("Tutor: ");
        tutorLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagPanel.add(tutorLabel, gridBagConstraints);

        parentMeetingJList = new JList<>();
        tutorComboBox = new JComboBox<>();
        tutorComboBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                refreshMeetingList();
            }
        });
        tutorComboBox.setFont(panelFont);
        List<Tutor> tutorList = Directories.getTutorDirectory().getAll();
        for(Tutor tutor : tutorList)
        {
            tutorComboBox.addItem(tutor);
        }
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagPanel.add(tutorComboBox, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel studentLabel = new JLabel("Student: ");
        studentLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(studentLabel, gridBagConstraints);

        studentJComboBox = new JComboBox<>();
        studentJComboBox.setFont(panelFont);
        List<Student> studentList = Directories.getStudentDirectory().getAll();
        for(Student student : studentList)
        {
            studentJComboBox.addItem(student);
        }
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 1;
        gridBagPanel.add(studentJComboBox, gridBagConstraints);
        gridBagConstraints.weightx = 0;


        JLabel availableSlots = new JLabel("Available: ");
        availableSlots.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(availableSlots, gridBagConstraints);

        parentMeetingJList.setFont(panelFont);
        parentMeetingJList.setBackground(GUIConstants.BACKGROUND_COLOUR);

        JScrollPane scrollPane = new JScrollPane(parentMeetingJList);
        scrollPane.setBackground(GUIConstants.BACKGROUND_COLOUR);
        scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, false));
        scrollPane.getVerticalScrollBar().setBackground(PANEL_COLOUR);
        scrollPane.getVerticalScrollBar().setUI(new BasicScrollBarUI()
        {
            @Override
            protected void configureScrollBarColors()
            {
                this.thumbColor = PANEL_COLOUR.darker();
            }
        });
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        gridBagConstraints.gridx = 1;
        gridBagPanel.add(scrollPane, gridBagConstraints);

        super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
        super.add(titleLabel, BorderLayout.NORTH);
        super.add(gridBagPanel, BorderLayout.CENTER);
        super.add(selectButton, BorderLayout.SOUTH);
    }

    public void addSelectButtonListener(ActionListener actionListener)
    {
        selectButton.addActionListener(actionListener);
    }

    // Parent meeting and slot
    public ParentMeeting getSelected()
    {
        return parentMeetings.get(parentMeetingJList.getSelectedIndex());
    }

    public int getSelectedTutorId()
    {
        return ((Tutor)tutorComboBox.getSelectedItem()).getUniqueId();
    }

    public int getSelectedStudentId()
    {
        return ((Student)studentJComboBox.getSelectedItem()).getUniqueId();
    }

    public void setStudentComboBoxEnabled(boolean enabled)
    {
        studentJComboBox.setEnabled(enabled);
    }

    public void setSelectedStudent(final Student student)
    {
        studentJComboBox.setSelectedItem(student);
    }

    public boolean runValidationChecks()
    {
        // Check that the selected tutor is not null and is of type tutor
        if(tutorComboBox.getSelectedItem() == null || !(tutorComboBox.getSelectedItem() instanceof Tutor))
        {
            ValidationUtils.showNoSelectionFailure(this, "Tutor");
            return false;
        }

        // Check that the selected student is not null and is of type student
        if(studentJComboBox.getSelectedItem() == null || !(studentJComboBox.getSelectedItem() instanceof Student))
        {
            ValidationUtils.showNoSelectionFailure(this, "Student");
            return false;
        }

        return true;
    }
}
