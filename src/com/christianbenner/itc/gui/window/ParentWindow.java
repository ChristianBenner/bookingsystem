package com.christianbenner.itc.gui.window;

import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.PanelPopup;
import com.christianbenner.itc.gui.custom.ViewPopup;
import com.christianbenner.itc.gui.timetable.ParentTimetable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class ParentWindow extends Window
{
    public ParentWindow(final Font font, final Student student)
    {
        super(font);

        final Window referenceToSelf = this;
        ParentTimetable timetable = new ParentTimetable(font, student);

        // Menu-bar font
        Font menuBarFont = font.deriveFont(18f);

        // Add file menu to the window
        JMenuBar menuBar = new JMenuBar();
        menuBar.setFont(menuBarFont);
        menuBar.setBackground(GUIConstants.PANEL_COLOUR);
        menuBar.setBorderPainted(false);

        JMenu viewMenu = new JMenu("View");
        viewMenu.setFont(menuBarFont);
        viewMenu.setBorderPainted(false);
        viewMenu.setFocusPainted(false);

        JMenuItem viewMyDetails = new JMenuItem("Student Details");
        viewMyDetails.setFont(menuBarFont);
        viewMenu.add(viewMyDetails);
        viewMyDetails.setBorderPainted(false);
        viewMyDetails.setFocusPainted(false);

        menuBar.add(viewMenu);

        viewMyDetails.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                ViewMemberDetails viewMemberDetails = new ViewMemberDetails(font, "Student Details", student);
                ViewPopup viewPopup = new ViewPopup(referenceToSelf, viewMemberDetails, "Student Details");
                viewPopup.setVisible(true);
            }
        });

        JMenu addMenu = new JMenu("Book");
        addMenu.setFont(menuBarFont);
        addMenu.setBorderPainted(false);
        addMenu.setFocusPainted(false);
        menuBar.add(addMenu);

        JMenuItem bookMeeting = new JMenuItem("Book Meeting");
        bookMeeting.setFont(menuBarFont);
        bookMeeting.setBorderPainted(false);
        bookMeeting.setFocusPainted(false);
        addMenu.add(bookMeeting);
        bookMeeting.addActionListener(actionEvent ->
        {
            // If there is a lesson there and you are viewing the timetable as a parent book a meeting
            BookMeetingPanel bookMeetingPanel = new BookMeetingPanel(font, "Book Meeting");
            bookMeetingPanel.setSelectedStudent(student);
            bookMeetingPanel.setStudentComboBoxEnabled(false);

            PanelPopup panelPopup = new PanelPopup((JFrame) SwingUtilities.getWindowAncestor(referenceToSelf),
                    bookMeetingPanel, "Book Meeting");

            bookMeetingPanel.addSelectButtonListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent actionEvent)
                {
                    ParentMeeting parentMeetingAndSlot = bookMeetingPanel.getSelected();

                    // Add student to meeting and close
                    if(parentMeetingAndSlot.isAvailable())
                    {
                        parentMeetingAndSlot.book(student.getUniqueId());

                        JOptionPane.showMessageDialog(bookMeetingPanel, "Successfully booked meeting",
                                "Success", JOptionPane.INFORMATION_MESSAGE);
                        panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                        timetable.refreshInfo();
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(bookMeetingPanel, "That meeting is full, try another",
                                "Failed", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            });

            panelPopup.setVisible(true);
        });

        add(menuBar, BorderLayout.NORTH);
        add(timetable, BorderLayout.CENTER);
    }
}
