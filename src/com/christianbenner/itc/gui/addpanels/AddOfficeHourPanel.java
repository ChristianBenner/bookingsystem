package com.christianbenner.itc.gui.addpanels;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Room;
import com.christianbenner.itc.data.SessionTime;
import com.christianbenner.itc.data.Student;
import com.christianbenner.itc.data.Tutor;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.ValidationUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import static com.christianbenner.itc.gui.GUIUtils.getNextMeetingSpaces;

public class AddOfficeHourPanel extends AddPanel
{
    private static final int PREFERRED_WIDTH = 300;
    private static final int PREFERRED_HEIGHT = 300;

    private JComboBox<Room> roomComboBox;
    private JComboBox<String> availableWeeks;
    private JComboBox<String> timeComboBox;
    private JComboBox<String> dayComboBox;

    private List<GregorianCalendar> weeksAvailable;

    public AddOfficeHourPanel(Font font, String text, Tutor tutor)
    {
        super(font, text);

        JPanel gridBagPanel = new JPanel(new GridBagLayout());
        gridBagPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        Font panelFont = font.deriveFont(22f);

        JLabel roomLabel = new JLabel("Room: ");
        roomLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(roomLabel, gridBagConstraints);

        roomComboBox = new JComboBox<>();
        roomComboBox.setFont(panelFont);
        List<Room> roomList = Directories.getRoomDirectory().getAll();
        for(Room room : roomList)
        {
            roomComboBox.addItem(room);
        }
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 1;
        gridBagPanel.add(roomComboBox, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel selectWeekLabel = new JLabel("Week: ");
        selectWeekLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(selectWeekLabel, gridBagConstraints);

        // Select week combo box
        availableWeeks = new JComboBox<>();
        availableWeeks.setFont(panelFont);
        availableWeeks.addActionListener(actionEvent ->
        {
            if(weeksAvailable.size() > availableWeeks.getSelectedIndex() && availableWeeks.getSelectedIndex() != -1)
            {
                // Get selected date
                GregorianCalendar gregorianCalendar = weeksAvailable.get(availableWeeks.getSelectedIndex());
                System.out.println(gregorianCalendar.getTime());
            }
        });
        gridBagConstraints.gridx = 1;
        gridBagPanel.add(availableWeeks, gridBagConstraints);

        weeksAvailable = getNextMeetingSpaces(tutor);

        // Create list of strings
        List<String> dateStrings = new ArrayList<>();
        for(GregorianCalendar week : weeksAvailable)
        {
            dateStrings.add(week.get(GregorianCalendar.DAY_OF_MONTH) + "/" +
                    (week.get(GregorianCalendar.MONTH) + 1) + "/" + week.get(GregorianCalendar.YEAR));
        }

        // Add the list of available weeks
        for(String dateString : dateStrings)
        {
            availableWeeks.addItem(dateString);
        }

        JLabel dayLabel = new JLabel("Day: ");
        dayLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(dayLabel, gridBagConstraints);

        dayComboBox = new JComboBox<>();
        dayComboBox.setFont(panelFont);
        dayComboBox.addItem("Monday");
        dayComboBox.addItem("Tuesday");
        dayComboBox.addItem("Wednesday");
        dayComboBox.addItem("Thursday");
        dayComboBox.addItem("Friday");
        gridBagConstraints.weightx = 1;
        gridBagConstraints.gridx = 1;
        gridBagPanel.add(dayComboBox, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel timeLabel = new JLabel("Time: ");
        timeLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(timeLabel, gridBagConstraints);

        timeComboBox = new JComboBox<>();
        timeComboBox.setFont(panelFont);
        for(int i = 0; i < SessionTime.HOURS_IN_DAY; i++)
        {
            timeComboBox.addItem(SessionTime.getTimeString(i));
        }

        gridBagConstraints.gridx = 1;
        gridBagPanel.add(timeComboBox, gridBagConstraints);

        super.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
        super.add(gridBagPanel, BorderLayout.CENTER);
    }

    public SessionTime getSessionTime()
    {
        // Get selected date
        GregorianCalendar gregorianCalendar = weeksAvailable.get(availableWeeks.getSelectedIndex());

        return new SessionTime(gregorianCalendar.get(GregorianCalendar.YEAR),
                gregorianCalendar.get(GregorianCalendar.MONTH) + 1,
                gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH) + dayComboBox.getSelectedIndex(),
                timeComboBox.getSelectedIndex());
    }

    public int getRoomId()
    {
        return ((Room)roomComboBox.getSelectedItem()).getUniqueId();
    }

    public boolean runValidationChecks()
    {
        // Check that the selected room is not null and is of type room
        if(roomComboBox.getSelectedItem() == null || !(roomComboBox.getSelectedItem() instanceof Room))
        {
            ValidationUtils.showNoSelectionFailure(this, "Room");
            return false;
        }

        // Check that the date is not in the past
        final SessionTime selectedSessionTime = getSessionTime();
        final GregorianCalendar selectedDate = new GregorianCalendar(selectedSessionTime.getYear(),
                selectedSessionTime.getMonth() - 1, selectedSessionTime.getDay(),
                selectedSessionTime.getHoursFromDayStart() - SessionTime.DAY_START_HOURS, 0);

        // Check if the selected date is in the past
        if(selectedDate.getTimeInMillis() < new GregorianCalendar().getTimeInMillis())
        {
            JOptionPane.showMessageDialog(this, "Cannot create a meeting in the past",
                    "Unsuccessful", JOptionPane.WARNING_MESSAGE);
            return false;
        }

        return true;
    }
}
