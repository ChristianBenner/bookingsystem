package com.christianbenner.itc.gui.addpanels;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.TextField;
import com.christianbenner.itc.gui.ValidationUtils;

import javax.swing.*;
import java.awt.*;

import static com.christianbenner.itc.gui.ValidationUtils.showValidationFailure;

public class AddSubjectPanel extends AddPanel
{
    private static final int SUBJECT_NAME_FIELD_MIN_VALUE = 1;
    private static final int SUBJECT_NAME_FIELD_MAX_VALUE = 25;

    private TextField subjectNameField;

    public AddSubjectPanel(Font font)
    {
        super(font, "Add Subject");

        JPanel gridBagPanel = new JPanel(new GridBagLayout());
        gridBagPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        Font panelFont = font.deriveFont(22f);

        JLabel subjectNameLabel = new JLabel("Subject Name");
        subjectNameLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagPanel.add(subjectNameLabel, gridBagConstraints);

        subjectNameField = new TextField(panelFont);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagPanel.add(subjectNameField, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        super.setPreferredSize(new Dimension(500, 150));
        super.add(gridBagPanel, BorderLayout.CENTER);
    }

    public String getSubjectName()
    {
        return subjectNameField.getText();
    }

    public boolean runValidationChecks()
    {
        // Validate the
        if(!ValidationUtils.validateText(subjectNameField.getText(), SUBJECT_NAME_FIELD_MIN_VALUE,
                SUBJECT_NAME_FIELD_MAX_VALUE))
        {
            showValidationFailure(this, "Subject name",
                    SUBJECT_NAME_FIELD_MIN_VALUE, SUBJECT_NAME_FIELD_MAX_VALUE);
            return false;
        }

        return true;
    }
}
