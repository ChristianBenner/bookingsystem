package com.christianbenner.itc.gui.addpanels;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.TextField;
import com.christianbenner.itc.gui.ValidationUtils;

import javax.swing.*;
import java.awt.*;

import static com.christianbenner.itc.gui.ValidationUtils.showSpinnerRangeFailure;
import static com.christianbenner.itc.gui.ValidationUtils.showValidationFailure;

public class AddRoomPanel extends AddPanel
{
    private static final int BUILDING_NAME_FIELD_MIN_LENGTH = 1;
    private static final int BUILDING_NAME_FIELD_MAX_LENGTH = 16;
    private static final int ROOM_NUMBER_FIELD_MIN_VALUE = 1;
    private static final int ROOM_NUMBER_FIELD_MAX_VALUE = 999;

    private TextField buildingNameField;
    private JSpinner roomNumberField;

    public AddRoomPanel(Font font)
    {
        super(font, "Add Room");
        JPanel gridBagPanel = new JPanel(new GridBagLayout());
        gridBagPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        JLabel buildingNameLabel = new JLabel("Building Name");
        buildingNameLabel.setFont(font);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagPanel.add(buildingNameLabel, gridBagConstraints);

        buildingNameField = new TextField(font);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagPanel.add(buildingNameField, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel roomNameLabel = new JLabel("Room Number");
        roomNameLabel.setFont(font);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagPanel.add(roomNameLabel, gridBagConstraints);

        roomNumberField = new JSpinner();
        roomNumberField.setFont(font);
        roomNumberField.setValue(1);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagPanel.add(roomNumberField, gridBagConstraints);

        super.setPreferredSize(new Dimension(500, 190));
        super.add(gridBagPanel, BorderLayout.CENTER);
    }

    public String getBuildingName()
    {
        return buildingNameField.getText();
    }

    public int getRoomNumber()
    {
        return (Integer)roomNumberField.getValue();
    }

    public boolean runValidationChecks()
    {
        if(!ValidationUtils.validateText(buildingNameField.getText(), BUILDING_NAME_FIELD_MIN_LENGTH, BUILDING_NAME_FIELD_MAX_LENGTH))
        {
            showValidationFailure(this, "Building name",
                    BUILDING_NAME_FIELD_MIN_LENGTH, BUILDING_NAME_FIELD_MAX_LENGTH);
            return false;
        }

        if(!(roomNumberField.getValue() instanceof  Integer) || !ValidationUtils.validateSpinner((Integer)roomNumberField.
                getValue(), ROOM_NUMBER_FIELD_MIN_VALUE, ROOM_NUMBER_FIELD_MAX_VALUE))
        {
            showSpinnerRangeFailure(this, "Room number", ROOM_NUMBER_FIELD_MIN_VALUE,
                    ROOM_NUMBER_FIELD_MAX_VALUE);
            return false;
        }

        return true;
    }
}
