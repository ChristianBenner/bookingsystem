package com.christianbenner.itc.gui.addpanels;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.Button;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public abstract class AddPanel extends JPanel
{
    private Button createButton;

    public AddPanel(Font font, String title, boolean titleAndButton)
    {
        super(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        if(titleAndButton)
        {
            Font textFont = font.deriveFont(28f);

            JLabel titleLabel = new JLabel(title, JLabel.HORIZONTAL);
            titleLabel.setFont(textFont);
            super.add(titleLabel, BorderLayout.NORTH);

            createButton = new Button("Create");
            createButton.setBackground(GUIConstants.PANEL_COLOUR);
            createButton.setFont(textFont);

            super.add(createButton, BorderLayout.SOUTH);
        }
    }

    public AddPanel(Font font, String title)
    {
        this(font, title, true);
    }

    public void addCreateButtonActionListener(ActionListener actionListener)
    {
        if(createButton != null)
        {
            createButton.addActionListener(actionListener);
        }
    }
}
