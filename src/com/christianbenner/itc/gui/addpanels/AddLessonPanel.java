package com.christianbenner.itc.gui.addpanels;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.ValidationUtils;

import java.util.List;
import javax.swing.*;
import java.awt.*;

public class AddLessonPanel extends AddPanel
{
    private static final int MAX_SPACES_SPINNER_VALUE_MIN = 1;
    private static final int MAX_SPACES_SPINNER_VALUE_MAX = 99;
    private static final int PREFERRED_WIDTH = 500;
    private static final int PREFERRED_HEIGHT = 400;
    private static final int PREFERRED_HEIGHT_EXACT_DATE = 500;

    private JComboBox<Tutor> tutorComboBox;
    private JComboBox<Room> roomComboBox;
    private JComboBox<Subject> subjectComboBox;
    private JComboBox<String> dayComboBox;
    private JComboBox<String> timeComboBox;
    private JSpinner maxSpacesSpinner;

    public AddLessonPanel(Font font, String text, boolean exactDate)
    {
        super(font, text);

        JPanel gridBagPanel = new JPanel(new GridBagLayout());
        gridBagPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        Font panelFont = font.deriveFont(22f);

        JLabel tutorLabel = new JLabel("Tutor: ");
        tutorLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagPanel.add(tutorLabel, gridBagConstraints);

        tutorComboBox = new JComboBox<>();
        tutorComboBox.setFont(panelFont);
        List<Tutor> tutorList = Directories.getTutorDirectory().getAll();
        for(Tutor tutor : tutorList)
        {
            tutorComboBox.addItem(tutor);
        }
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagPanel.add(tutorComboBox, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel roomLabel = new JLabel("Room: ");
        roomLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(roomLabel, gridBagConstraints);

        roomComboBox = new JComboBox<>();
        roomComboBox.setFont(panelFont);
        List<Room> roomList = Directories.getRoomDirectory().getAll();
        for(Room room : roomList)
        {
            roomComboBox.addItem(room);
        }
        gridBagConstraints.gridx = 1;
        gridBagPanel.add(roomComboBox, gridBagConstraints);

        JLabel dayLabel = new JLabel("Day: ");
        dayLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(dayLabel, gridBagConstraints);

        dayComboBox = new JComboBox<>();
        dayComboBox.setFont(panelFont);
        dayComboBox.addItem("Monday");
        dayComboBox.addItem("Tuesday");
        dayComboBox.addItem("Wednesday");
        dayComboBox.addItem("Thursday");
        dayComboBox.addItem("Friday");
        gridBagConstraints.weightx = 1;
        gridBagConstraints.gridx = 1;
        gridBagPanel.add(dayComboBox, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel timeLabel = new JLabel("Time: ");
        timeLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(timeLabel, gridBagConstraints);

        timeComboBox = new JComboBox<>();
        timeComboBox.setFont(panelFont);
        for(int i = 0; i < SessionTime.HOURS_IN_DAY; i++)
        {
            timeComboBox.addItem(SessionTime.getTimeString(i));
        }

        gridBagConstraints.gridx = 1;
        gridBagPanel.add(timeComboBox, gridBagConstraints);

        JLabel subjectLabel = new JLabel("Subject: ");
        subjectLabel.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(subjectLabel, gridBagConstraints);

        subjectComboBox = new JComboBox<>();
        subjectComboBox.setFont(panelFont);
        List<Subject> subjectList = Directories.getSubjectDirectory().getAll();
        for(Subject subject : subjectList)
        {
            subjectComboBox.addItem(subject);
        }
        gridBagConstraints.gridx = 1;
        gridBagPanel.add(subjectComboBox, gridBagConstraints);

        JLabel maxSpaces = new JLabel("Max Spaces: ");
        maxSpaces.setFont(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        gridBagPanel.add(maxSpaces, gridBagConstraints);

        maxSpacesSpinner = new JSpinner();
        maxSpacesSpinner.setValue(5);
        maxSpacesSpinner.setFont(panelFont);
        gridBagConstraints.gridx = 1;
        gridBagPanel.add(maxSpacesSpinner, gridBagConstraints);

        super.setPreferredSize(new Dimension(PREFERRED_WIDTH,
                exactDate ? PREFERRED_HEIGHT_EXACT_DATE : PREFERRED_HEIGHT));
        super.add(gridBagPanel, BorderLayout.CENTER);
    }

    public void setTutorComboBoxDisplay(Tutor tutor)
    {
        tutorComboBox.setSelectedItem(tutor);
    }

    public void setEnabledTutorComboBox(boolean state)
    {
        tutorComboBox.setEnabled(state);
    }

    public int getSelectedTutorId()
    {
        return ((Tutor)tutorComboBox.getSelectedItem()).getUniqueId();
    }

    public int getSelectedSubjectId()
    {
        return ((Subject)subjectComboBox.getSelectedItem()).getUniqueId();
    }

    public int getSelectedRoomId()
    {
        return ((Room)roomComboBox.getSelectedItem()).getUniqueId();
    }

    public void setDayComboBoxDisplay(int day)
    {
        dayComboBox.setSelectedItem(SessionTime.getDayString(day));
    }

    public void setEnabledDayComboBox(boolean state)
    {
        dayComboBox.setEnabled(state);
    }

    public void setTimeComboBoxDisplay(int time)
    {
        timeComboBox.setSelectedItem(SessionTime.getTimeString(time));
    }

    public void setEnabledTimeComboBox(boolean state)
    {
        timeComboBox.setEnabled(state);
    }

    public int getSelectedDay()
    {
        return SessionTime.getDayId((String)dayComboBox.getSelectedItem());
    }

    public int getSelectedTime()
    {
        return timeComboBox.getSelectedIndex();
    }

    public int getMaxSpaces()
    {
        return (Integer)maxSpacesSpinner.getValue();
    }

    public SessionTime getSessionTime()
    {
        return new SessionTime(getSelectedDay(), getSelectedTime());
    }

    public boolean runValidationChecks()
    {
        // Check that the selected tutor is not null and is of type tutor
        if(tutorComboBox.getSelectedItem() == null || !(tutorComboBox.getSelectedItem() instanceof Tutor))
        {
            ValidationUtils.showNoSelectionFailure(this, "Tutor");
            return false;
        }

        // Check that the selected room is not null and is of type room
        if(roomComboBox.getSelectedItem() == null || !(roomComboBox.getSelectedItem() instanceof Room))
        {
            ValidationUtils.showNoSelectionFailure(this, "Room");
            return false;
        }

        // Check that the selected subject is not null and is of type subject
        if(subjectComboBox.getSelectedItem() == null || !(subjectComboBox.getSelectedItem() instanceof Subject))
        {
            ValidationUtils.showNoSelectionFailure(this, "Subject");
            return false;
        }

        // Validate max spaces
        if(!(maxSpacesSpinner.getValue() instanceof Integer) || !(ValidationUtils.validateSpinner((Integer)maxSpacesSpinner
                .getValue(), MAX_SPACES_SPINNER_VALUE_MIN, MAX_SPACES_SPINNER_VALUE_MAX)))
        {
            ValidationUtils.showSpinnerRangeFailure(this, "Max Spaces", MAX_SPACES_SPINNER_VALUE_MIN,
                    MAX_SPACES_SPINNER_VALUE_MAX);
            return false;
        }

        if(dayComboBox != null)
        {
            // Check that the selected day is not null and is of type String. It also must not be an empty string
            if(dayComboBox.getSelectedItem() == null || !(dayComboBox.getSelectedItem() instanceof String) ||
                    ((String)dayComboBox.getSelectedItem()).isEmpty())
            {
                ValidationUtils.showNoSelectionFailure(this, "Day");
                return false;
            }
        }

        // Check that the selected time is not null and is of type String. It also must not be an empty string
        if(timeComboBox.getSelectedItem() == null || !(timeComboBox.getSelectedItem() instanceof String) ||
                ((String)timeComboBox.getSelectedItem()).isEmpty())
        {
            ValidationUtils.showNoSelectionFailure(this, "Time");
            return false;
        }

        // Check if the lesson is being booked on-top of another
        final SessionTime sessionTime = getSessionTime();
        final List<Lesson> tutorLessons = ((Tutor)tutorComboBox.getSelectedItem()).getLessons();
        for(final Lesson lesson : tutorLessons)
        {
            if(lesson.getSessionTime().equals(sessionTime))
            {
                JOptionPane.showMessageDialog(this,
                        "Failed to create lesson because a lesson already exists at the time",
                        "Could not create lesson", JOptionPane.INFORMATION_MESSAGE);
                return false;
            }
        }

        return true;
    }
}
