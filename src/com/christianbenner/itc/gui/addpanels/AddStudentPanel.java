package com.christianbenner.itc.gui.addpanels;

import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.GUIUtils;
import com.christianbenner.itc.gui.ValidationUtils;
import com.christianbenner.itc.gui.custom.TextField;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.christianbenner.itc.gui.GUIConstants.PANEL_COLOUR;

public class AddStudentPanel extends AddPanel
{
    private final static int FIRST_NAME_LENGTH_MIN = 1;
    private final static int FIRST_NAME_LENGTH_MAX = 16;
    private final static int LAST_NAME_LENGTH_MIN = 1;
    private final static int LAST_NAME_LENGTH_MAX = 16;
    private final static int POST_CODE_LENGTH_MIN = 5;
    private final static int POST_CODE_MAX_LENGTH = 8;
    private final static int ADDRESS_LINE_ONE_MIN_LENGTH = 1;
    private final static int ADDRESS_LINE_ONE_MAX_LENGTH = 50;
    private final static int ADDRESS_LINE_TWO_MIN_LENGTH = 1;
    private final static int ADDRESS_LINE_TWO_MAX_LENGTH = 50;
    private final static int ADDRESS_LINE_THREE_MIN_LENGTH = 0;
    private final static int ADDRESS_LINE_THREE_MAX_LENGTH = 50;
    private final static int PHONE_NUMBER_MIN_LENGTH = 10;
    private final static int PHONE_NUMBER_MAX_LENGTH = 12;

    private TextField firstNameField;
    private TextField lastNameField;
    private TextField addressLineOneField;
    private TextField addressLineTwoField;
    private TextField addressLineThreeField;
    private TextField phoneNumberField;
    private JComboBox<Address> addressJComboBox;

    public AddStudentPanel(final Font font, final String title, final boolean titleAndButton)
    {
        super(font, title, titleAndButton);

        JPanel borderLayoutPanel = new JPanel(new BorderLayout());
        borderLayoutPanel.setBackground(PANEL_COLOUR);

        Font panelFont = font.deriveFont(18f);
        Font postCodeFont = font.deriveFont(12f);

        JPanel panel = new JPanel();

        panel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);

        JLabel firstNameText = new JLabel("First Name: ");
        firstNameText.setFont(panelFont);
        firstNameField = new TextField(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(firstNameText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 1;
        panel.add(firstNameField, gridBagConstraints);
        gridBagConstraints.weightx = 0;

        JLabel lastNameText = new JLabel("Last Name: ");
        lastNameText.setFont(panelFont);
        lastNameField = new TextField(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(lastNameText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panel.add(lastNameField, gridBagConstraints);

        JPanel postCodeLayout = new JPanel(new BorderLayout());
        postCodeLayout.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2, true));
        JLabel postCodeText = new JLabel("Post Code: ");
        postCodeText.setFont(panelFont);
        com.christianbenner.itc.gui.custom.Button postCodeSearchButton = new com.christianbenner.itc.gui.custom.Button(font, "Search");
        postCodeSearchButton.setBorder(new EmptyBorder(0, 0, 0, 0));


        addressJComboBox = new JComboBox<>();
        addressJComboBox.setFont(postCodeFont);
        addressJComboBox.setEditable(true);
        addressJComboBox.setSelectedItem("");

        postCodeLayout.add(addressJComboBox, BorderLayout.CENTER);
        postCodeLayout.add(postCodeSearchButton, BorderLayout.EAST);
        postCodeSearchButton.addActionListener(actionEvent ->
        {
            List<Address> addressList = new ArrayList<>();

            panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            try
            {
                addressList = GUIUtils.getAddressListFromPostCode(addressJComboBox.getSelectedItem().toString());
                addressJComboBox.removeAllItems();
                for(Address address : addressList)
                {
                    addressJComboBox.addItem(address);
                }

                addressJComboBox.showPopup();
            }
            catch (IOException e)
            {
                e.printStackTrace();

                JOptionPane.showMessageDialog(panel, "Failed to retrieve addresses",
                        "Failed Address Search", JOptionPane.WARNING_MESSAGE);
            }

            panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        });

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(postCodeText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panel.add(postCodeLayout, gridBagConstraints);


        JLabel addressLineOneText = new JLabel("Address 1: ");
        addressLineOneText.setFont(panelFont);
        addressLineOneField = new TextField(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(addressLineOneText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panel.add(addressLineOneField, gridBagConstraints);

        JLabel addressLineTwoText = new JLabel("Address 2: ");
        addressLineTwoText.setFont(panelFont);
        addressLineTwoField = new TextField(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(addressLineTwoText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panel.add(addressLineTwoField, gridBagConstraints);

        JLabel addressLineThreeText = new JLabel("Address 3: ");
        addressLineThreeText.setFont(panelFont);
        addressLineThreeField = new TextField(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(addressLineThreeText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panel.add(addressLineThreeField, gridBagConstraints);

        addressJComboBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                if("comboBoxChanged".equals(actionEvent.getActionCommand()) &&
                        addressJComboBox.getSelectedItem() instanceof Address)
                {
                    Address address = (Address) addressJComboBox.getSelectedItem();
                    addressLineOneField.setText(address.getAddressLineOne());
                    addressLineTwoField.setText(address.getAddressLineTwo());
                    addressLineThreeField.setText(address.getAddressLineThree());
                    addressJComboBox.setSelectedItem(address.getPostCode());
                }
            }
        });

        JLabel phoneNumberText = new JLabel("Phone Number: ");
        phoneNumberText.setFont(panelFont);
        phoneNumberField = new TextField(panelFont);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy++;
        panel.add(phoneNumberText, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panel.add(phoneNumberField, gridBagConstraints);

        panel.setBackground(GUIConstants.BACKGROUND_COLOUR);
        borderLayoutPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);
        borderLayoutPanel.add(panel, BorderLayout.CENTER);

        super.setPreferredSize(new Dimension(500, 400));
        super.add(borderLayoutPanel, BorderLayout.CENTER);
    }

    public AddStudentPanel(final Font font)
    {
        this(font, null, false);
    }

    public String getFirstName()
    {
        return firstNameField.getText();
    }

    public String getLastName()
    {
        return lastNameField.getText();
    }

    public String getPostCode()
    {
        return addressJComboBox.getSelectedItem().toString();
    }

    public String getAddressLineOne()
    {
        return addressLineOneField.getText();
    }

    public String getAddressLineTwo()
    {
        return addressLineTwoField.getText();
    }

    public String getAddressLineThree()
    {
        return addressLineThreeField.getText();
    }

    public String getPhoneNumber()
    {
        return phoneNumberField.getText();
    }

    public boolean runValidationChecks()
    {
        // Retrieve all of the text fields
        final String firstName = getFirstName();
        final String lastName = getLastName();
        final String postCode = getPostCode();
        final String addressLineOne = getAddressLineOne();
        final String addressLineTwo = getAddressLineTwo();
        final String addressLineThree = getAddressLineThree();
        final String phoneNumber = getPhoneNumber();

        // Validate first name field
        if(!ValidationUtils.validateText(firstName, FIRST_NAME_LENGTH_MIN, FIRST_NAME_LENGTH_MAX))
        {
            ValidationUtils.showValidationFailure(this, "First name", FIRST_NAME_LENGTH_MIN,
                    FIRST_NAME_LENGTH_MAX);
            return false;
        }

        // Validate last name field
        if(!ValidationUtils.validateText(lastName, LAST_NAME_LENGTH_MIN, LAST_NAME_LENGTH_MAX))
        {
            ValidationUtils.showValidationFailure(this, "Last name", LAST_NAME_LENGTH_MIN,
                    LAST_NAME_LENGTH_MAX);
            return false;
        }

        // Validate post code field
        if(!ValidationUtils.validateText(postCode, POST_CODE_LENGTH_MIN, POST_CODE_MAX_LENGTH))
        {
            ValidationUtils.showValidationFailure(this, "Postcode", POST_CODE_LENGTH_MIN,
                    POST_CODE_MAX_LENGTH);
            return false;
        }

        // Validate address line one field
        if(!ValidationUtils.validateText(addressLineOne, ADDRESS_LINE_ONE_MIN_LENGTH, ADDRESS_LINE_ONE_MAX_LENGTH))
        {
            ValidationUtils.showValidationFailure(this, "Address line one",
                    ADDRESS_LINE_ONE_MIN_LENGTH, ADDRESS_LINE_ONE_MAX_LENGTH);
            return false;
        }

        // Validate address line two field
        if(!ValidationUtils.validateText(addressLineTwo, ADDRESS_LINE_TWO_MIN_LENGTH, ADDRESS_LINE_TWO_MAX_LENGTH))
        {
            ValidationUtils.showValidationFailure(this, "Address line two",
                    ADDRESS_LINE_TWO_MIN_LENGTH, ADDRESS_LINE_TWO_MAX_LENGTH);
            return false;
        }

        // Validate address line three field
        if(!ValidationUtils.validateText(addressLineThree, ADDRESS_LINE_THREE_MIN_LENGTH,
                ADDRESS_LINE_THREE_MAX_LENGTH))
        {
            ValidationUtils.showValidationFailure(this, "Address line three",
                    ADDRESS_LINE_THREE_MIN_LENGTH, ADDRESS_LINE_THREE_MIN_LENGTH);
            return false;
        }

        // Validate phone number field
        if(!ValidationUtils.validateText(phoneNumber, PHONE_NUMBER_MIN_LENGTH, PHONE_NUMBER_MAX_LENGTH))
        {
            ValidationUtils.showValidationFailure(this, "Phone number",
                    PHONE_NUMBER_MIN_LENGTH, PHONE_NUMBER_MAX_LENGTH);
            return false;
        }

        return true;
    }
}
