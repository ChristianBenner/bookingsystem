package com.christianbenner.itc.gui.timetable;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Lesson;
import com.christianbenner.itc.data.Session;
import com.christianbenner.itc.data.Student;
import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.custom.Button;
import com.christianbenner.itc.gui.custom.ViewPopup;
import com.christianbenner.itc.gui.window.ViewLessonPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class StudentTimetable extends Timetable
{
    private Student student;

    public void refreshInfo()
    {
        final List<Lesson> lessons = student.getLessons();
        final List<Session> sessions = new ArrayList<>();
        sessions.addAll(lessons);
        super.populate(sessions);
    }

    public StudentTimetable(final Font font, final Student student)
    {
        super(font);
        this.student = student;
        super.setSlotActionListener(new SlotActionListener(font, this));
        refreshInfo();
    }

    class SlotActionListener implements ActionListener
    {
        private final Font font;
        private final StudentTimetable studentTimetable;

        public SlotActionListener(final Font font, final StudentTimetable studentTimetable)
        {
            this.font = font;
            this.studentTimetable = studentTimetable;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            SessionSlot buttonSource = (SessionSlot) actionEvent.getSource();

            if (buttonSource.getLessonId() != -1)
            {
                final JFrame frameAncestor = (JFrame) SwingUtilities.getWindowAncestor(studentTimetable);
                final ViewLessonPanel viewLessonPanel = new ViewLessonPanel(font,
                        Directories.getLessonDirectory().get(buttonSource.getLessonId()), frameAncestor, false);

                final ViewPopup viewPopup = new ViewPopup(frameAncestor, viewLessonPanel, "View Lesson");

                Button removeButton = new Button("Remove");
                removeButton.setFont(font.deriveFont(16f));
                removeButton.setBackground(GUIConstants.PANEL_COLOUR);
                removeButton.addActionListener(actionEvent1 ->
                {
                    Lesson lesson = Directories.getLessonDirectory().get(buttonSource.getLessonId());
                    lesson.removeStudent(studentTimetable.student.getUniqueId());
                    refreshInfo();

                    JOptionPane.showMessageDialog(viewLessonPanel,
                            "Successfully removed student from lesson", "Success",
                            JOptionPane.INFORMATION_MESSAGE);
                    viewPopup.dispatchEvent(new WindowEvent(viewPopup, WindowEvent.WINDOW_CLOSING));
                });
                viewLessonPanel.add(removeButton, BorderLayout.SOUTH);

                viewPopup.setVisible(true);
            }
        }
    }
}
