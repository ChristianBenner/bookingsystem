package com.christianbenner.itc.gui.timetable;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.Lesson;
import com.christianbenner.itc.data.ParentMeeting;
import com.christianbenner.itc.data.Session;
import com.christianbenner.itc.data.Tutor;
import com.christianbenner.itc.gui.*;
import com.christianbenner.itc.gui.addpanels.AddLessonPanel;
import com.christianbenner.itc.gui.custom.PanelPopup;
import com.christianbenner.itc.gui.custom.Button;
import com.christianbenner.itc.gui.custom.ViewPopup;
import com.christianbenner.itc.gui.window.ViewLessonPanel;
import com.christianbenner.itc.gui.window.ViewParentMeetingPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class TutorTimetable extends Timetable
{
    private Tutor tutor;

    public void refreshInfo()
    {
        final List<Lesson> lessons = tutor.getLessons();
        final List<ParentMeeting> parentMeetings = tutor.getParentMeetings();

        final List<Session> sessions = new ArrayList<>();
        sessions.addAll(lessons);
        sessions.addAll(parentMeetings);

        super.populate(sessions);
    }

    public TutorTimetable(final Font font, final Tutor tutor)
    {
        super(font);
        this.tutor = tutor;
        super.setSlotActionListener(new SlotActionListener(font, this));
        refreshInfo();
    }

    class SlotActionListener implements ActionListener
    {
        private final Font font;
        private final TutorTimetable tutorTimetable;

        public SlotActionListener(final Font font, final TutorTimetable tutorTimetable)
        {
            this.font = font;
            this.tutorTimetable = tutorTimetable;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            SessionSlot buttonSource = (SessionSlot)actionEvent.getSource();

            if(buttonSource.getLessonId() != SessionSlot.NO_SESSION_OCCUPYING_SLOT)
            {
                final JFrame referenceToWindowAncestor = (JFrame)SwingUtilities.getWindowAncestor(tutorTimetable);
                final ViewLessonPanel viewLessonPanel = new ViewLessonPanel(font,
                        Directories.getLessonDirectory().get(buttonSource.getLessonId()), referenceToWindowAncestor,
                        true);

                final ViewPopup viewPopup = new ViewPopup(referenceToWindowAncestor, viewLessonPanel,
                        "View Lesson");

                Button removeButton = new Button("Remove");
                removeButton.setFont(font.deriveFont(16f));
                removeButton.setBackground(GUIConstants.PANEL_COLOUR);
                removeButton.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        Directories.getLessonDirectory().remove(buttonSource.getLessonId());
                        refreshInfo();

                        JOptionPane.showMessageDialog(viewLessonPanel,
                                "Successfully removed lesson", "Success",
                                JOptionPane.INFORMATION_MESSAGE);
                        viewPopup.dispatchEvent(new WindowEvent(viewPopup, WindowEvent.WINDOW_CLOSING));
                    }
                });
                viewLessonPanel.add(removeButton, BorderLayout.SOUTH);
                viewPopup.setVisible(true);
            }
            else if(buttonSource.containsParentMeeting())
            {
                final JFrame referenceToWindowAncestor = (JFrame)SwingUtilities.getWindowAncestor(tutorTimetable);

                final ViewParentMeetingPanel viewParentMeetingPanel = new ViewParentMeetingPanel(font,
                        buttonSource.getParentMeetingIds());

                final ViewPopup viewPopup = new ViewPopup(referenceToWindowAncestor, viewParentMeetingPanel,
                        "View Parent Meeting");
                viewPopup.setVisible(true);
            }
            else
            {
                AddLessonPanel addLessonPanel = new AddLessonPanel(font, "Add Lesson", false);
                PanelPopup panelPopup = new PanelPopup((JFrame) SwingUtilities.getWindowAncestor(tutorTimetable),
                        addLessonPanel, "Add Lesson");

                addLessonPanel.setTutorComboBoxDisplay(tutor);
                addLessonPanel.setEnabledTutorComboBox(false);
                addLessonPanel.setDayComboBoxDisplay(buttonSource.getSessionTime().getDay());
                addLessonPanel.setEnabledDayComboBox(false);
                addLessonPanel.setTimeComboBoxDisplay(buttonSource.getSessionTime().getHoursFromDayStart());
                addLessonPanel.setEnabledTimeComboBox(false);

                addLessonPanel.addCreateButtonActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent)
                    {
                        // Validate the entered data
                        if (addLessonPanel.runValidationChecks())
                        {
                            // Create and add lesson to directory
                            Lesson lesson = new Lesson(addLessonPanel.getSelectedTutorId(),
                                    addLessonPanel.getSelectedRoomId(), addLessonPanel.getSelectedSubjectId(),
                                    addLessonPanel.getSessionTime(),
                                    addLessonPanel.getMaxSpaces());

                            Directories.getLessonDirectory().add(lesson);

                            JOptionPane.showMessageDialog(addLessonPanel, "Successfully added lesson",
                                    "Success", JOptionPane.INFORMATION_MESSAGE);
                            panelPopup.dispatchEvent(new WindowEvent(panelPopup, WindowEvent.WINDOW_CLOSING));
                            tutorTimetable.refreshInfo();
                        }
                    }
                });

                panelPopup.setVisible(true);
            }
        }
    }
}
