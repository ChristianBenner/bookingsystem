package com.christianbenner.itc.gui.timetable;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.custom.ViewPopup;
import com.christianbenner.itc.gui.window.ViewLessonPanel;
import com.christianbenner.itc.gui.window.ViewParentMeetingPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ParentTimetable extends Timetable
{
    private Student student;

    public void refreshInfo()
    {
        final List<Lesson> lessons = student.getLessons();
        final List<ParentMeeting> parentMeetings = student.getParentMeetings();

        final List<Session> sessions = new ArrayList<>();
        sessions.addAll(lessons);
        sessions.addAll(parentMeetings);
        super.populate(sessions);
    }

    public ParentTimetable(final Font font, final Student student)
    {
        super(font);
        this.student = student;
        super.setSlotActionListener(new SlotActionListener(font, this));
        refreshInfo();
    }

    class SlotActionListener implements ActionListener
    {
        private final Font font;
        private final ParentTimetable parentTimetable;

        public SlotActionListener(final Font font, final ParentTimetable parentTimetable)
        {
            this.font = font;
            this.parentTimetable = parentTimetable;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            SessionSlot buttonSource = (SessionSlot)actionEvent.getSource();

            if(buttonSource.getLessonId() != SessionSlot.NO_SESSION_OCCUPYING_SLOT)
            {
                final JFrame referenceToWindowAncestor = (JFrame)SwingUtilities.getWindowAncestor(parentTimetable);

                final ViewLessonPanel viewLessonPanel = new ViewLessonPanel(font,
                        Directories.getLessonDirectory().get(buttonSource.getLessonId()), referenceToWindowAncestor,
                        false);

                final ViewPopup viewPopup = new ViewPopup(referenceToWindowAncestor, viewLessonPanel,
                        "View Lesson");
                viewPopup.setVisible(true);
            }

            if(buttonSource.containsParentMeeting())
            {
                final JFrame referenceToWindowAncestor = (JFrame)SwingUtilities.getWindowAncestor(parentTimetable);

                final ViewParentMeetingPanel viewParentMeetingPanel = new ViewParentMeetingPanel(font,
                        buttonSource.getParentMeetingIds());

                final ViewPopup viewPopup = new ViewPopup(referenceToWindowAncestor, viewParentMeetingPanel,
                        "View Parent Meeting");
                viewPopup.setVisible(true);
            }
        }
    }
}
