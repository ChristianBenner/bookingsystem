package com.christianbenner.itc.gui.timetable;

import com.christianbenner.itc.data.*;
import com.christianbenner.itc.gui.*;
import com.christianbenner.itc.gui.custom.Button;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static com.christianbenner.itc.gui.GUIUtils.*;

public class Timetable extends JPanel
{
    private static final Color ODD_ROW_COLOUR =  new Color(177, 195, 216);
    private static final Color EVEN_ROW_COLOUR =  new Color(157, 175, 196);
    private static final Color DAY_COLOUR = ODD_ROW_COLOUR.darker();
    private static final Color TIME_COLOUR = ODD_ROW_COLOUR.darker();
    private static final int ROWS = SessionTime.HOURS_IN_DAY + 1;
    private static final int COLUMNS = 6;
    private static final int LESSON_ROWS = ROWS - 1;
    private static final int LESSON_COLUMNS = COLUMNS - 1;
    private static final int DAY_OFFSET = -SessionTime.MONDAY; // this is because a Monday in the time table is x = 0, so we want to offset the lesson time

    private SessionSlot[][] sessionSlots = new SessionSlot[LESSON_ROWS][LESSON_COLUMNS];
    private Font slotFont;
    private Font borderFonts;
    private ActionListener slotActionListener;
    private JPanel gridLayout;
    private GregorianCalendar gregorianCalendar;
    private List<Session> sessions;

    protected Timetable(final Font font)
    {
        super.setLayout(new BorderLayout());
        super.setBackground(GUIConstants.BACKGROUND_COLOUR);

        slotFont = font.deriveFont(18f);
        borderFonts = font.deriveFont(26f).deriveFont(Font.BOLD);

        gregorianCalendar = getEducationWeek();

        JPanel changeWeekPanel = new JPanel();
        changeWeekPanel.setBackground(GUIConstants.BACKGROUND_COLOUR);
        JLabel selectWeekLabel = new JLabel("Week: " );
        selectWeekLabel.setFont(slotFont);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        JLabel selectionText = new JLabel(formatter.format(gregorianCalendar.getTime()), JLabel.HORIZONTAL);
        selectionText.setFont(slotFont);

        Button previousWeek = new Button("<", GUIConstants.BACKGROUND_COLOUR);
        previousWeek.setFocusPainted(false);
        previousWeek.setBorderPainted(false);
        previousWeek.setFont(slotFont);
        previousWeek.setBackground(GUIConstants.BACKGROUND_COLOUR);
        previousWeek.addActionListener(actionEvent ->
        {
            takeWeek(gregorianCalendar);
            selectionText.setText(formatter.format(gregorianCalendar.getTime()));
            refreshInfo();
        });

        Button nextWeek = new Button(">", GUIConstants.BACKGROUND_COLOUR);
        nextWeek.setFocusPainted(false);
        nextWeek.setBorderPainted(false);
        nextWeek.setFont(slotFont);
        nextWeek.setBackground(GUIConstants.BACKGROUND_COLOUR);
        nextWeek.addActionListener(actionEvent ->
        {
            addWeek(gregorianCalendar);
            selectionText.setText(formatter.format(gregorianCalendar.getTime()));
            refreshInfo();
        });

        changeWeekPanel.add(selectWeekLabel);
        changeWeekPanel.add(previousWeek);
        changeWeekPanel.add(selectionText, BorderLayout.NORTH);
        changeWeekPanel.add(nextWeek);

        super.add(changeWeekPanel, BorderLayout.NORTH);
    }

    public void populate(final List<Session> sessions)
    {
        this.sessions = sessions;
        refreshInfo();
    }

    protected void setSlotActionListener(final ActionListener slotActionListener)
    {
        this.slotActionListener = slotActionListener;
    }

    private void refreshInfo()
    {
        if(gridLayout != null)
        {
            // remove gridlayout from panel
            super.remove(gridLayout);
        }

        gridLayout = new JPanel(new GridLayout(ROWS, COLUMNS));
        gridLayout.setBackground(GUIConstants.BACKGROUND_COLOUR);

        TimetableSlot timetableInfoSlot = new TimetableSlot(borderFonts, TIME_COLOUR, false);
        timetableInfoSlot.setText("Time");
        gridLayout.add(timetableInfoSlot);

        TimetableSlot mondayButton = new TimetableSlot(borderFonts, DAY_COLOUR, false);
        mondayButton.setText("Monday");
        gridLayout.add(mondayButton);

        TimetableSlot tuesdayButton = new TimetableSlot(borderFonts, DAY_COLOUR, false);
        tuesdayButton.setText("Tuesday");
        gridLayout.add(tuesdayButton);

        TimetableSlot wednesdayButton = new TimetableSlot(borderFonts, DAY_COLOUR, false);
        wednesdayButton.setText("Wednesday");
        gridLayout.add(wednesdayButton);

        TimetableSlot thursdayButton = new TimetableSlot(borderFonts, DAY_COLOUR, false);
        thursdayButton.setText("Thursday");
        gridLayout.add(thursdayButton);

        TimetableSlot fridayButton = new TimetableSlot(borderFonts, DAY_COLOUR, false);
        fridayButton.setText("Friday");
        gridLayout.add(fridayButton);

        for(int y = 0; y < LESSON_ROWS; y++)
        {
            // Put the times on the left side of the timetable
            TimetableSlot timeButton = new TimetableSlot(borderFonts, TIME_COLOUR, false);
            timeButton.setText(SessionTime.getTimeString(y));
            gridLayout.add(timeButton);

            Color rowColour = y % 2 == 0 ? EVEN_ROW_COLOUR : ODD_ROW_COLOUR;
            for(int x = 0; x < LESSON_COLUMNS; x++)
            {
                // Clear the existing slot
                SessionSlot button = new SessionSlot(slotFont, rowColour, new SessionTime(x - DAY_OFFSET, y));
                button.addActionListener(slotActionListener);
                sessionSlots[y][x] = button;
                gridLayout.add(button);
            }
        }

        // Iterate through the lessons, adding each to the timetable
        for(final Session session : sessions)
        {
            final SessionTime sessionTime = session.getSessionTime();

            int day = sessionTime.getDay();
            if(!sessionTime.isRepeatedWeekly())
            {
                if(!weekContainsSessionTime(gregorianCalendar, sessionTime))
                {
                    continue;
                }

                day = getDayOfWeek(sessionTime);
            }

            // Calculate what lesson slot the lesson info should be added to
            int x = day + DAY_OFFSET;
            int y = sessionTime.getHoursFromDayStart();
            // Check the slot at that position exists
            if(sessionSlots[y][x] != null)
            {
                // Only add one lesson per slot
                if(sessionSlots[y][x].getLessonId() == SessionSlot.NO_SESSION_OCCUPYING_SLOT)
                {
                    // Set the lesson slot information
                    if(session instanceof ParentMeeting)
                    {
                        sessionSlots[y][x].addParentMeetingId(session.getUniqueId());
                    }

                    if(session instanceof Lesson)
                    {
                        sessionSlots[y][x].setLessonId(session.getUniqueId());
                    }

                    sessionSlots[y][x].setText(session.toString());
                }
            }
            else
            {
                System.err.println("Error: No timetable slot at y: " + y + ", x: " + x);
            }
        }

        gridLayout.setBorder(new EmptyBorder(0, 20, 20, 20));
        super.add(gridLayout, BorderLayout.CENTER);
        super.updateUI();
    }
}
