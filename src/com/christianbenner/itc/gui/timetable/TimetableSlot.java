package com.christianbenner.itc.gui.timetable;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TimetableSlot extends JButton
{
    public TimetableSlot(Font font, Color colour, boolean colourOnHover)
    {
        super.setPreferredSize(new Dimension(100, 50));

        super.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        super.setBackground(colour);
        super.setFont(font);
        super.setFocusPainted(false);
        super.setLayout(new BorderLayout());

        if(colourOnHover)
        {
            super.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseEntered(MouseEvent e)
                {
                    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    setBackground(colour.darker());
                    setForeground(Color.WHITE);
                }

                @Override
                public void mouseExited(MouseEvent e)
                {
                    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    setBackground(colour);
                    setForeground(Color.DARK_GRAY);
                }
            });
        }
    }
}
