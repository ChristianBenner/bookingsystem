package com.christianbenner.itc.gui.timetable;

import com.christianbenner.itc.data.SessionTime;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SessionSlot extends TimetableSlot
{
    public static final int NO_SESSION_OCCUPYING_SLOT = -1;

    private SessionTime sessionTime;
    private int lessonId;

    private List<Integer> parentMeetingIds;

    public SessionSlot(Font font, Color color, SessionTime sessionTime)
    {
        super(font, color, true);
        this.sessionTime = sessionTime;
        this.lessonId = NO_SESSION_OCCUPYING_SLOT;
        parentMeetingIds = new ArrayList<>();
    }

    public void setLessonId(final int lessonId)
    {
        this.lessonId = lessonId;
    }

    public int getLessonId()
    {
        return lessonId;
    }

    public void addParentMeetingId(final int parentMeetingId)
    {
        parentMeetingIds.add(parentMeetingId);
    }

    public boolean containsParentMeeting()
    {
        return parentMeetingIds.size() != 0;
    }

    public List<Integer> getParentMeetingIds()
    {
        return parentMeetingIds;
    }

    // Return copy to prevent editing reference
    public SessionTime getSessionTime()
    {
        return new SessionTime(sessionTime);
    }
}