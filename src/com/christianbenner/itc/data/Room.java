package com.christianbenner.itc.data;

import com.christianbenner.itc.Directory;

/**
 * Stores information on a room that exists at the tuition center
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Room extends DirectoryObject
{
    // Name of building that houses the room
    private String buildingName;

    // Number of the room
    private int roomNumber;

    /**
     * Constructs a Room object.
     *
     * @param buildingName  Name of building that houses the room
     * @param roomNumber    Number of the room
     * @since               1.0
     */
    public Room(final String buildingName, final int roomNumber)
    {
        this.buildingName = buildingName;
        this.roomNumber = roomNumber;
    }

    /**
     * Get the name of the building that the room is housed in
     *
     * @return  Name of building that houses the room
     * @since   1.0
     */
    public String getBuildingName()
    {
        return buildingName;
    }

    /**
     * Set the name of the building that the room is housed in
     *
     * @param buildingName  Name of building that houses the room
     * @since               1.0
     */
    public void setBuildingName(final String buildingName)
    {
        this.buildingName = buildingName;
        super.notifyChangeListeners();
    }

    /**
     * Get the room number
     *
     * @return  Number of the room
     * @since   1.0
     */
    public int getRoomNumber()
    {
        return roomNumber;
    }

    /**
     * Set the room number
     *
     * @param roomNumber    Number of the room
     * @since               1.0
     */
    public void setRoomNumber(final int roomNumber)
    {
        this.roomNumber = roomNumber;
        super.notifyChangeListeners();
    }

    /**
     * Check if the two objects are equal. The objects are determined the same if the raw values are the same. This
     * includes having the same id, building name and room number
     *
     * @param obj   The room to compare to
     * @since       1.0
     */
    @Override
    public boolean equals(Object obj)
    {
        // If the object is a reference to itself, return true
        if(obj == this)
        {
            return true;
        }

        // Check if the object being compared is the same type - If it isn't then it isn't the same object
        if(!(obj instanceof Room))
        {
            return false;
        }

        // Cast the object to an Room type as we checked that it is above
        Room other = (Room) obj;

        // Compare the data
        return super.getUniqueId() == other.getUniqueId() &&
                buildingName.compareTo(other.buildingName) == 0 &&
                roomNumber == other.roomNumber;
    }

    /**
     * Compares the rooms information with a search phrase
     *
     * @param searchPhrase  Phrase to compare to the room information
     * @return              MatchType that represents how well the room matches the search phrase
     * @since               1.0
     */
    @Override
    public Directory.MatchType compare(String searchPhrase)
    {
        // If the number and building name is in the search phrase it is a good match
        final String buildingNameSearchPhrase = Directory.convertToSearchPhrase(buildingName);
        final String roomNumberSearchPhrase = Integer.toString(roomNumber);

        // If the objects ID has been entered it is a good match
        if(searchPhrase.compareTo(Integer.toString(getUniqueId())) == 0)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        if(searchPhrase.contains(buildingNameSearchPhrase) && searchPhrase.contains(roomNumberSearchPhrase))
        {
            // If the number is contained in the search phrase it is a good match
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }
        else if(searchPhrase.contains(buildingNameSearchPhrase) || searchPhrase.contains(roomNumberSearchPhrase))
        {
            // If the search phrase contains the building name or room number it is an ok match
            return Directory.MatchType.SEARCH_RESULT_OK_MATCH;
        }
        else if(buildingNameSearchPhrase.contains(searchPhrase))
        {
            // If the building name contains the search phrase it is a basic match
            return Directory.MatchType.SEARCH_RESULT_BASIC_MATCH;
        }

        // If the
        return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
    }

    /**
     * Get a string containing information on the Room. The string will consist of all of the values that make
     * that Room such as building name and room number.
     *
     * @return  A string containing information about the Room
     * @since   1.0
     */
    @Override
    public String toString()
    {
        return String.format(buildingName + ", " + roomNumber);
    }
}
