package com.christianbenner.itc.data;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.Directory;

import java.util.ArrayList;
import java.util.List;

import static com.christianbenner.itc.Directory.getSearchScore;

public class Lesson extends Session
{
    // Score of a good match overall
    private static final int OVERALL_SCORE_GOOD_MATCH = 5;

    // Score of an ok match overall
    private static final int OVERALL_SCORE_OK_MATCH = 3;

    // Score of a basic match overall
    private static final int OVERALL_SCORE_BASIC_MATCH = 1;

    // Number of spaces for students in the class
    private final int maxSpaces;

    // Subject of the lesson
    private int subjectId;

    // List of students enrolled onto the lesson
    private List<Integer> students;

    /**
     * Constructs an Member object with all the necessary data and an existing ID. Protected visibility to prevent an
     * object of type Member being constructed - the class is designed to be derived from.
     *
     * @param tutorId     ID of the tutor Tutor that is hosting the lesson
     * @param roomId      ID of the tutor Room that the lesson is in
     * @param subjectId   ID of the tutor Subject of the lesson
     * @param sessionTime Time of the lesson
     * @param maxSpaces   Number of spaces for students in the class
     * @see SessionTime
     * @since 1.0
     */
    public Lesson(int tutorId, int roomId, int subjectId, SessionTime sessionTime, int maxSpaces)
    {
        super(tutorId, roomId, sessionTime);
        this.students = new ArrayList<>();
        this.subjectId = subjectId;
        this.maxSpaces = maxSpaces;
    }

    /**
     * Get a list of the students enrolled onto the lesson. This returns a copy of the list of students and therefore
     * editing it will not change the students enrolled on the lesson. To change the students enrolled you have to add,
     * remove or set a new list of students.
     *
     * @return  Returns a copy of the list of students enrolled onto the lesson
     * @since   1.0
     */
    public List<Integer> getStudents()
    {
        // Return a copy of the list so that they cannot be edited without using this class
        final List<Integer> copy = new ArrayList<>(students);
        return copy;
    }

    /**
     * Set the list of students on the lesson. No students will not be added to the lesson if there is more than the
     * maximum spaces available. This will remove any current students.
     *
     * @return  Returns a copy of the list of students enrolled onto the lesson
     * @since   1.0
     */
    public boolean setStudents(List<Integer> students)
    {
        // Check if there is enough space for the students
        if(students.size() > maxSpaces)
        {
            return false;
        }

        this.students = students;
        super.notifyChangeListeners();
        return true;
    }

    /**
     * Get the maximum number of spaces for students that the lesson supports
     *
     * @return  Maximum number of spaces for students that the lesson supports
     * @since   1.0
     */
    public int getMaxSpaces()
    {
        return maxSpaces;
    }

    /**
     * Check if there is space left for a student in the lesson
     *
     * @return  True if there is space left, else false
     * @since   1.0
     */
    public boolean hasSpace()
    {
        return getSpacesLeft() > 0;
    }

    /**
     * Get the number of spaces left for students in the lesson
     *
     * @return  The number of spaces left for students in the lesson
     * @since   1.0
     */
    public int getSpacesLeft()
    {
        return maxSpaces - students.size();
    }

    /**
     * Get the number of student enrolled onto the lesson
     *
     * @return  The number of students enrolled onto the lesson
     * @since   1.0
     */
    public int getNumberOfStudentsEnrolled()
    {
        return students.size();
    }

    /**
     * Adds a student to the lesson. A student will not be added if there is no remaining space
     *
     * @param studentId ID of the Student to add to the lesson
     * @return          True if the student has been added successfully else false. A student may not be added
     *                  successfully if there is no remaining space in the lesson
     * @since           1.0
     */
    public boolean addStudent(final int studentId)
    {
        // If the class does not have space, return false
        if(!hasSpace())
        {
            return false;
        }

        // Otherwise, add the student and return true
        students.add(studentId);
        super.notifyChangeListeners();
        return true;
    }

    /**
     * Removes a student from the lesson.
     *
     * @param studentId ID of the Student to remove from the lesson
     * @since           1.0
     */
    public void removeStudent(final int studentId)
    {
        // Has to be cast to an object so that the list method does not think we are trying to remove a student ID at
        // an index
        students.remove(new Integer(studentId));

        super.notifyChangeListeners();
    }

    /**
     * Check if the lesson contains a student or not
     *
     * @param studentId ID of the Student to check if the lesson contains them
     * @return          True if the lesson contains the student, else false
     * @since           1.0
     */
    public boolean containsStudent(final int studentId)
    {
        return students.contains(studentId);
    }

    /**
     * Get the ID of the subject of the lesson
     *
     * @return  ID of the subject of the lesson
     * @since   1.0
     */
    public int getSubjectId()
    {
        return subjectId;
    }

    /**
     * Set the ID of the subject of the lesson
     *
     * @param subjectId ID of the subject of the lesson
     * @since           1.0
     */
    public void setSubjectId(final int subjectId)
    {
        this.subjectId = subjectId;
        super.notifyChangeListeners();
    }

    /**
     * Compare a Lesson with a search phrase. The tutor, room and subject will be compared to the search phrase. The
     * match is determined by calculated score. Each good match gains 3 score, ok match gains 2 score and basic match
     * gains 1 score.
     *
     * SEARCH_RESULT_GOOD_MATCH: 6 score or more
     * SEARCH_RESULT_OK_MATCH: 4 score or more
     * SEARCH_RESULT_BASIC_MATCH: 2 score or more
     * SEARCH_RESULT_NO_MATCH: Less than 2 score
     *
     * @param searchPhrase  Search phrase to compare against the lesson
     * @return              A MatchType that represents a how well the subject matches the search phrase
     * @since               1.0
     */
    @Override
    public Directory.MatchType compare(final String searchPhrase)
    {
        // Retrieve each object from the directories via their IDs
        final Directory.MatchType tutorMatchType = Directories.getTutorDirectory().
                get(super.getTutorId()).compare(searchPhrase);
        final Directory.MatchType roomMatchType = Directories.getRoomDirectory().
                get(super.getRoomId()).compare(searchPhrase);
        final Directory.MatchType subjectMatchType = Directories.getSubjectDirectory().
                get(getSubjectId()).compare(searchPhrase);

        // Calculate overall search score based on each objects match to the search phrase
        final int searchScore = getSearchScore(tutorMatchType) + getSearchScore(roomMatchType) +
                getSearchScore(subjectMatchType);

        // If the objects ID has been entered it is a good match
        if(searchPhrase.compareTo(Integer.toString(getUniqueId())) == 0)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        // If there is no space, do not show the lesson
        if(getSpacesLeft() == 0)
        {
            return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
        }

        // If the overall score reaches this overall good score threshold return a good match
        if(searchScore >= OVERALL_SCORE_GOOD_MATCH)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        // If the overall score reaches this overall ok score threshold return an ok match
        if(searchScore >= OVERALL_SCORE_OK_MATCH)
        {
            return Directory.MatchType.SEARCH_RESULT_OK_MATCH;
        }

        // If the overall score reaches this overall basic score threshold return a basic match
        if(searchScore >= OVERALL_SCORE_BASIC_MATCH)
        {
            return Directory.MatchType.SEARCH_RESULT_BASIC_MATCH;
        }

        // No match if the score is less than the basic score threshold
        return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
    }

    @Override
    public String toString()
    {
        return Directories.getTutorDirectory().get(getTutorId()).toString() + ", " +
                Directories.getSubjectDirectory().get(getSubjectId()).getName() + " (" +
                Directories.getRoomDirectory().get(super.getRoomId()).getBuildingName() + ", " +
                Directories.getRoomDirectory().get(super.getRoomId()).getRoomNumber() + ")";
    }
}
