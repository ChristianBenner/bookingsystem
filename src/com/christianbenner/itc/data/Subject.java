package com.christianbenner.itc.data;

import com.christianbenner.itc.Directory;

/**
 * Subject is a data class that contains information on a subject such as its unique ID and its name.
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Subject extends DirectoryObject
{
    // Name of the subjects
    private String name;

    /**
     * Construct Subject object with a name
     *
     * @param name  The name of the subject
     * @since       1.0
     */
    public Subject(final String name)
    {
        this.name = name;
    }

    /**
     * Retrieve the subjects name
     *
     * @return  Returns the subjects name as a String
     * @since   1.0
     */
    public String getName()
    {
        return name;
    }

    /**
     * Set the subjects name
     *
     * @param name  Name of the subject
     * @since       1.0
     */
    public void setName(final String name)
    {
        this.name = name;
        super.notifyChangeListeners();
    }

    /**
     * Check if the two objects are equal. The objects are determined the same if the raw values are the same. This
     * includes having the same id and name.
     *
     * @param obj   The subject to compare to
     * @since       1.0
     */
    @Override
    public boolean equals(Object obj)
    {
        // If the object is a reference to itself, return true
        if(obj == this)
        {
            return true;
        }

        // Check if the object being compared is the same type - If it isn't then it isn't the same object
        if(!(obj instanceof Subject))
        {
            return false;
        }

        // Cast the object to an Subject type as we checked that it is above
        Subject other = (Subject)obj;

        // Compare the data
        return super.getUniqueId() == other.getUniqueId() &&
                name.compareTo(other.name) == 0;
    }

    /**
     * Get a string containing information on the Subject. The string will consist of all of the values that make
     * that Subject.
     *
     * @return  A string containing information about the Subject
     * @since   1.0
     */
    @Override
    public String toString()
    {
        return name;
    }

    /**
     * Compare a Subject with a search phrase. The subjects name will be compared to the search phrase and given a match
     * ranking depending on how closely related they are.
     *
     * SEARCH_RESULT_GOOD_MATCH: A good match is classified by the search phrase containing the subjects name (name
     * converted to search phrase)
     * SEARCH_RESULT_BASIC_MATCH: A basic match is classified by the subject name (converted to a search phrase)
     * containing the search phrase
     * SEARCH_RESULT_NO_MATCH: No match if the search phrase doesn't contain the subject name and the subject name also
     * does not contain the search phrase
     *
     * @param searchPhrase  Search phrase to compare against the name of the Subject
     * @return              A byte that represents a how well the subject matches the search phrase (see description)
     * @since               1.0
     */
    @Override
    public Directory.MatchType compare(String searchPhrase)
    {
        // Convert the subjects name into an appropriate search phrase
        final String nameSearchPhrase = Directory.convertToSearchPhrase(getName());

        // If the objects ID has been entered it is a good match
        if(searchPhrase.compareTo(Integer.toString(getUniqueId())) == 0)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        // Compare and determine how well the subjects name matches the search phrase
        if(searchPhrase.contains(nameSearchPhrase))
        {
            // If the search phrase contains the name it is a good match
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }
        else if(nameSearchPhrase.contains(searchPhrase))
        {
            // If the name contains the search phrase it is a basic match
            return Directory.MatchType.SEARCH_RESULT_BASIC_MATCH;
        }

        return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
    }
}
