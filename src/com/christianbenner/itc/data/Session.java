package com.christianbenner.itc.data;

import com.christianbenner.itc.Directories;
import com.christianbenner.itc.Directory;

import static com.christianbenner.itc.Directory.getSearchScore;

/**
 * Stores information on a session that exists at the tuition center
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public abstract class Session extends DirectoryObject
{
    // Score of a good match overall
    private static final int OVERALL_SCORE_GOOD_MATCH = 4;

    // Score of an ok match overall
    private static final int OVERALL_SCORE_OK_MATCH = 3;

    // Score of a basic match overall
    private static final int OVERALL_SCORE_BASIC_MATCH = 2;

    // Tutor that is hosting the session
    private int tutorId;

    // Room that the session is in
    private int roomId;

    // Time of the session
    private SessionTime sessionTime;

    /**
     * Constructs an Member object with all the necessary data and an existing ID. Protected visibility to prevent an
     * object of type Member being constructed - the class is designed to be derived from.
     *
     * @param tutorId       ID of the tutor Tutor that is hosting the session
     * @param roomId        ID of the tutor Room that the session is in
     * @param sessionTime    Time of the session
     * @see                 SessionTime
     * @since               1.0
     */
    protected Session(final int tutorId,
                      final int roomId,
                      final SessionTime sessionTime)
    {
        this.tutorId = tutorId;
        this.roomId = roomId;
        this.sessionTime = sessionTime;
    }

    /**
     * Get the ID of the tutor hosting the session
     *
     * @return  ID of the tutor hosting the session
     * @since   1.0
     */
    public int getTutorId()
    {
        return tutorId;
    }

    /**
     * Set the ID of the tutor hosting the session
     *
     * @param tutorId   ID of the tutor hosting the session
     * @since           1.0
     */
    public void setTutorId(final int tutorId)
    {
        this.tutorId = tutorId;
        super.notifyChangeListeners();
    }

    /**
     * Get the ID of the room that the session is hosted in
     *
     * @return  ID of the room that the session is hosted in
     * @since   1.0
     */
    public int getRoomId()
    {
        return roomId;
    }

    /**
     * Set the ID of the room that the session is hosted in
     *
     * @param roomId    ID of the room that the session is hosted in
     * @since           1.0
     */
    public void setRoomId(final int roomId)
    {
        this.roomId = roomId;
        super.notifyChangeListeners();
    }

    /**
     * Get the time of the session. This returns a copy of the session time and cannot be edited. To change the session
     * time, create a new SessionTime and set it
     *
     * @return  Copy of the SessionTime
     * @since   1.0
     */
    public SessionTime getSessionTime()
    {
        // Return copy so that the session time cannot be edited
        final SessionTime copy = new SessionTime(sessionTime);
        return copy;
    }

    /**
     * Set the time of the session
     *
     * @param sessionTime    New time for the session to be hosted on
     * @since               1.0
     */
    public void setSessionTime(final SessionTime sessionTime)
    {
        this.sessionTime = sessionTime;
        super.notifyChangeListeners();
    }

    /**
     * Check if the two objects are equal. The objects are determined the same if the raw values are the same. This
     * includes having the same tutor, room and subject IDs as well as being on at the same time
     *
     * @param obj   The session to compare to
     * @since       1.0
     */
    @Override
    public boolean equals(Object obj)
    {
        // If the object is a reference to itself, return true
        if(obj == this)
        {
            return true;
        }

        // Check if the object being compared is the same type - If it isn't then it isn't the same object
        if(!(obj instanceof Session))
        {
            return false;
        }

        // Cast the object to an Session type as we checked that it is above
        Session other = (Session) obj;

        // Compare the data
        return super.getUniqueId() == other.getUniqueId() &&
                tutorId == other.tutorId &&
                roomId == other.roomId &&
                sessionTime.equals(other.sessionTime);
    }
    
    /**
     * Compare a Session with a search phrase. The tutor, room and subject will be compared to the search phrase. The
     * match is determined by calculated score. Each good match gains 3 score, ok match gains 2 score and basic match
     * gains 1 score.
     *
     * SEARCH_RESULT_GOOD_MATCH: 6 score or more
     * SEARCH_RESULT_OK_MATCH: 4 score or more
     * SEARCH_RESULT_BASIC_MATCH: 2 score or more
     * SEARCH_RESULT_NO_MATCH: Less than 2 score
     *
     * @param searchPhrase  Search phrase to compare against the session
     * @return              A MatchType that represents a how well the subject matches the search phrase
     * @since               1.0
     */
    @Override
    public Directory.MatchType compare(final String searchPhrase)
    {
        // Retrieve each object from the directories via their IDs
        final Directory.MatchType tutorMatchType = Directories.getTutorDirectory().get(tutorId).compare(searchPhrase);
        final Directory.MatchType roomMatchType = Directories.getRoomDirectory().get(roomId).compare(searchPhrase);

        // Calculate overall search score based on each objects match to the search phrase
        final int searchScore = getSearchScore(tutorMatchType) + getSearchScore(roomMatchType);

        // If the objects ID has been entered it is a good match
        if(searchPhrase.compareTo(Integer.toString(getUniqueId())) == 0)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        // If the overall score reaches this overall good score threshold return a good match
        if(searchScore >= OVERALL_SCORE_GOOD_MATCH)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        // If the overall score reaches this overall ok score threshold return an ok match
        if(searchScore >= OVERALL_SCORE_OK_MATCH)
        {
            return Directory.MatchType.SEARCH_RESULT_OK_MATCH;
        }

        // If the overall score reaches this overall basic score threshold return a basic match
        if(searchScore >= OVERALL_SCORE_BASIC_MATCH)
        {
            return Directory.MatchType.SEARCH_RESULT_BASIC_MATCH;
        }

        // No match if the score is less than the basic score threshold
        return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
    }

    @Override
    public String toString()
    {
        return Directories.getTutorDirectory().get(tutorId).getPrefix() + " " +
                Directories.getTutorDirectory().get(tutorId).getLastName() + ", " +
                Directories.getRoomDirectory().get(roomId);
    }
}
