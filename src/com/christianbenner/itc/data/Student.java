package com.christianbenner.itc.data;

import com.christianbenner.itc.Directories;

import java.util.ArrayList;
import java.util.List;

/**
 * Student is a subclass of Member. It contains information relevant to students in the system.
 *
 * @see         Member
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Student extends Member
{
    /**
     * Construct a Student object. Calls the base class constructor
     *
     * @param firstName     First name of the ITC member
     * @param lastName      Last name of the ITC member
     * @param address       Address of the ITC member
     * @param phoneNumber   Phone number of the ITC member
     * @since               1.0
     */
    public Student(final String firstName,
                   final String lastName,
                   final Address address,
                   final String phoneNumber)
    {
        super(firstName, lastName, address, phoneNumber);
    }

    /**
     * Get student as a string. This will return the students full name
     *
     * @since 1.0
     */
    @Override
    public String toString()
    {
        return super.getFullName();
    }

    public List<ParentMeeting> getParentMeetings()
    {
        // List that will contain all the parent meetings for a student
        final List<ParentMeeting> parentMeetings = new ArrayList<>();

        // Grab all the parent meetings
        final List<ParentMeeting> allParentMeetings = Directories.getParentMeetingDirectory().getAll();

        // Iterate through all parent meetings adding them to the list if the student is on them
        for(ParentMeeting parentMeeting : allParentMeetings)
        {
            // Check if the student ID of the given student matches that of the student on the lesson
            if(parentMeeting.getStudentId() == getUniqueId())
            {
                parentMeetings.add(parentMeeting);
            }
        }

        return parentMeetings;
    }

    public List<Lesson> getLessons()
    {
        List<Lesson> enrolledLessons = new ArrayList<>();

        // Iterate through the lesson saves and see if the member is on the lesson. If they are, add the lesson to the
        // list
        List<Lesson> allLessons = Directories.getLessonDirectory().getAll();

        for(Lesson lesson : allLessons)
        {
            List<Integer> studentIds = lesson.getStudents();

            for(Integer studentId : studentIds)
            {
                if(getUniqueId() == studentId)
                {
                    enrolledLessons.add(lesson);
                    break;
                }
            }
        }

        return enrolledLessons;
    }
}

