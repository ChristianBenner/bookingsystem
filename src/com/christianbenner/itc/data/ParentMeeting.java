package com.christianbenner.itc.data;

import com.christianbenner.itc.Directories;

import java.util.*;

public class ParentMeeting extends Session
{
    public static final int MAX_SLOTS = 3;
    private static final int HOUR_IN_MINUTES = 60;
    private static final int DURATION_MINUTES = HOUR_IN_MINUTES / MAX_SLOTS;

    private static final int NO_STUDENT = -1;

    private final int slot;
    private int studentId;

    /**
     * Constructs an Member object with all the necessary data and an existing ID. Protected visibility to prevent an
     * object of type Member being constructed - the class is designed to be derived from.
     *
     * @param tutorId       ID of the tutor Tutor that is hosting the parent meeting
     * @param roomId        ID of the tutor Room that the parent meeting is in
     * @param sessionTime   Time of the parent meeting
     * @see                 SessionTime
     * @since               1.0
     */
    public ParentMeeting(final int tutorId,
                         final int roomId,
                         final SessionTime sessionTime,
                         final int slot)
    {
        super(tutorId, roomId, sessionTime);
        this.slot = slot;
        this.studentId = NO_STUDENT;
    }

    public boolean book(final int studentId)
    {
        if(!isAvailable())
        {
            return false;
        }

        this.studentId = studentId;
        notifyChangeListeners();
        return true;
    }

    public boolean isAvailable()
    {
        return studentId == NO_STUDENT;
    }

    public int getSlot()
    {
        return slot;
    }

    public int getStudentId()
    {
        return studentId;
    }

    public String getAvailableInfo()
    {
        // Slot number
        GregorianCalendar start = new GregorianCalendar(getSessionTime().getYear(),
                getSessionTime().getMonth() - 1, getSessionTime().getDay(),
                SessionTime.DAY_START_HOURS + getSessionTime().getHoursFromDayStart(),
                slot * DURATION_MINUTES);
        return start.getTime().toString();
    }

    @Override
    public String toString()
    {
        return "Parent Meeting (" + Directories.getRoomDirectory().get(super.getRoomId()).getBuildingName() +
                ", " + Directories.getRoomDirectory().get(super.getRoomId()).getRoomNumber() + ")";
    }
}
