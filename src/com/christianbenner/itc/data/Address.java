package com.christianbenner.itc.data;

/**
 * Address class contains all data associated to describing a physical house address.
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Address
{
    // Post code of the address
    private String postCode;

    // First address line e.g. '52 Example Road'
    private String addressLineOne;

    // Second address line e.g. 'Example Village'
    private String addressLineTwo;

    // Third address line e.g. 'Example Town'
    private String addressLineThree;

    /**
     * Creates an Address object with all the required data
     *
     * @param postCode          Post code string
     * @param addressLineOne    Address line 1 string
     * @param addressLineTwo    Address line 2 string
     * @param addressLineThree  Address line 3 string
     * @see     Address
     * @since   1.0
     */
    public Address(final String postCode,
                   final String addressLineOne,
                   final String addressLineTwo,
                   final String addressLineThree)
    {
        this.postCode = postCode;
        this.addressLineOne = addressLineOne;
        this.addressLineTwo = addressLineTwo;
        this.addressLineThree = addressLineThree;
    }

    /**
     * Copy constructor. Creates an Address object using an existing Address object. All of the data inside the given
     * Address object will be copied.
     *
     * @param address   An existing Address object
     * @see     Address
     * @since   1.0
     */
    public Address(final Address address)
    {
        this.postCode = address.postCode;
        this.addressLineOne = address.addressLineOne;
        this.addressLineTwo = address.addressLineTwo;
        this.addressLineThree = address.addressLineThree;
    }

    /**
     * Retrieve the post code of the address
     *
     * @return  Post code of the address as a String
     * @since   1.0
     */
    public String getPostCode()
    {
        return postCode;
    }

    /**
     * Set a new post code for the address
     *
     * @param postCode  A new post code for the address
     * @since   1.0
     */
    public void setPostCode(String postCode)
    {
        this.postCode = postCode;
    }

    /**
     * Retrieve the first address line
     *
     * @return  Address line 1 as a String
     * @since   1.0
     */
    public String getAddressLineOne()
    {
        return addressLineOne;
    }

    /**
     * Set the first address line
     *
     * @param addressLineOne    First address line
     * @since   1.0
     */
    public void setAddressLineOne(String addressLineOne)
    {
        this.addressLineOne = addressLineOne;
    }

    /**
     * Retrieve the second address line
     *
     * @return  Address line 2 as a String
     * @since   1.0
     */
    public String getAddressLineTwo()
    {
        return addressLineTwo;
    }

    /**
     * Set the second address line
     *
     * @param addressLineTwo    Second address line
     * @since   1.0
     */
    public void setAddressLineTwo(String addressLineTwo)
    {
        this.addressLineTwo = addressLineTwo;
    }

    /**
     * Retrieve the third address line
     *
     * @return  Address line 3 as a String
     * @since   1.0
     */
    public String getAddressLineThree()
    {
        return addressLineThree;
    }

    /**
     * Set the third address line
     *
     * @param addressLineThree  Third address line
     * @since   1.0
     */
    public void setAddressLineThree(String addressLineThree)
    {
        this.addressLineThree = addressLineThree;
    }

    /**
     * Check if the address matches another
     *
     * @param obj   The other address object
     * @since   1.0
     */
    @Override
    public boolean equals(Object obj)
    {
        // If the object is a reference to itself, return true
        if(obj == this)
        {
            return true;
        }

        // Check if the object being compared is the same type - If it isn't then it isn't the same object
        if(!(obj instanceof Address))
        {
            return false;
        }

        // Cast the object to an Address type as we checked that it is above
        Address other = (Address)obj;

        // Compare the data
        return postCode.compareTo(other.postCode) == 0 &&
                addressLineOne.compareTo(other.addressLineOne) == 0 &&
                addressLineTwo.compareTo(other.addressLineTwo) == 0 &&
                addressLineThree.compareTo(other.addressLineThree) == 0;
    }

    /**
     * Get an string containing information on the Address. The string will consist of all of the values that make
     * that Address.
     *
     * @return  A string containing information about the Address
     * @since   1.0
     */
    @Override
    public String toString()
    {
        return addressLineOne + ", " + addressLineTwo;
    }
}
