package com.christianbenner.itc.data;

import com.christianbenner.itc.Directory;

/**
 * Member class is designed to be a base class containing data relevant to all Inspire Tuition Centre members such as
 * students and tutors.
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Member extends DirectoryObject
{
    // Members first name
    private String firstName;

    // Members last name
    private String lastName;

    // Address of the member
    private Address address;

    // The phone number
    private String phoneNumber;

    /**
     * Constructs an Member object with all the necessary data. Protected visibility to prevent an object of type Member
     * being constructed - the class is designed to be derived from.
     *
     * @param firstName     First name of the ITC member
     * @param lastName      Last name of the ITC member
     * @param address       Address of the ITC member
     * @param phoneNumber   Phone number of the ITC member
     * @see                 Address
     * @since               1.0
     */
    protected Member(final String firstName,
                     final String lastName,
                     final Address address,
                     final String phoneNumber)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    /**
     * Retrieve the first name of the ITC member
     *
     * @return  The first name of the ITC member as a string
     * @since   1.0
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * Set the first name of the ITC member
     *
     * @param firstName The new first name of the ITC member
     * @since           1.0
     */
    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
        super.notifyChangeListeners();
    }

    /**
     * Retrieve the last name of the ITC member
     *
     * @return  The last name of the ITC member as a string
     * @since   1.0
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * Set the last name of the ITC member
     *
     * @param lastName  The new last name of the ITC member
     * @since           1.0
     */
    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
        super.notifyChangeListeners();
    }

    /**
     * Retrieve the full name of the ITC member. This is the first and last names concatenated with a space character
     * between them e.g. 'FirstName LastName'.
     *
     * @return  The full name of the ITC member as a string
     * @since   1.0
     */
    public String getFullName()
    {
        return getFirstName() + ' ' + getLastName();
    }

    /**
     * Retrieve the address of the ITC member
     *
     * @return  The address of the ITC member as an Address object
     * @see     Address
     * @since   1.0
     */
    public Address getAddress()
    {
        // Return copy so that the data cannot be altered outside this class
        final Address copy = new Address(address);
        return copy;
    }

    /**
     * Set the address of the ITC member
     *
     * @param address   The new address of the ITC member
     * @since           1.0
     */
    public void setAddress(Address address)
    {
        // Copy the address over
        this.address = new Address(address);

        super.notifyChangeListeners();
    }

    /**
     * Retrieve the phone number of the ITC member
     *
     * @return  The phone number of the ITC member as a string
     * @since   1.0
     */
    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    /**
     * Set the phoneNumber of the ITC member
     *
     * @param phoneNumber   The new phoneNumber of the ITC member
     * @since               1.0
     */
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
        super.notifyChangeListeners();
    }

    /**
     * Compares the member information with a search phrase
     *
     * @param searchPhrase  Phrase to compare to the member information
     * @return              MatchType that represents how well the member matches the search phrase
     * @since               1.0
     */
    @Override
    public Directory.MatchType compare(final String searchPhrase)
    {
        // Convert the members full name into an appropriate search phrase
        final String fullNameSearchPhrase = Directory.convertToSearchPhrase(getFullName());

        // Convert the members first name into an appropriate search phrase
        final String firstNameSearchPhrase = Directory.convertToSearchPhrase(getFirstName());

        // Convert the members last name into an appropriate search phrase
        final String lastNameSearchPhrase = Directory.convertToSearchPhrase(getLastName());

        // If the objects ID has been entered it is a good match
        if(searchPhrase.compareTo(Integer.toString(getUniqueId())) == 0)
        {
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }

        // Compare and determine how well the members name matches the search phrase
        if(searchPhrase.contains(fullNameSearchPhrase))
        {
            // If the search phrase contains the full name it is a good match
            return Directory.MatchType.SEARCH_RESULT_GOOD_MATCH;
        }
        else if(searchPhrase.contains(firstNameSearchPhrase))
        {
            // If the search phrase contains the first name it is an ok match
            return Directory.MatchType.SEARCH_RESULT_OK_MATCH;
        }
        else if(searchPhrase.contains(lastNameSearchPhrase))
        {
            // If the search phrase contains the last name it is an ok match
            return Directory.MatchType.SEARCH_RESULT_OK_MATCH;
        }
        else if(fullNameSearchPhrase.contains(searchPhrase))
        {
            // If the full name contains the search phrase it is a somewhat match and is worth including in the
            // search results
            return Directory.MatchType.SEARCH_RESULT_BASIC_MATCH;
        }

        return Directory.MatchType.SEARCH_RESULT_NO_MATCH;
    }

    /**
     * Check if the two objects are equal. The objects are determined the same if the raw values are the same. This
     * includes having the same id, first name, last name, phone number and address.
     *
     * @param obj   The member to compare to
     * @since       1.0
     */
    @Override
    public boolean equals(Object obj)
    {
        // If the object is a reference to itself, return true
        if(obj == this)
        {
            return true;
        }

        // Check if the object being compared is the same type - If it isn't then it isn't the same object
        if(!(obj instanceof Member))
        {
            return false;
        }

        // Cast the object to an Member type as we checked that it is above
        Member other = (Member)obj;

        // Compare the data
        return firstName.compareTo(other.firstName) == 0 &&
                lastName.compareTo(other.lastName) == 0 &&
                phoneNumber.compareTo(other.phoneNumber) == 0 &&
                address.equals(other.address);
    }

    /**
     * Get a string containing information on the Member. The string will consist of all of the values that make
     * that Member.
     *
     * @return  A string containing information about the Member
     * @since   1.0
     */
    @Override
    public String toString()
    {
        return String.format("id: '%d', firstName: '%s', lastName: '%s', %s, phoneNumber '%s'",
                super.getUniqueId(), firstName, lastName, address, phoneNumber);
    }
}
