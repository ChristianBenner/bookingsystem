package com.christianbenner.itc.data;

import com.christianbenner.itc.ChangeListener;
import com.christianbenner.itc.Directory;

import java.util.ArrayList;
import java.util.List;

/**
 * UniqueObject is a class used with Directories and the UniqueObjectJsonParser. It is the base class for data types
 * that are required to have unique identifiers and to be saved to JSON save files. Each UniqueObject will be assigned
 * a unique identifier when used in a Directory.
 *
 * @see         com.christianbenner.itc.json.DirectoryObjectJsonParser
 * @see         Directory
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public abstract class DirectoryObject
{
    // Represents an unassigned unique identifier
    private static final int UNASSIGNED_UNIQUE_IDENTIFIER = -1;

    // The ChangeListener objects
    private List<ChangeListener> changeListenerList;

    // Unique identifier assigned when used in a directory
    private int uniqueId;

    /**
     * Add a change listener to the list of change listeners. If data in the object changes, the change listener will
     * be interacted with.
     *
     * @param changeListener    Change listener to add to the object.
     * @since                   1.0
     */
    public final void addChangeListener(final ChangeListener changeListener)
    {
        changeListenerList.add(changeListener);
    }

    /**
     * Remove a change listener from the list of change listeners.
     *
     * @param changeListener    Change listener to remove from the object.
     * @since                   1.0
     */
    public final void removeChangeListener(final ChangeListener changeListener)
    {
        changeListenerList.remove(changeListener);
    }

    /**
     * Set the unique ID of the object. Final so that inheriting classes cannot change functionality. A unique ID can
     * only be set once.
     *
     * @param uniqueId  Unique identifier for the object
     * @return          Returns true if successful, else false
     * @since           1.0
     */
    public final boolean setUniqueId(final int uniqueId)
    {
        if(this.uniqueId != UNASSIGNED_UNIQUE_IDENTIFIER)
        {
            System.err.println("UniqueObject has already been assigned a unique identifier. Cannot be changed");
            return false;
        }

        this.uniqueId = uniqueId;
        notifyChangeListeners();
        return true;
    }

    /**
     * Get the unique ID of the object. Final so that inheriting classes cannot change functionality
     *
     * @return  The objects unique identifier
     * @since   1.0
     */
    public final int getUniqueId()
    {
        return uniqueId;
    }

    /**
     * Compare the object with a search phrase. This is to be implemented by the sub-class
     *
     * @param searchPhrase  SearchPhrase to compare the object against
     * @return              MatchType - Determines the level of match between the object and searchPhrase
     * @since               1.0
     */
    public abstract Directory.MatchType compare(final String searchPhrase);

    /**
     * Construct unique object with the default unique ID of -1 (unassigned)
     *
     * @since   1.0
     */
    protected DirectoryObject()
    {
        this.uniqueId = UNASSIGNED_UNIQUE_IDENTIFIER;
        changeListenerList = new ArrayList<>();
    }

    /**
     * Notify all the change listeners in the change listener list that data in the object has changed.
     *
     * @since 1.0
     */
    protected final void notifyChangeListeners()
    {
        // Loop through the list of change listeners. Call each change listeners 'hasChanged' method to notify the
        // implementer of a change to data in the object
        for(final ChangeListener changeListener : changeListenerList)
        {
            changeListener.hasChanged();
        }
    }

    /**
     * Get a string that describes the DirectoryObject. Contains the unique ID of the object and the number of change
     * listeners it has
     */
    @Override
    public String toString()
    {
        return "UniqueID[" + uniqueId + "], ChangeListeners[" + changeListenerList.size() + "]";
    }
}
