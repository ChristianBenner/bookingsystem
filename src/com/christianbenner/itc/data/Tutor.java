package com.christianbenner.itc.data;

import com.christianbenner.itc.Directories;

import java.util.ArrayList;
import java.util.List;

/**
 * Tutor contains information relevant to ITC tutors in the system.
 *
 * @see         Member
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Tutor extends Member
{
    // Tutors preferred prefix
    private String prefix;

    /**
     * Constructs an tutor object.
     *
     * @param firstName     First name of the ITC member
     * @param lastName      Last name of the ITC member
     * @param address       Address of the ITC member
     * @param phoneNumber   Phone number of the ITC member
     * @see                 Address
     * @since               1.0
     */
    public Tutor(final String prefix,
                 final String firstName,
                 final String lastName,
                 final Address address,
                 final String phoneNumber)
    {
        super(firstName, lastName, address, phoneNumber);
        this.prefix = prefix;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(final String prefix)
    {
        this.prefix = prefix;
        super.notifyChangeListeners();
    }

    /**
     * Get tutor as a string. This will return the tutor full name
     *
     * @since 1.0
     */
    @Override
    public String toString()
    {
        return getPrefix() + " " + super.getLastName();
    }

    public List<ParentMeeting> getParentMeetings()
    {
        // List that will contain all the tutors parent meetings
        final List<ParentMeeting> parentMeetings = new ArrayList<>();

        // Grab all the parent meetings
        final List<ParentMeeting> allParentMeetings = Directories.getParentMeetingDirectory().getAll();

        // Iterate through all parent meetings adding them to the list if the tutor is hosting them
        for(ParentMeeting parentMeeting : allParentMeetings)
        {
            // Check if the tutor ID of the given tutor matches that of the tutor hosting the session
            if(parentMeeting.getTutorId() == getUniqueId())
            {
                parentMeetings.add(parentMeeting);
            }
        }

        return parentMeetings;
    }

    public List<Lesson> getLessons()
    {
        // List that will contain all the tutors lessons
        final List<Lesson> lessons = new ArrayList<>();

        // Grab all the lessons
        final List<Lesson> allLessons = Directories.getLessonDirectory().getAll();

        // Iterate through all lessons adding them to the list if the tutor is hosting them
        for(Lesson lesson : allLessons)
        {
            // Check if the tutor ID of the given tutor matches that of the tutor hosting the session
            if(lesson.getTutorId() == getUniqueId())
            {
                lessons.add(lesson);
            }
        }

        return lessons;
    }
}
