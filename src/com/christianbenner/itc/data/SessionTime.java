package com.christianbenner.itc.data;

import java.util.GregorianCalendar;

/**
 * Stores information on the time a session is with respect to the beginning of the week
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class SessionTime
{
    // If there is no value applied to time metric
    public static final int NO_VALUE = -1;

    // Day start
    public static final int DAY_START_HOURS = 9;

    // Hours in a day
    public static final int HOURS_IN_DAY = 8;

    // ID for the day Monday
    public static final int MONDAY = 0x01;

    // ID for the day Tuesday
    public static final int TUESDAY = 0x02;

    // ID for the day Wednesday
    public static final int WEDNESDAY = 0x03;

    // ID for the day Thursday
    public static final int THURSDAY = 0x04;

    // ID for the day Friday
    public static final int FRIDAY = 0x05;

    // Year of the session
    private int year;

    // Month of the session
    private int month;

    // Day of the session
    private int day;

    // Hours from the beginning of the day (9:00 AM)
    private int hoursFromDayStart;

    // Whether the session repeats weekly or not
    public boolean repeatsWeekly;

    /**
     * Construct a SessionTime object that repeats weekly.
     *
     * @param day               Day of the week that the session is on
     * @param hoursFromDayStart Hours from the beginning of the tuition day (9:00 AM)
     * @since                   1.0
     */
    public SessionTime(final int day, final int hoursFromDayStart)
    {
        this.year = NO_VALUE;
        this.month = NO_VALUE;
        this.day = day;
        this.hoursFromDayStart = hoursFromDayStart;
        this.repeatsWeekly = true;
    }

    /**
     * Construct a SessionTime object that does not repeat weekly.
     *
     * @param year              Year that the session is on
     * @param month             Day of the week that the session is on
     * @param day               Day of the week that the session is on
     * @param hoursFromDayStart Hours from the beginning of the tuition day (9:00 AM)
     * @since                   1.0
     */
    public SessionTime(final int year, final int month, final int day, final int hoursFromDayStart)
    {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hoursFromDayStart = hoursFromDayStart;
        this.repeatsWeekly = false;
    }

    /**
     * Construct a SessionTime object from another. Copies data from the given SessionTime object.
     *
     * @param copy  SessionTime object to copy data from
     * @since       1.0
     */
    public SessionTime(final SessionTime copy)
    {
        this.year = copy.year;
        this.month = copy.month;
        this.day = copy.day;
        this.hoursFromDayStart = copy.hoursFromDayStart;
        this.repeatsWeekly = copy.repeatsWeekly;
    }

    /**
     * Retrieve the day as an ID (convert string to an ID)
     *
     * @param string    String containing day text e.g. 'Monday' or 'monday'
     * @return          ID associated to day or '0' if not a valid day/not recognised
     * @since           1.0
     */
    public static int getDayId(final String string)
    {
        final String lowerCase = string.toLowerCase();

        // Check if the string is 'monday'
        if(lowerCase.equals("monday"))
        {
            return MONDAY;
        }

        // Check if the string is 'tuesday'
        if(lowerCase.equals("tuesday"))
        {
            return TUESDAY;
        }

        // Check if the string is 'wednesday'
        if(lowerCase.equals("wednesday"))
        {
            return WEDNESDAY;
        }

        // Check if the string is 'thursday'
        if(lowerCase.equals("thursday"))
        {
            return THURSDAY;
        }

        // Check if the string is 'friday'
        if(lowerCase.equals("friday"))
        {
            return FRIDAY;
        }

        // Unrecognised, return 0
        return 0;
    }

    /**
     * Retrieve the day as a string (convert ID to string)
     *
     * @param dayId ID of the day
     * @return      Day string associated to given ID
     * @since       1.0
     */
    public static String getDayString(final int dayId)
    {
        // Check the ID and return the associated day as a string
        switch (dayId)
        {
            case MONDAY:
                return "Monday";
            case TUESDAY:
                return "Tuesday";
            case WEDNESDAY:
                return "Wednesday";
            case THURSDAY:
                return "Thursday";
            case FRIDAY:
                return "Friday";
        }

        // Unrecognised ID, return null
        return null;
    }

    /**
     * Retrieve the day as a string (convert ID to string)
     *
     * @param sessionTime    Reference to session time
     * @return              Day string associated to given session time
     * @since               1.0
     */
    public static String getDayString(final SessionTime sessionTime)
    {
        // If the session is repeated weekly then we already know the day
        if(sessionTime.isRepeatedWeekly())
        {
            return getDayString(sessionTime.getDay());
        }

        // Create a calendar so that we can work out what day the meeting is on
        final GregorianCalendar gregorianCalendar = new GregorianCalendar(sessionTime.getYear(),
                sessionTime.getMonth() - 1, sessionTime.getDay(), sessionTime.getHoursFromDayStart(), 0);

        // The day of the week that the meeting is on (in terms of gregorian calendar)
        final int day = gregorianCalendar.get(GregorianCalendar.DAY_OF_WEEK);

        // Convert gregorian calendar day of week to SessionTime day of week and then convert that to a string
        return getDayString((day - GregorianCalendar.MONDAY) + SessionTime.MONDAY);
    }


    /**
     * Get the time as a string
     *
     * @param hoursFromDayStart Hours from the beginning of the day
     * @return                  Time string e.g. '14:00'
     * @since                   1.0
     */
    public static String getTimeString(final int hoursFromDayStart)
    {
        // Calculate time
        int time = DAY_START_HOURS + hoursFromDayStart;

        // Determine if a suffix is necessary on the string
        if(time < 10)
        {
            return "0" + time + ":00";
        }

        return time + ":00";
    }

    /**
     * Get the year that the session occurs on
     *
     * @return  Year that the session occurs on
     * @since   1.0
     */
    public int getYear()
    {
        return year;
    }

    /**
     * Get the month that the session occurs on
     *
     * @return  Month that the session occurs on
     * @since   1.0
     */
    public int getMonth()
    {
        return month;
    }

    /**
     * Get whether or not the session repeats weekly. If it doesn't, an exact date is being used. If it does, only a day
     * of week and hours from start of the day are being used.
     *
     * @return  True if the session repeats weekly, else false.
     * @since   1.0
     */
    public boolean isRepeatedWeekly()
    {
        return repeatsWeekly;
    }

    /**
     * Retrieve the session start as hours from the beginning of the day (9:00 AM)
     *
     * @return  Hours from the beginning of the day to start the session on (9:00 AM)
     * @since   1.0
     */
    public int getHoursFromDayStart()
    {
        return hoursFromDayStart;
    }

    /**
     * Set the time the session starts as hours from the beginning of the day (9:00 AM)
     *
     * @param hoursFromDayStart Hours from the beginning of the day to start the session on (9:00 AM)
     * @since                   1.0
     */
    public void setTime(final int hoursFromDayStart)
    {
        this.hoursFromDayStart = hoursFromDayStart;
    }

    /**
     * Get the day that the session is on
     *
     * @return  The day that the session is on
     * @since   1.0
     */
    public int getDay()
    {
        return day;
    }

    /**
     * Set the day that the session is on
     *
     * @param day   The day to set the session on
     * @since       1.0
     */
    public void setDay(final int day)
    {
        this.day = day;
    }

    /**
     * Get the day that the session is on as a String
     *
     * @return  The day the session is on as a String
     * @since   1.0
     */
    public String getDayString()
    {
        // Switch through the different day values
        switch (day)
        {
            case MONDAY:
                return "Monday";
            case TUESDAY:
                return "Tuesday";
            case WEDNESDAY:
                return "Wednesday";
            case THURSDAY:
                return "Thursday";
            case FRIDAY:
                return "Friday";
        }

        return "Undefined";
    }

    /**
     * Check if the two objects are equal. The objects are determined the same if the raw values are the same. This
     * includes having the same day and hoursFromDayStart.
     *
     * @param obj   The session time to compare to
     * @since       1.0
     */
    @Override
    public boolean equals(Object obj)
    {
        // If the object is a reference to itself, return true
        if(obj == this)
        {
            return true;
        }

        // Check if the object being compared is the same type - If it isn't then it isn't the same object
        if(!(obj instanceof SessionTime))
        {
            return false;
        }

        // Cast the object to an SessionTime type as we checked that it is above
        SessionTime other = (SessionTime)obj;

        // Compare the data
        return day == other.day &&
                hoursFromDayStart == other.hoursFromDayStart;
    }

    /**
     * Get information on the SessionTime as a String
     *
     * @return  Information on the SessionTime as a String
     * @since   1.0
     */
    @Override
    public String toString()
    {
        return "day: " + day + ", hoursFromDayStart: " + hoursFromDayStart;
    }
}
