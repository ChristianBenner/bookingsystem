package com.christianbenner.itc;

import com.christianbenner.itc.data.DirectoryObject;
import com.christianbenner.itc.json.DirectoryObjectJsonParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Directory class is an object designed to handle a collection of DirectoryObject types (object that extend
 * DirectoryObject). The class uses generics to support multiple custom types. Data is read and written to a specified
 * JSON file and parsed with a given DirectoryObjectJsonParser object which must be written for each data type that is
 * used in Directories.
 *
 * @see         com.christianbenner.itc.json.DirectoryObjectJsonParser
 * @see         Directory
 * @see         DirectoryObject
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Directory<E extends DirectoryObject>
{
    // Match type determines the level of match between an object and search phrase
    public enum MatchType
    {
        SEARCH_RESULT_GOOD_MATCH,
        SEARCH_RESULT_OK_MATCH,
        SEARCH_RESULT_BASIC_MATCH,
        SEARCH_RESULT_NO_MATCH
    }

    // Characters that should be removed from search phrases/strings
    private static final Character[] NON_SEARCH_CHARS =     { ' ', '\'', '\"', '-', '\\', '.', '/'};

    // Score of a good match type
    private static final int GOOD_MATCH_TYPE_SCORE = 3;

    // Score of an ok match type
    private static final int OK_MATCH_TYPE_SCORE = 2;

    // Score of a basic match type
    private static final int BASIC_MATCH_TYPE_SCORE = 1;


    // JSON save file
    private File file;

    // JSON parser unique to the object type
    private DirectoryObjectJsonParser<E> jsonParser;

    // List of unique objects (of generic type)
    private List<E> uniqueObjectList;

    // Latest unique identifier stored in the JSON save
    private int uniqueId;

    /**
     * Construct a directory object. This will read JSON data from the given file using the given JSON parser if it
     * exists.
     *
     * @param file          JSON save file location
     * @param jsonParser    JSONParser unique to parsing objects of the same type
     * @see                 DirectoryObjectJsonParser
     * @since               1.0
     */
    public Directory(final File file, final DirectoryObjectJsonParser<E> jsonParser)
    {
        this.file = file;
        this.jsonParser = jsonParser;

        uniqueObjectList = new ArrayList<>();

        // Read the JSON file and parse it
        uniqueId = jsonParser.readJson(SaveUtils.readJSONFile(file), uniqueObjectList);

        // Add change listeners to all the existing objects
        for(E object : uniqueObjectList)
        {
            // Add a change listener to the object. This means that the Directory will be notified upon whenever the
            // data in the object is altered. If the data is changed, update the save (so that the JSON file contains
            // the new data)
            object.addChangeListener(() ->
            {
                System.out.println("Object in directory changed, updating save file '" + file.getName() + "'");
                updateSave();
            });
        }
    }

    /**
     * Check if the directory contains no objects
     *
     * @return  True if the directory contains no objects, else false
     * @since   1.0
     */
    public boolean isEmpty()
    {
        return uniqueObjectList.size() == 0;
    }

    /**
     * Updates the JSON save file with the latest unique ID and state of the object list
     *
     * @since   1.0
     */
    public void updateSave()
    {
        SaveUtils.writeJSONFile(file, jsonParser.writeJson(uniqueId, uniqueObjectList));
    }

    /**
     * Add an object to the directory. This operation causes an update to the JSON save file.
     *
     * @param object    Name of the object
     * @since           1.0
     */
    public void add(final E object)
    {
        // Assign a unique ID to the object
        object.setUniqueId(uniqueId++);

        // Add object to the array
        uniqueObjectList.add(object);

        // Add a change listener to the object. This means that the Directory will be notified upon whenever the data in
        // the object is altered. If the data is changed, update the save (so that the JSON file contains the new data)
        object.addChangeListener(() ->
        {
            System.out.println("Object in directory changed, updating save file '" + file.getName() + "'");
            updateSave();
        });

        // Update the save file because the list has changed
        updateSave();
    }

    /**
     * Retrieves an object from the directory by its unique identifier
     *
     * @param id    Unique identifier of the object
     * @return      Reference to the object
     * @see         DirectoryObject
     * @since       1.0
     */
    public E get(final int id)
    {
        // Search the object array to see if one matches the specified ID
        for(E object : uniqueObjectList)
        {
            // Compare the IDs
            if(object.getUniqueId() == id)
            {
                // Found object with matching ID, return it
                return object;
            }
        }

        return null;
    }

    /**
     * Retrieves a list of objects from the directory by their unique identifiers
     *
     * @param idList    List of unique identifiers
     * @return          List of objects containing the objects associated to each unique identifier in the given list
     * @see             DirectoryObject
     * @since           1.0
     */
    public List<E> getList(final List<Integer> idList)
    {
        // List that will contain all of the fetched objects
        final List<E> objectList = new ArrayList<>();

        // Iterate through the list of given identifiers, adding each associated object to the object list
        for(Integer id : idList)
        {
            // Fetch the object from its ID
            final E object = get(id);

            // Check if the directory contains the ID before attempting to add it
            if(object != null)
            {
                objectList.add(object);
            }
        }

        return objectList;
    }

    /**
     * Removes an object from the directory. This operation causes an update to the JSON save file.
     *
     * @param id    Objects unique identifier
     * @return      Success state - True if object was removed successfully, else false
     * @since       1.0
     */
    public boolean remove(final int id)
    {
        // Find and remove the object from the array
        for(E object : uniqueObjectList)
        {
            // Check if the ID matches
            if(object.getUniqueId() == id)
            {
                // Remove object from array
                uniqueObjectList.remove(object);

                // Object list has changed so update the save file
                updateSave();

                return true;
            }
        }

        return false;
    }

    /**
     * Removes an object from the directory. This operation causes an update to the JSON save file.
     *
     * @param object    Reference to the object
     * @return          Success state - True if object was removed successfully, else false
     * @since           1.0
     */
    public boolean remove(final E object)
    {
        // Check if the array contains the object
        if(uniqueObjectList.contains(object))
        {
            // Remove the object by its object reference
            uniqueObjectList.remove(object);

            // Object list has changed so update the save file
            updateSave();

            return true;
        }

        return false;
    }

    /**
     * Check if the directory contains a DirectoryObject with the specified ID
     *
     * @param id    Unique Identifier of the object
     * @return      True if the directory contains the object with that ID, else false.
     * @since       1.0
     */
    public boolean contains(final int id)
    {
        // Iterate through the list of unique objects comparing each ID
        for(int i = 0; i < uniqueObjectList.size(); i++)
        {
            // Compare objects ID with given ID
            if(uniqueObjectList.get(i).getUniqueId() == id)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the directory contains a DirectoryObject
     *
     * @param object    Reference to object
     * @return          True if the directory contains the object with that ID, else false.
     * @since           1.0
     */
    public boolean contains(final E object)
    {
        return uniqueObjectList.contains(object);
    }

    /**
     * Get the number of objects in the directory
     *
     * @return  Number of objects in the directory
     * @since   1.0
     */
    public int getSize()
    {
        return uniqueObjectList.size();
    }

    /**
     * Retrieve all of the objects in the directory. This is will return copies of the array so that the directory can
     * manage the list (in terms of saving it when it is altered e.g. when something is added or removed).
     *
     * @return  List of all the objects in the directory
     * @since   1.0
     */
    public List<E> getAll()
    {
        return new ArrayList(uniqueObjectList);
    }

    /**
     * Remove all references to the objects in the directory. This causes a save file update and will result in an empty
     * save file. This will not reset the unique identifier value.
     *
     * @since   1.0
     */
    public void clear()
    {
        // Clear the list of object
        uniqueObjectList.clear();

        // Update save file
        updateSave();
    }

    /**
     * Searches the directory by a phrase (string). The phrase is compared against the object by the
     * <code>compareObjectWithPhrase</code> method implemented by the subclass. The phrase and names will be converted
     * to a search phrase (lower case and excluding certain characters) to insure that flexible search results are
     * returned.
     *
     * @param phrase    Phrase to search the directory for
     * @return          List of objects. The list is returned in an order of better match first. Better matches are
     *                  determined by how much they relate to the search phrase.
     * @since           1.0
     */
    public List<E> search(final String phrase)
    {
        // Array containing the best matches
        final List<E> bestMatches = new ArrayList();
        final List<E> okMatches = new ArrayList<>();
        final List<E> basicMatches = new ArrayList<>();

        // Convert search phrase to have characters that shouldn't be considered in the search
        final String searchPhrase = convertToSearchPhrase(phrase);

        // Iterate through the object array determining there match with the search phrase
        for(E object : uniqueObjectList)
        {
            switch (object.compare(searchPhrase))
            {
                case SEARCH_RESULT_GOOD_MATCH:
                    bestMatches.add(object);
                    break;
                case SEARCH_RESULT_OK_MATCH:
                    okMatches.add(object);
                    break;
                case SEARCH_RESULT_BASIC_MATCH:
                    basicMatches.add(object);
                    break;
            }
        }

        // Create the ordered array (with best matches first)
        final List<E> searchResults = new ArrayList<>();
        searchResults.addAll(bestMatches);
        searchResults.addAll(okMatches);
        searchResults.addAll(basicMatches);

        return searchResults;
    }

    /**
     * Converts a string to a search friendly string. This means that it will be converted to lower case and all
     * characters deemed 'non-search characters' will be removed.
     *
     * @param text  Text to convert to a search phrase
     * @return      Text string converted to search phrase (excluding the NON_SEARCH_CHARS)
     * @see         #NON_SEARCH_CHARS
     * @since       1.0
     */
    public static String convertToSearchPhrase(final String text)
    {
        // Convert text to lower case
        String searchPhrase = text.toLowerCase();

        // Remove any 'non-search characters' from the string
        for(Character character : NON_SEARCH_CHARS)
        {
            searchPhrase = searchPhrase.replace(character + "", "");
        }

        return searchPhrase;
    }

    /**
     * Get a score for the given match type. Each match type is worth:
     *
     * SEARCH_RESULT_GOOD_MATCH: 3 score
     * SEARCH_RESULT_OK_MATCH: 2 score
     * SEARCH_RESULT_BASIC_MATCH: 1 score
     * SEARCH_RESULT_NO_MATCH: 0 score
     *
     * @param matchType Match type to get score of
     * @return          Score of the given match type
     * @since           1.0
     */
    public static int getSearchScore(final Directory.MatchType matchType)
    {
        // Switch through the match type to give each a score
        switch (matchType)
        {
            case SEARCH_RESULT_GOOD_MATCH:
                return GOOD_MATCH_TYPE_SCORE;
            case SEARCH_RESULT_OK_MATCH:
                return OK_MATCH_TYPE_SCORE;
            case SEARCH_RESULT_BASIC_MATCH:
                return BASIC_MATCH_TYPE_SCORE;
        }

        return 0;
    }
}
