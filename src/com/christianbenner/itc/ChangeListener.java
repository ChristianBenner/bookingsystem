package com.christianbenner.itc;

import java.util.EventListener;

/**
 * ChangeListener is an interface designed for UniqueObject classes to have any data changes listened to by other
 * objects. For example, the directory class created a ChangeListener in a UniqueObject type in order to know when
 * data in that object has changed so that it can re-write the save file it has for it.
 *
 * @see         EventListener
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public interface ChangeListener extends EventListener
{
    /**
     * Method that gets called when the object with the change listener has been changed. A change could be the
     * addition, removal or alteration of data in that object. It is up to the object that composes of change listeners
     * what it deems as a change. This method is to be implemented by the sub-class.
     *
     * @since 1.0
     */
    void hasChanged();
}