package com.christianbenner.itc;

import com.christianbenner.itc.gui.GUIConstants;
import com.christianbenner.itc.gui.roleselect.RoleSelectPopup;

import javax.swing.*;

public class Core
{
    public static void main(String[] args)
    {
/*        Directory<ParentMeeting> parentMeetingDirectory = Directories.getParentMeetingDirectory();

        ParentMeeting parentMeeting = new ParentMeeting(1, 2, new MeetingTime(2020, 5,
                11, 13));

        parentMeetingDirectory.add(parentMeeting);

        ParentMeeting parentMeeting2 = parentMeetingDirectory.get(parentMeeting.getUniqueId());

        if(parentMeeting == parentMeeting2)
        {
            System.out.println("EQUAL");
        }
        else
        {
            System.out.println("NOT EQUAL");
        }

        System.out.println(parentMeeting2);*/

/*        Directory<Tutor> tutorDirectory = Directories.getTutorDirectory();
        tutorDirectory.add(new Tutor("Chris", "Benner", new Address("AB1 2CD",
                "1 Test Road", "Test Town", "Test City"),
                "555-55-5"));

        List<Tutor> tutors = tutorDirectory.search("Benner");
        for(Tutor tutor : tutors)
        {
            System.out.println(tutor);
        }
        tutors.get(0).setFirstName("Christian");


        Directory<Student> studentDirectory = Directories.getStudentDirectory();
        studentDirectory.add(new Student("Student", "Person", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1"));
        studentDirectory.add(new Student("Student2", "Person2", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1"));
        studentDirectory.add(new Student("Student3", "Person3", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1"));
        studentDirectory.add(new Student("Student4", "Person4", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1"));
        studentDirectory.add(new Student("Student5", "Person5", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1"));
        studentDirectory.add(new Student("Student6", "Person6", new Address("AB3 6CD",
                "63 Test Road", "Test Town", "Test City"),
                "556-33-1"));

        List<Student> students = studentDirectory.search("stu");
        for(Student student : students)
        {
            System.out.println(student);
        }

        Directory<Subject> subjectDirectory = Directories.getSubjectDirectory();
        subjectDirectory.add(new Subject("History"));
        subjectDirectory.add(new Subject("Mathematics"));
        subjectDirectory.add(new Subject("English"));
        subjectDirectory.add(new Subject("Physics"));

        List<Subject> subjects = subjectDirectory.search("ic");
        for(Subject subject : subjects)
        {
            System.out.println(subject);
        }

        Directory<Room> roomDirectory = Directories.getRoomDirectory();
        roomDirectory.add(new Room("HallA", 5));
        roomDirectory.add(new Room("Cafe", 2));

        List<Room> rooms = roomDirectory.search("2");
        for(Room room : rooms)
        {
            System.out.println(room);
        }


        Lesson l = new Lesson(tutors.get(0).getUniqueId(), rooms.get(0).getUniqueId(),
                subjects.get(0).getUniqueId(), new LessonTime(LessonTime.MONDAY, 1), 5);
        Directory<Lesson> lessonDirectory = Directories.getLessonDirectory();
        lessonDirectory.add(l);
        List<Student> list = studentDirectory.getAll();

        for(Student student : list)
        {
            l.addStudent(student.getUniqueId());
        }*/
       /* final Font oswaldRegularFont = GUIConstants.loadFont("/resources/fonts/oswald_regular.ttf");
        JFrame frame = new JFrame("Test");
        frame.add(new AddRoomPanel(oswaldRegularFont));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);*/

        UIManager userInterface =new UIManager();
        userInterface.put("OptionPane.background", GUIConstants.BACKGROUND_COLOUR);
        userInterface.put("Panel.background", GUIConstants.BACKGROUND_COLOUR);
        userInterface.put("Button.background", GUIConstants.PANEL_COLOUR);

        SwingUtilities.invokeLater(() ->
        {
            RoleSelectPopup roleSelectPopup = new RoleSelectPopup();
            roleSelectPopup.setVisible(true);
        });
    }
}
