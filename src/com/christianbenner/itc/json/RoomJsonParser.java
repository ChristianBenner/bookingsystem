package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Room;
import org.json.JSONObject;

/**
 * RoomJsonParser reads and writes JSON objects based on room objects.
 * Example of the format of the save file:
 *   {
 *   "rooms": [
 *     {
 *       "buildingName": "BuildingName",
 *       "roomNumber": 5
 *       "id": 6
 *     },
 *     ],
 *     "uniqueId": 7
 *    }
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class RoomJsonParser extends DirectoryObjectJsonParser<Room>
{
    // Room name tag for the JSON data file
    private static final String BUILDING_NAME_TAG = "buildingName";

    // Room number tag for the JSON data file
    private static final String ROOM_NUMBER_TAG = "roomNumber";

    // Room array tag for the JSON data file
    private static final String ROOM_ARRAY_TAG = "rooms";

    /**
     * Construct RoomJsonParser. Calls sub-class constructor with a name for the array containing the Rooms.
     *
     * @since 1.0
     */
    public RoomJsonParser()
    {
        super(ROOM_ARRAY_TAG);
    }

    /**
     * Read a Room from a JSONObject
     *
     * @param roomJson  JSONObject containing room data
     * @return          Room object populated from data found in the given JSONObject
     * @see             JSONObject
     * @see             Room
     * @since           1.0
     */
    protected final Room readObject(final JSONObject roomJson)
    {
        return new Room(roomJson.getString(BUILDING_NAME_TAG), roomJson.getInt(ROOM_NUMBER_TAG));
    }

    /**
     * Write a Room object to a JSONObject. This is in the format (order dictated by JSONObject):
     *
     * {
     *      "buildingName": "BuildingName",
     *      "roomNumber": number
     *      "id": id
     * }
     *
     * @param room  Room object to write to a JSONObject
     * @return      JSONObject containing the Room object data
     * @see         JSONObject
     * @see         Room
     * @since       1.0
     */
    protected final JSONObject writeObject(final Room room)
    {
        // Create and populate the room JSON object with the room
        JSONObject roomObject = new JSONObject();
        roomObject.put(BUILDING_NAME_TAG, room.getBuildingName());
        roomObject.put(ROOM_NUMBER_TAG, room.getRoomNumber());

        return roomObject;
    }
}
