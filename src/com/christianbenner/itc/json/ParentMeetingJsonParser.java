package com.christianbenner.itc.json;

import com.christianbenner.itc.data.ParentMeeting;
import com.christianbenner.itc.data.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

/**
 * ParentMeetingJsonParser reads and writes JSON objects based on parentMeeting objects.
 * Example of the format of the save file:
 * {
 *  "uniqueId":10,
 *  "parentMeetings":[
 *  {
 *      "tutorId":9,
 *      "studentId":2,
 *      "parentMeetingTime":{
 *          "year":2020,
 *          "month":5,
 *          "day":12,
 *          "hoursFromDayStart":2,
 *      },
 *      "id":9,
 *      "roomId":19
 *  }]
 * }
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class ParentMeetingJsonParser extends DirectoryObjectJsonParser<ParentMeeting>
{
    // ParentMeeting array tag for the JSON data file
    private static final String PARENT_MEETINGS_ARRAY_TAG = "parentMeetings";

    // Tutor ID tag for the JSON data file
    private static final String TUTOR_ID_TAG = "tutorId";

    // Room ID tag for the JSON data file
    private static final String ROOM_ID_TAG = "roomId";

    // Session Time tag for the JSON data file
    private static final String SESSION_TIME_TAG = "sessionTime";

    private static final String SLOT_NUMBER_TAG = "slot";

    private static final String STUDENT_ID = "studentId";

    /**
     * Construct ParentMeetingJsonParser. Calls sub-class constructor with a name for the array containing the
     * ParentMeetings.
     *
     * @since 1.0
     */
    public ParentMeetingJsonParser()
    {
        super(PARENT_MEETINGS_ARRAY_TAG);
    }

    /**
     * Read a ParentMeeting from a JSONObject
     *
     *
     * @param parentMeetingJson  JSONObject containing parentMeeting data
     * @return          ParentMeeting object populated from data found in the given JSONObject
     * @see             JSONObject
     * @see             Session
     * @since           1.0
     */
    @Override
    protected ParentMeeting readObject(final JSONObject parentMeetingJson)
    {
        final ParentMeeting session = new ParentMeeting(parentMeetingJson.getInt(TUTOR_ID_TAG),
                parentMeetingJson.getInt(ROOM_ID_TAG),
                SessionTimeJsonParser.readSessionTime(parentMeetingJson.getJSONObject(SESSION_TIME_TAG)),
                parentMeetingJson.getInt(SLOT_NUMBER_TAG));

        session.book(parentMeetingJson.getInt(STUDENT_ID));

        return session;
    }

    /**
     * Write a ParentMeeting object to a JSONObject. This is in the format (order dictated by JSONObject):
     *  {
     *      "tutorId":9,
     *      "studentIdArray":[9],
     *      "parentMeetingTime":{
     *          "minutes":660,
     *          "day":1
     *      },
     *      "maxSpaces":5,
     *      "id":9,
     *      "roomId":19,
     *      "subjectId":37
     *  }
     *
     * @param parentMeeting ParentMeeting object to write to a JSONObject
     * @return              JSONObject containing the ParentMeeting object data
     * @see                 JSONObject
     * @see                 ParentMeeting
     * @since               1.0
     */
    @Override
    protected JSONObject writeObject(final ParentMeeting parentMeeting)
    {
        final JSONObject jsonObject = new JSONObject();

        // Write the ID of the tutor to the object
        jsonObject.put(TUTOR_ID_TAG, parentMeeting.getTutorId());

        // Write the ID of the room to the object
        jsonObject.put(ROOM_ID_TAG, parentMeeting.getRoomId());

        // Write the parentMeeting time to the object
        jsonObject.put(SESSION_TIME_TAG, SessionTimeJsonParser.writeSessionTime(parentMeeting.getSessionTime()));

        jsonObject.put(SLOT_NUMBER_TAG, parentMeeting.getSlot());

        jsonObject.put(STUDENT_ID, parentMeeting.getStudentId());

        return jsonObject;
    }
}
