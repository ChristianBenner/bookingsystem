package com.christianbenner.itc.json;

import com.christianbenner.itc.data.*;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;

/**
 * An interface for parsers of JSON data relevant to objects that extend DirectoryObject.
 *
 * @see         DirectoryObject
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public abstract class DirectoryObjectJsonParser<E extends DirectoryObject>
{
    // Unique ID tag for the JSON data file
    private static final String UNIQUE_ID_TAG = "uniqueId";

    // Lesson ID tag for the JSON data file
    private static final String ID_TAG = "id";

    // Tag for the array of DirectoryObjects in the JSON data file
    private final String ARRAY_TAG;

    /**
     * Construct DirectoryObjectJsonParser (must be called from sub-class). Give an array name so that parser knows what to
     * call the array containing the objects. This is so that if the wrong JSON is read it will give an error associated
     * to the wrong ID tag.
     *
     * @param arrayTagName  Name of the array containing the array of DirectoryObjects
     * @since               1.0
     */
    protected DirectoryObjectJsonParser(final String arrayTagName)
    {
        ARRAY_TAG = arrayTagName;
    }

    /**
     * Read a DirectoryObject type from a JSONObject
     *
     * @param object    JSONObject containing object data of the generic type E
     * @return          Unique object populated from data found in the given JSONObject
     * @see             JSONObject
     * @since           1.0
     */
    protected abstract E readObject(final JSONObject object);

    /**
     * Write a DirectoryObject type to a JSONObject.
     *
     * @param object    DirectoryObject type to write to a JSONObject
     * @return          JSONObject containing the DirectoryObject type data
     * @see             JSONObject
     * @since           1.0
     */
    protected abstract JSONObject writeObject(final E object);

    /**
     * Read a JSONObject to an array of objects that extend DirectoryObject types
     *
     * @param jsonObject    JSONObject containing the array of objects
     * @return              The latest available unique ID (all IDs greater than this value are available also)
     * @see                 JSONObject
     * @see                 DirectoryObject
     * @since               1.0
     */
    public final int readJson(final JSONObject jsonObject, final List<E> objectOut)
    {
        // Check that the JSON object is not null before trying to process it
        if(jsonObject != null)
        {
            // Retrieve the object array from the JSON object
            final JSONArray objectArray = jsonObject.getJSONArray(ARRAY_TAG);

            // Read each object from the array
            for(int i = 0; i < objectArray.length(); i++)
            {
                // Get the parser sub-class to read the data to the JSON object
                final E object = readObject(objectArray.getJSONObject(i));

                // Read the unique ID from the JSON and add to the object
                object.setUniqueId(objectArray.getJSONObject(i).getInt(ID_TAG));

                // Add object to list out
                objectOut.add(object);
            }

            // Retrieve the unique ID from the JSON object
            return jsonObject.getInt(UNIQUE_ID_TAG);
        }

        return 0;
    }

    /**
     * Write an array of objects that extend DirectoryObject to a JSONObject
     *
     * @param uniqueId  Latest unique ID to write to the JSONObject
     * @param obj       List of objects to write to the JSONObject
     * @return          JSONObject containing the data from the array of unique objects and the latest unique identifier
     * @see             JSONObject
     * @see             DirectoryObject
     * @since           1.0
     */
    public final JSONObject writeJson(final int uniqueId, final List<E> obj)
    {
        // Main JSON object to store the object array
        final JSONObject jsonObject = new JSONObject();

        // Create the object JSON array
        final JSONArray objectArray = new JSONArray();

        // Write the objects to the object JSON array
        for(E object : obj)
        {
            // Get the parser sub-class to write the data to the JSON object
            final JSONObject arrayObject = writeObject(object);

            // Write the objects ID
            arrayObject.put(ID_TAG, object.getUniqueId());

            // Put object in array
            objectArray.put(arrayObject);
        }

        // Add the object array to the JSON object
        jsonObject.put(ARRAY_TAG, objectArray);

        // Add the unique ID to the JSON array
        jsonObject.put(UNIQUE_ID_TAG, uniqueId);

        return jsonObject;
    }
}
