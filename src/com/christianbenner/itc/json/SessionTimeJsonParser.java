package com.christianbenner.itc.json;

import com.christianbenner.itc.data.SessionTime;
import org.json.JSONObject;

public class SessionTimeJsonParser
{
    // Session year tag for the JSON data file
    private static final String SESSION_TIME_YEAR_TAG = "year";

    // Session month tag for the JSON data file
    private static final String SESSION_TIME_MONTH_TAG = "month";

    // Session day tag for the JSON data file
    private static final String SESSION_TIME_DAY_TAG = "day";

    // If the session repeats weekly or not
    private static final String SESSION_REPEATS_WEEKLY_TAG = "repeatedWeekly";

    // Session minutes tag for the JSON data file
    private static final String SESSION_TIME_HOURS_FROM_DAY_START_TAG = "hoursFromDayStart";

    public static JSONObject writeSessionTime(final SessionTime sessionTime)
    {
        // Write the session time to a separate object
        final JSONObject sessionTimeJson = new JSONObject();

        // Only write year and month if the session is repeated weekly
        if(!sessionTime.isRepeatedWeekly())
        {
            sessionTimeJson.put(SESSION_TIME_YEAR_TAG, sessionTime.getYear());
            sessionTimeJson.put(SESSION_TIME_MONTH_TAG, sessionTime.getMonth());
        }

        sessionTimeJson.put(SESSION_TIME_DAY_TAG, sessionTime.getDay());
        sessionTimeJson.put(SESSION_TIME_HOURS_FROM_DAY_START_TAG, sessionTime.getHoursFromDayStart());
        sessionTimeJson.put(SESSION_REPEATS_WEEKLY_TAG, sessionTime.isRepeatedWeekly());

        return sessionTimeJson;
    }

    /**
     * Read a SessionTime object from a JSONObject.
     *
     * @param jsonObject    JSONObject to read
     * @return              SessionTime object read from JSONObject
     * @see                 JSONObject
     * @see                 SessionTime
     * @since               1.0
     */
    public static SessionTime readSessionTime(final JSONObject jsonObject)
    {
        // Get repeats weekly tag
        boolean repeatsWeekly = jsonObject.getBoolean(SESSION_REPEATS_WEEKLY_TAG);

        // If the session repeats weekly, create a session with the rest of the data
        if(!repeatsWeekly)
        {
            return new SessionTime(jsonObject.getInt(SESSION_TIME_YEAR_TAG), jsonObject.getInt(SESSION_TIME_MONTH_TAG),
                    jsonObject.getInt(SESSION_TIME_DAY_TAG), jsonObject.getInt(SESSION_TIME_HOURS_FROM_DAY_START_TAG));
        }

        // If the session does not repeat weekly, create a session with just the day and hours start tag
        return new SessionTime(jsonObject.getInt(SESSION_TIME_DAY_TAG),
                jsonObject.getInt(SESSION_TIME_HOURS_FROM_DAY_START_TAG));
    }
}
