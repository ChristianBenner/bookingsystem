package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Address;
import com.christianbenner.itc.data.Tutor;
import org.json.JSONObject;

/**
 * TutorJsonParser reads and writes JSON objects based on tutor objects.
 * Example of the format of the save file:
 *   {
 *   "tutors": [
 *     {
 *       "firstName": "TChris",
 *       "lastName": "TBenner",
 *       "phoneNumber": "444-55-5",
 *       "address": {
 *         "lineTwo": "TAdd2",
 *         "lineOne": "TAdd1",
 *         "postCode": "TAA BBB",
 *         "lineThree": "TAdd3"
 *       },
 *       "id": 6
 *     },
 *     ],
 *     "uniqueId": 20
 *    }
 *
 * @see         Tutor
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class TutorJsonParser extends DirectoryObjectJsonParser<Tutor>
{
    // Tutor prefix tag for the JSON data file
    private static final String PREFIX_TAG = "prefix";

    // Tutor first name tag for the JSON data file
    private static final String FIRST_NAME_TAG = "firstName";

    // Tutor last name tag for the JSON data file
    private static final String LAST_NAME_TAG = "lastName";

    // Tutor phone number tag for the JSON data file
    private static final String PHONE_NUMBER_TAG = "phoneNumber";

    // Tutor array tag for the JSON data file
    private static final String TUTOR_ARRAY_TAG = "tutors";

    // Tutor address tag for the JSON data file
    private static final String ADDRESS_TAG = "address";

    // Address post code tag for the JSON data file
    private static final String ADDRESS_POST_CODE_TAG = "postCode";

    // Address line one tag for the JSON data file
    private static final String ADDRESS_LINE_ONE_TAG = "lineOne";

    // Address line two tag for the JSON data file
    private static final String ADDRESS_LINE_TWO_TAG = "lineTwo";

    // Address line three tag for the JSON data file
    private static final String ADDRESS_LINE_THREE_TAG = "lineThree";

    /**
     * Construct TutorJsonParser. Calls sub-class constructor with a name for the array containing the Tutors.
     *
     * @since 1.0
     */
    public TutorJsonParser()
    {
        super(TUTOR_ARRAY_TAG);
    }

    /**
     * Read a Tutor from a JSONObject
     *
     * @param memberJson    JSONObject containing member data
     * @return              Tutor object populated from data found in the given JSONObject
     * @see                 JSONObject
     * @see                 Tutor
     * @since           1.0
     */
    protected final Tutor readObject(final JSONObject memberJson)
    {
        return new Tutor(memberJson.getString(PREFIX_TAG),
                memberJson.getString(FIRST_NAME_TAG),
                memberJson.getString(LAST_NAME_TAG),
                readAddress(memberJson.getJSONObject(ADDRESS_TAG)),
                memberJson.getString(PHONE_NUMBER_TAG));
    }

    /**
     * Write a Tutor object to a JSONObject. This is in the format (order dictated by JSONObject):
     *
     * {
     *      "firstName": "FirstName",
     *      "lastName": "LastName",
     *      "phoneNumber": "PhoneNumber",
     *      "address": adressFormat,
     *      "id": id
     * }
     *
     * @param member    Tutor object to write to a JSONObject
     * @return          JSONObject containing the Tutor object data
     * @see             JSONObject
     * @see             Tutor
     * @since           1.0
     */
    protected final JSONObject writeObject(final Tutor member)
    {
        // Create and populate the member JSON object with the member data
        JSONObject memberObject = new JSONObject();
        memberObject.put(PREFIX_TAG, member.getPrefix());
        memberObject.put(FIRST_NAME_TAG, member.getFirstName());
        memberObject.put(LAST_NAME_TAG, member.getLastName());
        memberObject.put(PHONE_NUMBER_TAG, member.getPhoneNumber());
        memberObject.put(ADDRESS_TAG, writeAddress(member.getAddress()));

        return memberObject;
    }

    /**
     * Read an Address from a JSONObject
     *
     * @param address   JSONObject containing address data
     * @return          Address object populated from data found in the given JSONObject
     * @see             JSONObject
     * @see             Address
     * @since           1.0
     */
    private Address readAddress(final JSONObject address)
    {
        // Create and populate member address object with the given JSONObject
        final Address memberAddress = new Address(address.getString(ADDRESS_POST_CODE_TAG), address.getString(
                ADDRESS_LINE_ONE_TAG), address.getString(ADDRESS_LINE_TWO_TAG), address.getString(
                ADDRESS_LINE_THREE_TAG));
        return memberAddress;
    }

    /**
     * Write an Address object to a JSONObject. This is in the format (order dictated by JSONObject):
     *
     * "address": {
     *      "lineTwo": "address2",
     *      "lineOne": "address1",
     *      "postCode": "postCode",
     *      "lineThree": "address3"
     * }
     *
     * @param address   Address object to write to a JSONObject
     * @return          JSONObject containing the Address object data
     * @see             JSONObject
     * @see             Address
     * @since           1.0
     */
    private JSONObject writeAddress(final Address address)
    {
        // Create and populate the address JSON object with the address object
        JSONObject addressObject = new JSONObject();
        addressObject.put(ADDRESS_POST_CODE_TAG, address.getPostCode());
        addressObject.put(ADDRESS_LINE_ONE_TAG, address.getAddressLineOne());
        addressObject.put(ADDRESS_LINE_TWO_TAG, address.getAddressLineTwo());
        addressObject.put(ADDRESS_LINE_THREE_TAG, address.getAddressLineThree());

        return addressObject;
    }
}
