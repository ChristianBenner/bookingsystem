package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Subject;
import org.json.JSONObject;

/**
 * SubjectJsonParser reads and writes JSON objects based on subject objects.
 * Example of the format of the save file:
 *   {
 *   "subjects": [
 *     {
 *       "name": "SubjectName",
 *       "id": 6
 *     },
 *     ],
 *     "uniqueId": 7
 *    }
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class SubjectJsonParser extends DirectoryObjectJsonParser<Subject>
{
    // Subject name tag for the JSON data file
    private static final String NAME_TAG = "name";

    // Subject array tag for the JSON data file
    private static final String SUBJECT_ARRAY_TAG = "subjects";

    /**
     * Construct SubjectJsonParser. Calls sub-class constructor with a name for the array containing the Subjects.
     *
     * @since 1.0
     */
    public SubjectJsonParser()
    {
        super(SUBJECT_ARRAY_TAG);
    }

    /**
     * Read a Subject from a JSONObject
     *
     * @param subjectJson   JSONObject containing subject data
     * @return              Subject object populated from data found in the given JSONObject
     * @see                 JSONObject
     * @see                 Subject
     * @since               1.0
     */
    protected final Subject readObject(final JSONObject subjectJson)
    {
        return new Subject(subjectJson.getString(NAME_TAG));
    }

    /**
     * Write a Subject object to a JSONObject. This is in the format (order dictated by JSONObject):
     *
     * {
     *      "name": "SubjectName",
     *      "id": id
     * }
     *
     * @param subject   Subject object to write to a JSONObject
     * @return          JSONObject containing the Subject object data
     * @see             JSONObject
     * @see             Subject
     * @since           1.0
     */
    protected final JSONObject writeObject(final Subject subject)
    {
        // Create and populate the subject JSON object with the subject
        JSONObject subjectObject = new JSONObject();
        subjectObject.put(NAME_TAG, subject.getName());

        return subjectObject;
    }
}
