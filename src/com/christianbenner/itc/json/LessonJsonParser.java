package com.christianbenner.itc.json;

import com.christianbenner.itc.data.Lesson;
import com.christianbenner.itc.data.Session;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * LessonJsonParser reads and writes JSON objects based on lesson objects.
 * Example of the format of the save file:
 * {
 *  "uniqueId":10,
 *  "lessons":[
 *  {
 *      "tutorId":9,
 *      "studentIdArray":[9],
 *      "lessonTime":{
 *          "minutes":660,
 *          "day":1
 *      },
 *      "maxSpaces":5,
 *      "id":9,
 *      "roomId":19,
 *      "subjectId":37
 *  }]
 * }
 *
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class LessonJsonParser extends DirectoryObjectJsonParser<Lesson>
{
    // Lesson array tag for the JSON data file
    private static final String LESSONS_ARRAY_TAG = "lessons";

    // Tutor ID tag for the JSON data file
    private static final String TUTOR_ID_TAG = "tutorId";

    // Room ID tag for the JSON data file
    private static final String ROOM_ID_TAG = "roomId";

    // Subject ID tag for the JSON data file
    private static final String SUBJECT_ID_TAG = "subjectId";

    // Lesson Time tag for the JSON data file
    private static final String LESSON_TIME_TAG = "lessonTime";

    // Max spaces tag for the JSON data file
    private static final String MAX_SPACES_TAG = "maxSpaces";

    // Student ID array tag for the JSON data file
    private static final String STUDENT_ID_ARRAY_TAG = "studentIdArray";

    /**
     * Construct LessonJsonParser. Calls sub-class constructor with a name for the array containing the Lessons.
     *
     * @since 1.0
     */
    public LessonJsonParser()
    {
        super(LESSONS_ARRAY_TAG);
    }

    /**
     * Read a Lesson from a JSONObject
     *
     *
     * @param lessonJson  JSONObject containing lesson data
     * @return          Lesson object populated from data found in the given JSONObject
     * @see             JSONObject
     * @see             Session
     * @since           1.0
     */
    @Override
    protected Lesson readObject(final JSONObject lessonJson)
    {
        Lesson session = new Lesson(lessonJson.getInt(TUTOR_ID_TAG), lessonJson.getInt(ROOM_ID_TAG),
                lessonJson.getInt(SUBJECT_ID_TAG),
                SessionTimeJsonParser.readSessionTime(lessonJson.getJSONObject(LESSON_TIME_TAG)),
                lessonJson.getInt(MAX_SPACES_TAG));

        JSONArray jsonArray = lessonJson.getJSONArray(STUDENT_ID_ARRAY_TAG);

        // Add student IDs to the lesson
        for(int i = 0; i < jsonArray.length(); i++)
        {
            session.addStudent(jsonArray.getInt(i));
        }

        return session;
    }

    /**
     * Write a Lesson object to a JSONObject. This is in the format (order dictated by JSONObject):
     *  {
     *      "tutorId":9,
     *      "studentIdArray":[9],
     *      "lessonTime":{
     *          "minutes":660,
     *          "day":1
     *      },
     *      "maxSpaces":5,
     *      "id":9,
     *      "roomId":19,
     *      "subjectId":37
     *  }
     *
     * @param object    Lesson object to write to a JSONObject
     * @return          JSONObject containing the Lesson object data
     * @see             JSONObject
     * @see             Lesson
     * @since           1.0
     */
    @Override
    protected JSONObject writeObject(Lesson object)
    {
        final JSONObject jsonObject = new JSONObject();

        // Write the ID of the tutor to the object
        jsonObject.put(TUTOR_ID_TAG, object.getTutorId());

        // Write the ID of the room to the object
        jsonObject.put(ROOM_ID_TAG, object.getRoomId());

        // Write the ID of the subject to the object
        jsonObject.put(SUBJECT_ID_TAG, object.getSubjectId());

        // Write the lesson time to the object
        jsonObject.put(LESSON_TIME_TAG, SessionTimeJsonParser.writeSessionTime(object.getSessionTime()));

        // Write the maximum number of spaces to the object
        jsonObject.put(MAX_SPACES_TAG, object.getMaxSpaces());

        // Create array for student IDs
        final JSONArray studentIdArray = new JSONArray();

        // Write student IDs to the student ID array
        for(final Integer studentId : object.getStudents())
        {
            studentIdArray.put(studentId);
        }

        // Write the student ID array to the object
        jsonObject.put(STUDENT_ID_ARRAY_TAG, studentIdArray);

        return jsonObject;
    }
}
