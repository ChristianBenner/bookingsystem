package com.christianbenner.itc;

import com.christianbenner.itc.data.*;
import com.christianbenner.itc.json.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Directories class contains singleton instances of all the different data directories of the system
 *
 * @see         DirectoryObjectJsonParser
 * @see         Directory
 * @author      Christian Benner
 * @version     %I%, %G%
 * @since       1.0
 */
public class Directories
{
    // Tutors JSON save file name
    private static final String TUTORS_SAVE_NAME = "tutors.json";

    // Students JSON save file name
    private static final String STUDENTS_SAVE_NAME = "students.json";

    // Subjects JSON save file name
    private static final String SUBJECTS_SAVE_NAME = "subjects.json";

    // Rooms JSON save file name
    private static final String ROOMS_SAVE_NAME = "rooms.json";

    // Lesson JSON save file name
    private static final String LESSONS_SAVE_NAME = "lessons.json";

    // Parent meeting JSON save file name
    private static final String PARENT_MEETINGS_SAVE_NAME = "meetings.json";

    // Tutor directory singleton instance
    private static Directory<Tutor> tutorDirectoryInstance;

    // Student directory singleton instance
    private static Directory<Student> studentDirectoryInstance;

    // Subject directory singleton instance
    private static Directory<Subject> subjectDirectoryInstance;

    // Room directory singleton instance
    private static Directory<Room> roomDirectory;

    // Lesson directory singleton instance
    private static Directory<Lesson> lessonDirectory;

    // Parent meeting directory singleton instance
    private static Directory<ParentMeeting> parentMeetingDirectory;

    /**
     * Retrieve the tutor directory singleton instance
     *
     * @return  Singleton instance of the tutor directory
     * @see     Directory
     * @see     Tutor
     * @since   1.0
     */
    public static final Directory<Tutor> getTutorDirectory()
    {
        // If the instance has not been constructed yet, construct it
        if(tutorDirectoryInstance == null)
        {
            tutorDirectoryInstance = new Directory<>(new File(TUTORS_SAVE_NAME), new TutorJsonParser());
        }

        return tutorDirectoryInstance;
    }

    /**
     * Retrieve the student directory singleton instance
     *
     * @return  Singleton instance of the student directory
     * @see     Directory
     * @see     Student
     * @since   1.0
     */
    public static final Directory<Student> getStudentDirectory()
    {
        // If the instance has not been constructed yet, construct it
        if(studentDirectoryInstance == null)
        {
            studentDirectoryInstance = new Directory<>(new File(STUDENTS_SAVE_NAME), new StudentJsonParser());
        }

        return studentDirectoryInstance;
    }

    /**
     * Retrieve the subject directory singleton instance
     *
     * @return  Singleton instance of the subject directory
     * @see     Directory
     * @see     Subject
     * @since   1.0
     */
    public static final Directory<Subject> getSubjectDirectory()
    {
        // If the instance has not been constructed yet, construct it
        if(subjectDirectoryInstance == null)
        {
            subjectDirectoryInstance = new Directory<>(new File(SUBJECTS_SAVE_NAME), new SubjectJsonParser());
        }

        return subjectDirectoryInstance;
    }

    /**
     * Retrieve the room directory singleton instance
     *
     * @return  Singleton instance of the room directory
     * @see     Directory
     * @see     Room
     * @since   1.0
     */
    public static final Directory<Room> getRoomDirectory()
    {
        // If the instance has not been constructed yet, construct it
        if(roomDirectory == null)
        {
            roomDirectory = new Directory<>(new File(ROOMS_SAVE_NAME), new RoomJsonParser());
        }

        return roomDirectory;
    }

    /**
     * Retrieve the lesson directory singleton instance
     *
     * @return  Singleton instance of the lesson directory
     * @see     Directory
     * @see     Lesson
     * @since   1.0
     */
    public static final Directory<Lesson> getLessonDirectory()
    {
        // If the instance has not been constructed yet, construct it
        if(lessonDirectory == null)
        {
            lessonDirectory = new Directory<>(new File(LESSONS_SAVE_NAME), new LessonJsonParser());
        }

        return lessonDirectory;
    }

    /**
     * Retrieve the parent meeting directory singleton instance
     *
     * @return  Singleton instance of the parent meeting directory
     * @see     Directory
     * @see     ParentMeeting
     * @since   1.0
     */
    public static final Directory<ParentMeeting> getParentMeetingDirectory()
    {
        // If the instance has not been constructed yet, construct it
        if(parentMeetingDirectory == null)
        {
            parentMeetingDirectory = new Directory<>(new File(PARENT_MEETINGS_SAVE_NAME),
                    new ParentMeetingJsonParser());
        }

        return parentMeetingDirectory;
    }
}
