package com.christianbenner.itc;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

public class SaveUtils
{
    /**
     * Read the JSON from a JSON file
     *
     * @param saveFile  Save file to read from
     * @return          JSONObject containing JSON data from the saveFile
     * @since           1.0
     */
    public static JSONObject readJSONFile(final File saveFile)
    {
        // Check if the save file exists yet
        if(saveFile.exists())
        {
            try
            {
                // File input stream
                final FileInputStream fileInputStream = new FileInputStream(saveFile);

                // Create JSONTokener from the input stream
                final JSONTokener jsonTokener = new JSONTokener(fileInputStream);

                if(fileInputStream.available() == 0)
                {
                    System.out.println("Save file '" + saveFile.getName() +
                            "' is empty, aborting JSON read");
                }
                else
                {
                    // Process the JSONTokener adding the data to referenced arrays of students and tutors
                    return new JSONObject(jsonTokener);
                }
            }
            catch (FileNotFoundException e)
            {
                // File does not exist, print error
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                // Failed to parse JSON, print error
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // Input/output exception, print error
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("Save file does not exist '" + saveFile.getName() + "'");
        }

        return null;
    }

    /**
     * Write to the JSON file
     *
     * @param saveFile      Save file to write to
     * @param jsonObject    JSONObject to write to the save file
     * @return              Success state - True if written successfully, else false
     * @since               1.0
     */
    public static boolean writeJSONFile(final File saveFile, final JSONObject jsonObject)
    {
        try
        {
            // Create a file output stream to the save file
            final FileOutputStream fileOutputStream = new FileOutputStream(saveFile);

            // Write the JSON data to the file output stream
            fileOutputStream.write(jsonObject.toString().getBytes());

            // File written successfully, return true
            return true;
        }
        catch (IOException e)
        {
            // Input/output exception, print error
            e.printStackTrace();
            return false;
        }
    }
}
